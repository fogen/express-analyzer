<%--
  Created by IntelliJ IDEA.
  User: sa
  Date: 11.05.2019
  Time: 13:16
--%>

<%@ page import="express.StatisticController" contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>[${request.drawing.number}] ${StatisticController.availableActions[actionName]}</title>
        <meta name="layout" content="main">
    </head>

    <body>
        <g:form action="intellectualMaxStakes" method="post" id="${request.drawing.number}">
            <div class="form-group row align-items-center">
                <div class="col-1 align-self-end">
                    <div class="text-center">Кол-во итераций</div>
                    <div class="input-group">
                        <g:textField class="form-control" name="iteration" type="text" value="${iteration ?: 1}"/>
                    </div>
                </div>
                <div class="col-1 align-self-end">
                    <div class="text-center">Кол-во купонов</div>
                    <div class="input-group">
                        <g:textField class="form-control" name="howMany" type="text" value="${howMany ?: 10}"/>
                    </div>
                </div>
                <div class="col-1">
                    <div class="text-center">Кол-во месяцев</div>
                    <div class="input-group">
                        <g:textField class="form-control" name="monthCount" type="text" value="${monthCount ?: 1}"/>
                    </div>
                </div>
                <div class="col-1">
                    <div class="text-center">Мин. угадано</div>
                    <div class="input-group">
                        <g:textField class="form-control" name="minWinCount" type="text" value="${minWinCount ?: 13}"/>
                    </div>
                </div>
                <div class="col-1 align-self-end">
                    <g:submitButton class="btn btn-outline-success" name="search" value="Найти"/>
                </div>
                <g:if test="${stakesData}">
                    <div class="col-12">
                        <g:each in="${stakesData}" var="data">
                            <table class="default-table table-bordered" style="text-align: center">
                                <thead>
                                    <tr>
                                        <th rowspan="2">№ п/п</th>
                                        <th colspan="2">max</th>
                                        <th colspan="2">mid</th>
                                        <th colspan="2">min</th>
                                    </tr>
                                    <tr>
                                        <g:each in="${1..3}">
                                            <th>Код</th>
                                            <th>%</th>
                                        </g:each>
                                    </tr>
                                </thead>
                                <tbody>
                                    <g:each in="${data.maxStakes}" var="rowData" status="i">
                                        <tr>
                                            <td>${i + 1}</td>
                                            <g:each in="${rowData}" var="row">
                                                <td class="text-monospace"><b>${row.value.code}</b></td>
                                                <td>${row.value.percent}</td>
                                            </g:each>
                                        </tr>
                                    </g:each>
                                </tbody>
                            </table>

                            <%=data.max%> - ${data.additional} <br/>
                            <g:link controller="drawing"
                                    action="intellectual"
                                    id="${request.drawing.number}"
                                    params="${[
                                        search     : true,
                                        monthCount : monthCount,
                                        minWinCount: minWinCount,
                                        excludes   : data.min?.toList()
                                    ]}">Подобрать</g:link>
                        </g:each>
                    </div>
                </g:if>
            </div>
        </g:form>

    </body>
</html>