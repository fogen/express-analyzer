<%--
  Created by IntelliJ IDEA.
  User: semenuk
  Date: 04.06.2019
  Time: 19:15
--%>

<%@ page import="express.StatisticController" contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>${StatisticController.availableActions[actionName]}</title>
        <meta name="layout" content="main">
    </head>

    <body>
    <div style="font-family: monospace;font-size: 12px;font-weight: bold;">
        <div style="padding: 5px;display: table;">
            <div style="display: table-cell;width: 80px;text-align: center;">Тираж</div>
            <div style="display: table-cell;width: 130px;text-align: center;">Код</div>
            <div style="display: table-cell;width: 80px;text-align: center;">Связи</div>
            <div style="display: table-cell;width: 80px;text-align: center;">Пул</div>
            <div style="display: table-cell;width: 80px;text-align: center;">Линия</div>
        </div>
        <g:each in="${data}" var="averages">
            <div style="padding: 5px;display: table;border-top:1px solid;">
                <div style="display: table-cell;width: 80px;text-align: center;">${averages.number}</div>
                <div style="display: table-cell;width: 130px;text-align: center;">${averages.code}</div>
                <div style="display: table-cell;width: 80px;text-align: center;">${averages.linkAverage}%</div>
                <div style="display: table-cell;width: 80px;text-align: center;">${averages.poolAverage}%</div>
                <div style="display: table-cell;width: 80px;text-align: center;">${averages.lineAverage}%</div>
            </div>
        </g:each>
    </body>
</html>