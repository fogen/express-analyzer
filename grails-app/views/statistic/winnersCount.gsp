<%--
  Created by IntelliJ IDEA.
  User: semenuk
  Date: 30.04.2019
  Time: 10:31
--%>

<%@ page import="express.StatisticController" contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>${StatisticController.availableActions[actionName]}</title>
        <meta name="layout" content="main">
    </head>

    <body>
        <g:form action="winnersCount" method="post">
            <div class="form-group row align-items-center">
                <div class="col-1">
                    <div class="text-center">Кол-во месяцев</div>
                    <div class="input-group">
                        <g:textField class="form-control" name="monthCount" type="text" value="${monthCount ?: 1}"/>
                    </div>
                </div>
                <div class="col-1 align-self-end">
                    <g:submitButton class="btn btn-outline-success" name="calc" value="Посчитать"/>
                </div>
            </div>
        </g:form>
        <g:if test="${dataMaxMin}">
            <h1>Совмещённые</h1>
            <g:render template="data_table" model="${[data: dataMaxMin, renderParams: renderParams]}"/>
        </g:if>
    </body>
</html>