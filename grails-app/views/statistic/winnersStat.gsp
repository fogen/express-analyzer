<%--
  Created by IntelliJ IDEA.
  User: sa
  Date: 24.03.2019
  Time: 12:12
--%>

<%@ page import="express.StatisticController" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>${StatisticController.availableActions[actionName]}</title>
    <meta name="layout" content="main">
</head>

<body>
<g:render template="data_table" model="${[data: data, renderParams: renderParams]}"/>
</body>
</html>