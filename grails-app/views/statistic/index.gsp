<%--
  Created by IntelliJ IDEA.
  User: sa
  Date: 24.03.2019
  Time: 11:26
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Доступные действия</title>
    <meta name="layout" content="main">
</head>

<body>
    <g:if test="${flash.error}">
        <div class="error">${flash.error}</div>
    </g:if>
    <g:each in="${actions}" var="action">
        <g:link action="${action.key}" id="${!actionsWithoutId.contains(action.key) ? request.drawing.number : ''}">${action.value}</g:link><br/>
    </g:each>
</body>
</html>