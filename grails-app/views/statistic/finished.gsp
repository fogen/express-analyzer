<%--
  Created by IntelliJ IDEA.
  User: sa
  Date: 27.03.2019
  Time: 21:00
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Графики законченных тиражей</title>
    <meta name="layout" content="main">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {packages: ['corechart', 'line']});
        google.charts.setOnLoadCallback(drawCurveTypes);

        function drawCurveTypes() {
            <g:each in="${data}" var="line" status="counter">
                drawChart(<%= line.chartData %>, 'chart_${counter}');

                data = new google.visualization.DataTable();
                data.addColumn('string', '% выигрыша');
                data.addColumn('number', 'Уникальные');
                data.addColumn('number', 'Все');
                data.addColumn('number', 'Пул');
                data.addColumn('number', 'Линия');

                data.addRows(<%= line.chartData2.data %>);
                chartId = 'chart_av_${counter}';

                options = {
                    title: 'Распределение ставок по процентному выигрышу',
                    height: document.getElementById(chartId).offsetWidth / 2,
                    vAxis: {
                        format: 'percent'
                    },
                    legend: {position: 'bottom'}
                };

                chart = new google.visualization.LineChart(document.getElementById(chartId));
                chart.draw(data, options);
            </g:each>
        }
    </script>
</head>

<body>
<g:each in="${data}" var="line" status="counter">
    <div class="text-center">
        <g:link controller="drawing" action="view" id="${line.drawing.number}">${line.drawing.number}</g:link><br/>
        <span><g:formatDate date="${line.drawing.expired}" format="dd.MM HH:mm"/></span><br/>
        <span>${line.jackpot ? 'Выигран суперприз!' : ''}</span>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-12">
            <div id="chart_${counter}"></div>
        </div>
        <div class="col-lg-6 col-md-12">
            <div id="chart_av_${counter}"></div>
        </div>
    </div>
</g:each>
</body>
</html>