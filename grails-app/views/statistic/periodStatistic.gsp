<%--
  Created by IntelliJ IDEA.
  User: sa
  Date: 24.03.2019
  Time: 12:17
--%>

<%@ page import="express.AverageType; express.StatisticController" contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>${StatisticController.availableActions[actionName]}</title>
        <meta name="layout" content="main">
    </head>

    <body>
        <g:form action="periodStatistic" method="post">
            <div class="form-group row align-items-center">
                <div class="col-1">
                    <div class="text-center">Кол-во месяцев</div>
                    <div class="input-group">
                        <g:textField class="form-control" name="monthCount" type="text" value="${monthCount ?: 1}"/>
                    </div>
                </div>
                <div class="col-1">
                    <div class="text-center">Мин. угадано</div>
                    <div class="input-group">
                        <g:textField class="form-control" name="minWinCount" type="text" value="${minWinCount ?: 13}"/>
                    </div>
                </div>
                <div class="col-1">
                    <div class="row align-items-center">
                        <div class="text-center">Процент</div>
                        <g:select name="averageType"
                                  from="${AverageType.values()}"
                                  optionKey="id"
                                  optionValue="decription"
                                  value="${averageType ?: AverageType.LINK}"/>
                    </div>
                </div>
                <div class="col-1 align-self-end">
                    <g:submitButton class="btn btn-outline-success" name="calc" value="Посчитать"/>
                </div>
            </div>
        </g:form>
        <g:if test="${data}">
            <g:render template="data_table" model="${[data: data, renderParams: renderParams]}"/>
        </g:if>
    </body>
</html>