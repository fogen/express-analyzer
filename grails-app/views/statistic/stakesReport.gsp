<%--
  Created by IntelliJ IDEA.
  User: sa
  Date: 24.03.2019
  Time: 12:03
--%>

<%@ page import="express.StatisticController" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>[${request.drawing.number}] ${StatisticController.availableActions[actionName]}</title>
    <meta name="layout" content="main">
</head>

<body>
    <h1>${request.drawing.stakeCount + " / " + request.drawing.optionCount + " - " + (winnersAverage ?: '? ')}</h1>
    <g:render template="data_table" model="${[data: data, renderParams: renderParams]}"/>
</body>
</html>