<%--
  Created by IntelliJ IDEA.
  User: sa
  Date: 24.03.2019
  Time: 11:29
--%>

<%@ page import="express.StatisticController" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>[${request.drawing.number}] ${StatisticController.availableActions[actionName]}</title>
    <meta name="layout" content="main">
</head>

<body>
    <g:each in="${stakes}" var="stake">
        <h1>${stake.winCount}</h1>
        <g:render template="data_table" model="${[data: stake.rates, renderParams: renderParams]}"/>
    </g:each>

</body>
</html>