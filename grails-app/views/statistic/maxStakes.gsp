<%--
  Created by IntelliJ IDEA.
  User: sa
  Date: 24.03.2019
  Time: 12:46
--%>

<%@ page import="express.StatisticController" contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>[${request.drawing.number}] ${StatisticController.availableActions[actionName]}</title>
        <meta name="layout" content="main">
        <script type="text/javascript">
        </script>
    </head>

    <body>
    <g:form action="maxStakes" method="post" id="${request.drawing.number}">
        <div class="form-group row align-items-center">
            <div class="col-1 align-self-end">
                <div class="text-center">Макс. кол-во фаворитов</div>
                <div class="input-group">
                    <g:textField class="form-control" name="maxCount" type="text" value="${maxCount ?: 7}"/>
                </div>
            </div>
            <div class="col-1 align-self-end">
                <div class="text-center">Мин. кол-во кодов в группе</div>
                <div class="input-group">
                    <g:textField class="form-control" name="minGroupSize" type="text" value="${minGroupSize ?: 2}"/>
                </div>
            </div>
            <div class="col-1 align-self-end">
                <div class="text-center">Макс. совпадений 14 кат</div>
                <div class="input-group">
                    <g:textField class="form-control" name="max14" type="number" value="${max14 ?: ''}"/>
                </div>
            </div>
            <div class="col-1 align-self-end">
                <div class="text-center">Макс. совпадений 13 кат</div>
                <div class="input-group">
                    <g:textField class="form-control" name="max13" type="number" value="${max13 ?: ''}"/>
                </div>
            </div>
            <div class="col-1">
                <div class="row align-items-center">
                    <g:checkBox name="unique" value="${unique}"/>
                    <label class="m-0">Только уникальные</label>
                </div>
            </div>
            <div class="col-1 align-self-end">
                <g:submitButton class="btn btn-outline-success" name="search" value="Найти"/>
            </div>
            <g:if test="${stakesData}">
                <div class="col-12">
                    <g:each in="${stakesData}" var="data">
                        <div>Найдено <b>${data.total}</b> купонов по маске <span class="text-monospace">${data.maxLike}</span></div>
                        <table class="default-table table-bordered" style="text-align: center">
                            <thead>
                                <tr>
                                    <th rowspan="2">№ п/п</th>
                                    <th colspan="${grouped ? 3 : 2}">max</th>
                                    <th colspan="${grouped ? 3 : 2}">mid</th>
                                    <th colspan="${grouped ? 3 : 2}">min</th>
                                </tr>
                                <tr>
                                    <g:each in="${1..3}">
                                        <th>Код</th>
                                        <th>%</th>
                                        <g:if test="${grouped}">
                                            <th>%</th>
                                        </g:if>
                                    </g:each>
                                </tr>
                            </thead>
                            <tbody>
                                <g:each in="${data.maxStakes}" var="rowData" status="i">
                                    <tr>
                                        <td>${i + 1}</td>
                                        <g:each in="${rowData}" var="row">
                                            <td class="text-monospace"><b>${row.value.code}</b></td>
                                            <td>${row.value.percent}</td>
                                            <g:if test="${grouped}">
                                                <td>${row.value.fromAll}</td>
                                            </g:if>
                                        </g:each>
                                    </tr>
                                </g:each>
                            </tbody>
                        </table>

                        <%=data.max%> - ${data.additional}
                    </g:each>
                </div>
            </g:if>
        </div>
    </g:form>
    </body>
</html>