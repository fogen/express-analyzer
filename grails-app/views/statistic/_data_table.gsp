<table class="default-table">
    <thead>
    <tr>
        <g:each in="${data?.first()?.keySet()}" var="key">
            <th style="text-align: center">${renderParams?.thead?.get(key) ?: key}</th>
        </g:each>
    </tr>
    </thead>
    <tbody>
        <g:each in="${data}" var="rowData">
            <tr>
                <g:each in="${rowData}" var="row">
                    <%
                        def styles = [renderParams?.style?.get(row.key) ?: '']
                        def value = row.value

                        if (value instanceof Date) {
                            value = g.formatDate([date: value, format: "dd.MM HH:mm"])
                        }

                        if (value instanceof Map) {
                            value = value.value
                            styles << value?.style
                        } else {

                            if (value instanceof Number) {
                                styles << "text-align:center"
                            }
                        }
                    %>
                    <td style="${styles.join(";")}"><%=  value %></td>
                </g:each>
            </tr>
        </g:each>
    </tbody>
</table>
