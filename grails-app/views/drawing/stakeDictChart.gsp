<%--
  Created by IntelliJ IDEA.
  User: semenuk
  Date: 26.03.2019
  Time: 15:53
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>График распределения ставок</title>
        <meta name="layout" content="main">
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
            google.charts.load('current', {packages: ['corechart', 'line']});
            google.charts.setOnLoadCallback(drawCurveTypes);

            function drawCurveTypes() {
                drawChart(<%= chartData %>, 'chart_div');

                data = new google.visualization.DataTable();
                data.addColumn('string', '% выигрыша');
                data.addColumn('number', 'Уникальные');
                data.addColumn('number', 'Все');
                data.addColumn('number', 'Пул');
                data.addColumn('number', 'Линия');

                data.addRows(<%= chartData2.data %>);

                chartId = 'chart_av';
                options = {
                    title: 'Распределение ставок по процентному выигрышу',
                    height: document.getElementById(chartId).offsetWidth / 2,
                    vAxis: {
                        format: 'percent'
                    },
                    legend: {position: 'bottom'}
                };

                chart = new google.visualization.LineChart(document.getElementById(chartId));
                chart.draw(data, options);
            }
        </script>
    </head>

    <body>
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div id="chart_div"></div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div id="chart_av"></div>
            </div>
        </div>
    </body>
</html>