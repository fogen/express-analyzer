<%--
  Created by IntelliJ IDEA.
  User: semenuk
  Date: 26.09.18
  Time: 12:13
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>[${request.drawing.number}] Данные по суперэкспрессу</title>
        <meta name="layout" content="main">
        <script type="text/javascript">

            function getWinAmount(selector, code) {
                var request = new XMLHttpRequest();
                request.open('GET', '${g.createLink(controller: "drawing", action: "getPossibleWinAmount", id: request.drawing?.number)}?code=' + code);
                request.addEventListener('readystatechange', function () {
                    if (request.readyState !== 4) {
                        return;
                    }
                    var data = JSON.parse(request.responseText);
                    if (data.winAmount) {
                        selector.style.cssText = "";
                        selector.innerText = data.winAmount;
                    }
                });
                request.send();
            }

            var checkUpdateStakeDicInterval;

            function updateStakeDict() {
                var request = new XMLHttpRequest();
                request.open('GET', '${g.createLink(controller: "drawing", action: "updateStakeDict", id: request.drawing?.number)}');
                request.addEventListener('readystatechange', function () {
                    if (request.readyState !== 4) {
                        return;
                    }
                    var data = JSON.parse(request.responseText);
                    if (data.result) {
                        $("#updateStakeDic").hide();
                        checkUpdateStakeDicInterval = setTimeout(function () {
                            checkUpdateStakeDic()
                        }, 5000);
                    }
                });
                request.send();
            }

            function checkUpdateStakeDic() {
                var request = new XMLHttpRequest();
                request.open('GET', '${g.createLink(controller: "drawing", action: "checkUpdateStakeDic", id: request.drawing?.number)}');
                request.addEventListener('readystatechange', function () {
                    if (request.readyState !== 4) {
                        clearTimeout(checkUpdateStakeDicInterval);
                        return;
                    }
                    var data = JSON.parse(request.responseText);
                    if (data.finished) {
                        <g:if test="${request.drawing.state == 2}">
                            $("#updateStakeDic").show();
                        </g:if>
                        clearTimeout(checkUpdateStakeDicInterval);
                    }
                });
                request.send();
            }

            $(document).ready(function () {
                <g:if test="${request.drawing.state == 2}">
                checkUpdateStakeDicInterval = setTimeout(function () {
                    checkUpdateStakeDic()
                }, 5000);
                </g:if>
            });
        </script>
    </head>

    <body>
        <g:form action="view" method="post" id="${request.drawing.number}">
            <g:hiddenField name="from" value="${from}"/>
            <g:hiddenField name="to" value="${to}"/>
            <div class="row text-center py-3">
                <div class="col-sm-2">
                    <div class="text-primary">Дата</div>
                    <div class="property-value"><g:formatDate date="${request.drawing.expired}" format="dd.MM HH:mm"/></div>
                </div>

                <div class="col-sm-1">
                    <div class="text-primary">Сложность</div>
                    <div class="property-value"><g:formatNumber number="${request.drawing.complexity}" type="number"/></div>
                </div>

                <div class="col-sm-2">
                    <div class="text-primary">Пул</div>
                    <div class="property-value"><g:formatNumber number="${request.drawing.pool}" type="currency"/></div>
                </div>

                <div class="col-sm-2">
                    <div class="text-primary">Суперприз</div>
                    <div class="property-value"><g:formatNumber number="${request.drawing.jackpot}" type="currency"/></div>
                </div>

                <div class="col-sm-1">
                    <div class="text-primary">Купоны</div>
                    <div class="property-value"><g:formatNumber number="${request.drawing.couponCount}" type="number"/></div>
                </div>

                <div class="col-sm-2">
                    <div class="text-primary">Варианты</div>
                    <div class="property-value"><g:formatNumber number="${request.drawing.stakeCount}" type="number"/></div>
                </div>

                <div class="col-sm-2">
                    <div class="text-primary">Уникальные</div>
                    <div class="property-value"><g:formatNumber number="${request.drawing.optionCount}" type="number"/></div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <table width="100%" class="table-bordered table-striped">
                        <g:each in="${events}" var="event" status="i">
                            <tr>
                                <g:set var="maxPerc" value="${[event.win1Percentage, event.win2Percentage, event.drawPercentage].max()}"/>
                                <g:set var="maxProb" value="${[event.win1Probability, event.win2Probability, event.drawProbability].max()}"/>
                                <g:set var="maxStake" value="${uniqueStakes.data ? uniqueStakes.data[i].max() : null}"/>
                                <g:set var="winner" value="${event.resultCode ?: uniqueStakes.winners?.get(i)}"/>
                                <td class="media-middle text-center">${event.sortOrder + 1}</td>
                                <td class="media-middle">${event.name}</td>
                                <td class="media-middle text-center">
                                    <div><b><g:formatDate date="${event.date}" format="HH:mm" /></b></div>
                                    <div><g:formatDate date="${event.date}" format="dd.MM" /></div>
                                </td>
                                <td class="text-center media-middle">${event.score ?: ' -:- '}</td>
                                <td class="text-center ${winner == "1" ? ' text-danger border-danger' : ''}" style="${uniqueStakes.winners?.get(i) == "1" ? 'box-shadow: 0 0 0 3px #28a745;' : ''}${event.resultCode == "1" ? 'border: 2px solid' : ''}">
                                    <div class="center-block ${maxPerc == event.win1Percentage ? 'bg-info text-light' : ''}"><g:formatNumber number="${event.win1Percentage}" type="number"/></div>
                                    <div class="center-block ${maxProb == event.win1Probability ? 'bg-info text-light' : ''}"><g:formatNumber number="${event.win1Probability}" type="number"/></div>
                                    <g:if test="${uniqueStakes.count}">
                                        <div class="center-block ${maxStake == uniqueStakes.data[i][0] ? 'bg-danger text-light' : ''}"><g:formatNumber number="${uniqueStakes.data[i][0]}" type="number"/></div>
                                    </g:if>
                                </td>
                                <td class="text-center ${winner == "X" ? ' text-danger border-danger' : ''}" style="${uniqueStakes.winners?.get(i) == "X" ? 'box-shadow: 0 0 0 3px #28a745;' : ''}${event.resultCode == "X" ? 'border: 2px solid' : ''}">
                                    <div class="center-block ${maxPerc == event.drawPercentage ? 'bg-info text-light' : ''}"><g:formatNumber number="${event.drawPercentage}" type="number"/></div>
                                    <div class="center-block ${maxProb == event.drawProbability ? 'bg-info text-light' : ''}"><g:formatNumber number="${event.drawProbability}" type="number"/></div>
                                    <g:if test="${uniqueStakes.count}">
                                        <div class="center-block ${maxStake == uniqueStakes.data[i][1] ? 'bg-danger text-light' : ''}"><g:formatNumber number="${uniqueStakes.data[i][1]}" type="number"/></div>
                                    </g:if>
                                </td>
                                <td class="text-center ${winner == "2" ? ' text-danger border-danger' : ''}" style="${uniqueStakes.winners?.get(i) == "2" ? 'box-shadow: 0 0 0 3px #28a745;' : ''}${event.resultCode == "2" ? 'border: 2px solid' : ''}">
                                    <div class="center-block ${maxPerc == event.win2Percentage ? 'bg-info text-light' : ''}"><g:formatNumber number="${event.win2Percentage}" type="number"/></div>
                                    <div class="center-block ${maxProb == event.win2Probability ? 'bg-info text-light' : ''}"><g:formatNumber number="${event.win2Probability}" type="number"/></div>
                                    <g:if test="${uniqueStakes.count}">
                                        <div class="center-block ${maxStake == uniqueStakes.data[i][2] ? 'bg-danger text-light' : ''}"><g:formatNumber number="${uniqueStakes.data[i][2]}" type="number"/></div>
                                    </g:if>
                                </td>
                            </tr>
                        </g:each>
                    </table>
                </div>
                <div class="col-lg-6">
                    <g:if test="${uniqueStakes.count}">
                        <div>
                            <span>Найдено <b>${uniqueStakes.count}</b> ставок</span>
                        </div>
                    </g:if>
                    <div class="d-flex">
                        <div class="dropdown">
                            <button class="btn btn-outline-success dropdown-toggle" type="button" id="dropdownCount" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Посчитать
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownCount">
                                <g:submitButton class="dropdown-item" name="winCount" value="0"/>
                                <g:submitButton class="dropdown-item" name="winCount" value="9"/>
                                <g:submitButton class="dropdown-item" name="winCount" value="10"/>
                                <g:submitButton class="dropdown-item" name="winCount" value="11"/>
                                <g:submitButton class="dropdown-item" name="winCount" value="12"/>
                                <g:submitButton class="dropdown-item" name="winCount" value="13"/>
                                <g:submitButton class="dropdown-item" name="winCount" value="14"/>
                                <g:submitButton class="dropdown-item" name="winCount" value="15"/>
                            </div>
                        </div>
                        <div class="dropdown">
                            <button class="btn btn-outline-success dropdown-toggle" type="button" id="dropdownFill" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Заполнить
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownFill">
                                <g:submitButton class="dropdown-item" name="fillMinimal" value="Минимальными"/>
                                <g:submitButton class="dropdown-item" name="fillRandom" value="Случайными"/>
                                <g:if test="${events.find { it.resultCode }}">
                                    <g:submitButton class="dropdown-item" name="fillWinners" value="Победителями"/>
                                </g:if>
                            </div>
                        </div>
                        <g:submitButton class="btn btn-outline-secondary btn-sm" name="reset" value="Сбросить"/>
                        <g:if test="${request.drawing.state == 2}">
                            <div id="updateStakeDic" style="display: none">
                                <button class="btn btn-outline-success" onclick="updateStakeDict()" style="cursor: pointer;">Обновить ставки</button>
                            </div>
                        </g:if>
                    </div>
                    <table width="100%" style="table-layout: fixed;">
                        <thead>
                        <tr>
                            <g:each in="${(1..15)}" var="i">
                                <th class="text-center">${i}</th>
                            </g:each>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <g:each in="${(0..14)}" var="i">
                                <td class="text-center">
                                    <g:select id="stakePos-${i}"
                                              class="custom-select"
                                              name="stakePos"
                                              from="${["1", "X", "2"]}"
                                              noSelection="${["_": ""]}"
                                              value="${stakePos[i]}"/>
                                </td>
                            </g:each>
                        </tr>
                        <tr>
                            <g:each in="${(0..14)}" var="i">
                                <td class="text-center">
                                    <span style="cursor: pointer" onclick="nextLink('${i}', false);return false">&#9195;</span>
                                </td>
                            </g:each>
                        </tr>
                        <tr>
                            <g:each in="${(0..14)}" var="i">
                                <td class="text-center">
                                    <span style="cursor: pointer" onclick="nextLink('${i}', true);return false">&#9196;</span>
                                </td>
                            </g:each>
                        </tr>
                        <g:if test="${nonexistentStake}">
                            <tr>
                                <g:each in="${nonexistentStake}" var="ch">
                                    <td class="text-center bg-info">${ch}</td>
                                </g:each>
                            </tr>
                        </g:if>
                        <g:each in="${stakes}" var="stake">
                            <tr>
                                <g:each in="${stake}" var="ch">
                                    <td class="text-center">${ch}</td>
                                </g:each>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                    <div class="d-flex">
                        <g:submitButton class="btn btn-outline-success btn-sm" name="findStakes" value="Найти"/>
                    </div>

                    <g:if test="${chartData}">
                        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                        <script type="text/javascript">
                            google.charts.load('current', {packages: ['corechart', 'line']});
                            google.charts.setOnLoadCallback(drawCurveTypes);

                            function drawCurveTypes() {
                                drawChart(<%= chartData %>, "chart_div");
                            }
                        </script>
                        <div id="chart_div"></div>
                        <div class="text-center">
                            <g:if test="${request.drawing.state == 2 && session.user?.canRegisterCoupon() && stakePos && !stakePos.any { it == '_'} }">
                                <div>
                                    <label for="comment">Комментарий</label><input type="text" id="comment" value="Вручную">
                                    <button class="btn btn-outline-success register-btn"
                                    onclick="couponRegister(this.parentElement, currentCode, $('#comment').val());return false">Поставить</button>
                                </div>
                            </g:if>
                        </div>
                    </g:if>
                    <g:hiddenField name="showOperations" value="${showOperations}"/>
                    <g:if test="${session.user.isAuth() && !showOperations}">
                        <g:submitButton class="btn btn-outline-success btn-sm" name="showOperations" value="Показать ставки"/>
                    </g:if>
                    <g:else>
                        <g:if test="${operations}">
                            <table class="table table-sm table-striped text-center p">
                                <thead>
                                    <tr>
                                        <th>Код</th>
                                        <th>Совпадений</th>
                                        <th>${request.drawing.state == 3 ? 'Ожидаемый выигрыш' : 'Выигрыш'}</th>
                                        <th>Примечание</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <g:each in="${operations}" var="operation">
                                        <tr>
                                            <td class="text-monospace">
                                                <%= operation.code %>
                                                <g:link action="view"
                                                        params="${[stakePos: operation.simpleCode.toList(), showOperations: true]}"
                                                        id="${request.drawing.number}"
                                                        title="Показать график и редактировать">&#128480;</g:link>
                                                <a href="http://toto-info.co/#/${request.drawing.number}/${operation.couponId}"
                                                   title="Посмотреть расчёт на toto-info"
                                                   target="_blank">&#128270;</a>
                                            </td>
                                            <td>${operation.matchCount}</td>
                                            <g:if test="${request.drawing.state == 3}">
                                                <td><div style="cursor: pointer" onclick="getWinAmount(this, '${operation.simpleCode}')">Посчитать</div></td>
                                            </g:if>
                                            <g:else>
                                                <td>${operation.sum}</td>
                                            </g:else>
                                            <td>${operation.comment}</td>
                                        </tr>
                                    </g:each>
                                </tbody>
                            </table>
                        </g:if>
                    </g:else>
                </div>
            </div>
        </g:form>
    </body>
</html>