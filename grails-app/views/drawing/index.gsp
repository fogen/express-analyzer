<%--
  Created by IntelliJ IDEA.
  User: semenuk
  Date: 25.09.18
  Time: 11:52
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>Список тиражей</title>
        <meta name="layout" content="main">
    </head>

    <body>
        <table class="table table-striped text-center" width="100%">
            <thead>
                <tr>
                    <th>Тираж</th>
                    <th>Дата</th>
                    <th>Состояние</th>
                    <th>Джекпот</th>
                    <th>Призовой фонд</th>
                    <th>Купоны</th>
                    <th>Варианты</th>
                    <th>Уникальные</th>
                    <th>Сложность</th>
                </tr>
            </thead>
            <tbody style="line-height: 1">
                <g:each in="${drawings}" var="drawing">
                    <tr>
                        <td class="text-center"><g:link action="view" id="${drawing.number}">${drawing.number}</g:link></td>
                        <td class="text-center"><g:formatDate date="${drawing.expired}" format="dd.MM HH:mm"/></td>
                        <td class="text-center">${drawing.state}</td>
                        <td class="text-center"><g:formatNumber number="${drawing.jackpot}" type="currency"/></td>
                        <td class="text-center"><g:formatNumber number="${drawing.pool}" type="currency"/></td>
                        <td class="text-center"><g:formatNumber number="${drawing.couponCount}" type="number"/></td>
                        <td class="text-center"><g:formatNumber number="${drawing.stakeCount}" type="number"/></td>
                        <td class="text-center"><g:formatNumber number="${drawing.optionCount}" type="number"/></td>
                        <td class="text-center"><g:formatNumber number="${drawing.complexity}" type="number"/></td>
                    </tr>
                </g:each>
            </tbody>
        </table>
    </body>
</html>