<%--
  Created by IntelliJ IDEA.
  User: semenuk
  Date: 11.04.2019
  Time: 18:48
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>[${request.drawing.number}] Подбор варианта ставок</title>
        <meta name="layout" content="main">
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
            google.charts.load('current', {packages: ['corechart', 'line']});
            google.charts.setOnLoadCallback(drawCurveTypes);

            function drawCurveTypes() {
                drawChart(<%= chartData %>, "chart_div");
            }
        </script>
    </head>

    <body>
        <g:form action="selection" method="post" id="${request.drawing.number}">
            <div class="form-group row align-items-center">
                <div class="col-6">
                    <table width="100%" style="table-layout: fixed;">
                        <thead>
                            <tr>
                                <g:each in="${(1..15)}" var="i">
                                    <th class="text-center">${i}</th>
                                </g:each>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <g:each in="${(0..14)}" var="i">
                                    <td class="text-center">
                                        <g:select id="stakePos-${i}"
                                                  class="custom-select"
                                                  name="stakePos"
                                                  from="${["1", "X", "2"]}"
                                                  noSelection="${["_": ""]}"
                                                  value="${stakePos ? stakePos[i] : '_'}"/>
                                    </td>
                                </g:each>
                            </tr>
                            <tr>
                                <g:each in="${(0..14)}" var="i">
                                    <td class="text-center">
                                        <span style="cursor: pointer" onclick="nextLink('${i}', false);return false">&#9195;</span>
                                    </td>
                                </g:each>
                            </tr>
                            <tr>
                                <g:each in="${(0..14)}" var="i">
                                    <td class="text-center">
                                        <span style="cursor: pointer" onclick="nextLink('${i}', true);return false">&#9196;</span>
                                    </td>
                                </g:each>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-1 align-self-end">
                    <g:submitButton class="btn btn-outline-success" name="search" value="Найти"/>
                </div>
            </div>
        </g:form>
        <g:if test="${request.drawing.state == 2 && session.user?.canRegisterCoupon() && stakePos && !stakePos.any { it == '_'} }">
            <div>
                <label for="comment">Комментарий</label><input type="text" id="comment" value="Вручную">
                <button class="btn btn-outline-success register-btn"
                        onclick="couponRegister(this, currentCode, $('#comment').val());return false">Поставить</button>
            </div>
        </g:if>
        <div id="chart_div"></div>
    </body>
</html>