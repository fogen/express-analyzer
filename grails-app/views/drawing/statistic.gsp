<%--
  Created by IntelliJ IDEA.
  User: sa
  Date: 23.10.18
  Time: 9:55
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Статистика</title>
    <meta name="layout" content="main">
</head>

<body>
    <h3>Статистика по законченным экспрессам</h3>
    <div class="col-4">
        <g:if test="${eventResults}">
            <g:link action="eventResultsChart">Графики роста</g:link>
            <table width="100%" class="table-bordered table-striped">
                <g:each in="${eventResults}" var="result" status="i">
                    <g:set var="max" value="${result.max { it.value }.value}"/>
                    <tr>
                        <td class="text-center">${i + 1}</td>
                        <td class="text-center ${max == result.win1 ? 'bg-info text-light' : ''}">
                            <div>${result.win1}</div>
                            <div><g:formatNumber number="${result.win1Percentage}" type="number"/></div>
                        </td>
                        <td class="text-center ${max == result.draw ? 'bg-info text-light' : ''}">
                            <div>${result.draw}</div>
                            <div><g:formatNumber number="${result.drawPercentage}" type="number"/></div>
                        </td>
                        <td class="text-center ${max == result.win2 ? 'bg-info text-light' : ''}">
                            <div>${result.win2}</div>
                            <div><g:formatNumber number="${result.win2Percentage}" type="number"/></div>
                        </td>
                    </tr>
                </g:each>
            </table>
        </g:if>
        <g:else>
            <g:form action="index">
                <g:submitButton value="Показать общую статистику" name="eventResults"/>
            </g:form>
        </g:else>

    </div>
</body>
</html>