<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<div style="font-family: monospace;font-size: 12px;font-weight: bold;">
    <g:if test="${averages}">
        <div style="padding: 5px;display: table;">
            <div style="display: table-cell;vertical-align: middle;width: 30px;text-align: center;">Кат</div>
            <div>
                <div style="display: table-cell;width: 130px;text-align: center;">Код</div>
                <div style="display: table-cell;width: 80px;text-align: center;">Связи</div>
                <div style="display: table-cell;width: 80px;text-align: center;">Пул</div>
                <div style="display: table-cell;width: 80px;text-align: center;">Линия</div>
                <div style="display: table-cell;width: 180px;text-align: center;">Совпадения</div>
                <div style="display: table-cell;width: 20px;text-align: center;"></div>
                <div style="display: table-cell;width: 20px;text-align: center;"></div>
            </div>
        </div>
    </g:if>
    <g:each in="${formattedCodes}" var="codes" status="i">
        <g:set var="border" value="${formattedCodes.size() - i > 1 ? 'border-bottom:1px solid;' : ''}"/>
        <div style="padding: 5px;display: table;${border}">
            <div style="display: table-cell;vertical-align: middle;width: 30px;text-align: center;">${codes.key}</div>
            <g:each in="${codes.value}" var="value">
                <div>
                    <div style="display: table-cell;width: 130px;text-align: center;">
                        <%=value.code%>
                    </div>
                    <g:if test="${value.average}">
                        <div style="display: table-cell;width: 80px;text-align: center;">${value.average}%</div>
                        <div style="display: table-cell;width: 80px;text-align: center;">${value.poolAverage}%</div>
                        <div style="display: table-cell;width: 80px;text-align: center;">${value.lineAverage}%</div>

                        <div style="display: table-cell;width: 180px;text-align: center;">${value.calc}</div>

                        <div style="display: table-cell;width: 20px;text-align: center;">
                            <g:link action="view"
                                    params="${[stakePos: value.code.replaceAll("-", "").toList()]}"
                                    id="${request.drawing.number}"
                                    title="Показать график и редактировать">&#128480;</g:link>
                        </div>
                    </g:if>
                    <g:if test="${value.sum}">
                        <div style="display: table-cell;"><g:formatNumber number="${value.sum}" type="number"/> ₽</div>
                    </g:if>
                    <g:else>
                        <div style="display: table-cell;width: 20px;text-align: center;">${value.matchCount < 9 ? '✖' : '✔'}</div>
                    </g:else>
                </div>
            </g:each>
        </div>
    </g:each>
</div>