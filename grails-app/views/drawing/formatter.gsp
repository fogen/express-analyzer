<%--
  Created by IntelliJ IDEA.
  User: semenuk
  Date: 07.05.2019
  Time: 20:26
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>[${request.drawing.number}] Форматирование кодов</title>
        <meta name="layout" content="main">
        <style type="text/css">
            tr {
                border-bottom:1pt solid darkgrey;
            }
            tr:last-child {
                border:none;
        }
        </style>
    </head>

    <body>
        <g:if test="${!formattedCodes}">
            <%= drawingInfo ?: '' %>
        </g:if>
        <g:form action="formatter" id="${request.drawing.number}">
            <g:hiddenField name="fullInfo" value="1"/>
            <g:textArea name="codes" />
            <g:textField name="urlId" type="text"/>
            <g:checkBox name="average" value="1">average</g:checkBox>
            <g:checkBox name="bbCode" value="">bbCode</g:checkBox>
            <g:submitButton class="btn btn-outline-success" name="doFormat" value="Форматировать"/>
            <g:submitButton class="btn btn-outline-success" name="save" value="Форматировать и сохранить"/>
        </g:form>
        <g:if test="${formattedCodes}">
            <div style="font-size: 11px">
                <g:render template="formatted_code" model="${[formattedCodes: formattedCodes, averages: averages]}"/>
                <g:each in="${formattedCodes.values()}" var="codes">
                    <g:each in="${codes.bbcode}" var="code">
                            <g:if test="${code}">
                                <div class="text-monospace"><%= code %></div>
                            </g:if>
                        </g:each>
                </g:each>
            </div>
        </g:if>
    </body>
</html>