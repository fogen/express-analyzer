<%--
  Created by IntelliJ IDEA.
  User: sa
  Date: 26.09.18
  Time: 22:14
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>[${request.drawing.number}] График изменения</title>
    <meta name="layout" content="main">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {packages: ['corechart', 'line']});
        google.charts.setOnLoadCallback(drawCurveTypes);

        function drawCurveTypes() {
            var chartId, options, data, chart;
            <g:each in="${data}" var="line" status="counter">
                data = new google.visualization.DataTable();
                data.addColumn('number', 'X');
                data.addColumn('number', 'win1');
                data.addColumn('number', 'draw');
                data.addColumn('number', 'win2');

                data.addRows(${line.value.data});
                chartId = 'chart_div_${counter}';
                options = {
                    height: document.getElementById(chartId).offsetWidth / 2,
                    vAxis: {
                        minValue: 15,
                        ticks: [33, 66]
                    },
                    title: "${counter + 1}: ${line.value.name}"
                };

                chart = new google.visualization.${chartType}Chart(document.getElementById(chartId));
                chart.draw(data, options);
            </g:each>
        }
    </script>
</head>

<body>
    <g:link action="chart" params="${[id: params.id, chartType: "Line"]}">Линии</g:link>
    <g:link action="chart" params="${[id: params.id, chartType: "Area"]}">Область</g:link>
    <g:each in="${data}" var="line" status="counter">
        <div id="chart_div_${counter}"></div>
    </g:each>
</body>
</html>