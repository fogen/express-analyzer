<%--
  Created by IntelliJ IDEA.
  User: semenuk
  Date: 30.10.18
  Time: 8:56
--%>

<%@ page import="express.AverageType; express.DrawingUtils; grails.converters.JSON" contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>[${request.drawing.number}] Возможные варианты выигрыша</title>
        <meta name="layout" content="main">
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">

        function resetCache() {
            var request = new XMLHttpRequest();
            request.open('GET', '${g.createLink(controller: "drawing", action: "reset_average_cache", id: request.drawing.number)}');
            request.send();
        }

        google.charts.load('current', {packages: ['corechart', 'line']});
            google.charts.setOnLoadCallback(drawCurveTypes);

            function drawCurveTypes() {
                <g:each in="${chartsData}" var="chartData" status="counter">
                    drawChart(<%= chartData as JSON %>, 'chart_div_${counter}');
                </g:each>
            }
        </script>
    </head>

    <body>
        <g:form action="average" method="post" id="${request.drawing.number}">
            <div class="form-group row align-items-center">
                <div class="col-lg-6 col-md-12">
                    <div class="text-center">Холд</div>
                    <table width="100%" style="table-layout: fixed;">
                        <thead>
                            <tr>
                                <g:each in="${(1..15)}" var="i">
                                    <th class="text-center">${i}</th>
                                </g:each>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <g:each in="${(0..14)}" var="i">
                                    <td class="text-center">
                                        <g:select class="custom-select" name="mask" from="${["1", "X", "2"]}" noSelection="${["_": ""]}" value="${mask ? mask[i] : '_'}"/>
                                    </td>
                                </g:each>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="text-center">Исключения</div>
                    <table width="100%" style="table-layout: fixed;">
                        <thead>
                            <tr>
                                <g:each in="${(1..15)}" var="i">
                                    <th class="text-center">${i}</th>
                                </g:each>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <g:each in="${(0..14)}" var="i">
                                    <td class="text-center">
                                        <g:select class="custom-select" name="excludes" from="${["1", "X", "2"]}" noSelection="${["_": ""]}" value="${excludes ? excludes[i] : '_'}"/>
                                    </td>
                                </g:each>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="text-center">13 совпадений</div>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">с</span></div>
                        <g:textField class="form-control" name="from13" type="text" value="${from13 ?: 0}"/>
                        <div class="input-group-prepend"><span class="input-group-text">по</span></div>
                        <g:textField class="form-control" name="to13" type="text" value="${to13 ?: 100}"/>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="text-center">14 совпадений</div>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">с</span></div>
                        <g:textField class="form-control" name="from14" type="text" value="${from14 ?: 0}"/>
                        <div class="input-group-prepend"><span class="input-group-text">по</span></div>
                        <g:textField class="form-control" name="to14" type="text" value="${to14 ?: 2}"/>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="text-center">%</div>
                    <div class="input-group">
                        <g:select name="averageType"
                                  from="${AverageType.values()}"
                                  optionKey="id"
                                  optionValue="decription"
                                  value="${averageType ?: AverageType.LINK.id}"/>
                        <div class="input-group-prepend"><span class="input-group-text">с</span></div>
                        <g:textField class="form-control" name="averageMin" type="text" value="${averageMin ?: 100}"/>
                        <div class="input-group-prepend"><span class="input-group-text">по</span></div>
                        <g:textField class="form-control" name="averageMax" type="text" value="${averageMax ?: 120}"/>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="text-center">Кол-во кодов для поиска</div>
                    <div class="input-group">
                        <g:textField class="form-control" name="howMany" type="text" value="${howMany ?: 20}"/>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <g:checkBox name="onlyUnique" value="${onlyUnique}"/>
                    <label>Только уникальные</label>
                </div>
                <div class="col-lg-3 col-md-6">
                    <g:checkBox name="withoutCharts" value="${withoutCharts}"/>
                    <label>Без графиков</label>
                </div>
                <div class="col-lg-1 col-md-1 align-self-end">
                    <g:submitButton class="btn btn-outline-success" name="search" value="Найти"/>
                </div>
                <g:if test="${request.drawing.state == 2}">
                    <div class="col-lg-1 col-md-1 align-self-end">
                        <button class="btn btn-outline-success" name="search" onclick="resetCache();return false">Сбросить кэш</button>
                    </div>
                </g:if>
            </div>
        </g:form>

        <g:if test="${withoutCharts}">
            <g:each in="${possible}" var="data">
                <div class="col-lg-6 col-md-12">${DrawingUtils.formatCode(data.code)}</div>
            </g:each>
            <g:each in="${possible}" var="data">
                <div class="col-lg-6 col-md-12">${data.packetCode}</div>
            </g:each>
        </g:if>
        <g:else>
            <div class="row">
                <g:each in="${chartsData}" var="data" status="counter">
                    <div class="col-lg-6 col-md-12">
                        <div id="chart_div_${counter}"></div>
                        <div>${DrawingUtils.formatCode(data.winnersCode)}</div>
                        <div>${data.packetCode}</div>
                        <div class="text-center">
                            <g:link action="selection"
                                    target="_blank"
                                    id="${request.drawing.number}"
                                    params="${[stakePos: data.winnersCode.toList()]}">Изменить</g:link>
                            <g:if test="${request.drawing.state == 2 && session.user?.canRegisterCoupon()}">
                                <div>
                                    <label for="comment_${counter}">Комментарий</label><input type="text" id="comment_${counter}" value="Вручную">
                                    <button class="btn btn-outline-success register-btn"
                                            onclick="couponRegister(this.parentElement, '${data.winnersCode}', $('#comment_${counter}').val());return false">Поставить</button>
                                </div>
                            </g:if>
                        </div>
                    </div>
                </g:each>
            </div>
        </g:else>
    </body>

</html>