<%@ page import="express.StatisticController" %>
<!doctype html>
<html lang="en" class="no-js">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <title>
            <g:layoutTitle default="Информационный сервис СУПЕРЭКСПРЕСС"/>
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <asset:link rel="icon" href="favicon.png" type="image/png"/>
        <asset:stylesheet src="application.css"/>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
              integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
              crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
                integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
                crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
                integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
                crossorigin="anonymous"></script>

        <style type="text/css">
            .user-balance {
                cursor: pointer;
            }
        </style>
        <script type="text/javascript">

            var currentCode;

            $(document).ready(function () {
                <g:if test="${session.user?.isAuth()}">
                    updateBalance();
                </g:if>
                currentCode = "${stakePos && !stakePos.any { it == '_'} ? stakePos.join() : ''}"
            });

            function setBalance(balance) {
                $("#user-balance").text(balance ? balance.toFixed(0) + " ₽" : 0);
                $('.register-btn').each(function() {
                    if (!balance || balance < 50) {
                        $(this).hide();
                    } else {
                        if (!$(this).hasClass("done")) {
                            $(this).show();
                        }
                    }
                });
            }

            function updateBalance() {
                var request = new XMLHttpRequest();
                request.open('GET', '${g.createLink(controller: "user", action: "update_balance_ajax")}');
                request.addEventListener('readystatechange', function() {
                    if (request.readyState !== 4) {
                        return;
                    }
                    var data = JSON.parse(request.responseText);
                    setBalance(data.balance);
                });

                request.send();
            }

            function couponRegister(button, code, comment) {
                var request = new XMLHttpRequest();
                var params = 'code=' + encodeURIComponent(code) + '&comment=' + encodeURIComponent(comment);
                request.open('GET', '${g.createLink(controller: "user", action: "couponRegister", id: request.drawing?.number)}?' + params);
                request.addEventListener('readystatechange', function() {
                    if (request.readyState !== 4) {
                        return;
                    }
                    var data = JSON.parse(request.responseText);
                    if (data.resultCode === 0) {
                        $(button).addClass("done");
                        $(button).hide();
                        if (data.clientSaldo) {
                            setBalance(data.clientSaldo);
                        }
                    }
                });
                request.send();
            }

            function drawChart(data, elementId) {
                var chartData = new google.visualization.DataTable();
                chartData.addColumn('string', 'Связь');
                chartData.addColumn('number', 'min/max');
                chartData.addColumn({id:'i0', type:'number'});
                chartData.addColumn({type: 'string', role: 'annotation'});
                chartData.addColumn({id:'i0', type:'number'});
                chartData.addColumn({type: 'string', role: 'annotation'});
                chartData.addColumn({id:'i0', type:'number'});
                chartData.addColumn({type: 'string', role: 'annotation'});
                chartData.addColumn({id:'i0', type:'number'});
                chartData.addColumn({type: 'string', role: 'annotation'});
                chartData.addColumn({id:'i1', type:'number'});
                chartData.addColumn({type: 'string', role: 'annotation'});
                chartData.addColumn({id:'i0', type:'number'});
                chartData.addColumn({type: 'string', role: 'annotation'});
                chartData.addColumn({id:'i0', type:'number'});
                chartData.addColumn({type: 'string', role: 'annotation'});
                chartData.addColumn({id:'i0', type:'number'});
                chartData.addColumn({type: 'string', role: 'annotation'});
                chartData.addColumn({id:'i0', type:'number'});
                chartData.addColumn({type: 'string', role: 'annotation'});

                var title = '';

                if (data.winnersCode) {
                    chartData.addColumn('number', 'Ставки');
                    chartData.addColumn('number', 'Вероятность');
                    chartData.addColumn('number', 'Переходы');
                    title = data.winnersCode;
                    title = title + ' - ' + data.winnersAverage + '/' + data.poolAverage + '/' + data.lineAverage;
                    title = title + ' (13: ' + data.m13 + ', 14: ' + data.m14 + ', 15: ' + data.m15 + ')'
                }

                if (data.maxWin || data.minWin) {
                    title = title  + ' фаворитов: '+ data.maxWin + '/' + data.minWin
                }

                chartData.addRows(data.data);

                chartOptions = {
                    title      : title,
                    height     : document.getElementById(elementId).offsetWidth / 2,
                    interval   : {
                        'i0': {
                            style: 'line',
                            color: '#999'
                        },
                        'i1': {
                            style    : 'line',
                            color    : '#333',
                            lineWidth: 3
                        }
                    },
                    annotations: {
                        stemColor: 'white',
                        textStyle: {
                            fontSize: 10
                        }
                    },
                    series     : {
                        0 : {
                            color: '#28a745'
                        },
                        1 : {
                            color    : '#999',
                            lineWidth: 1
                        },
                        2 : {
                            color    : '#999',
                            lineWidth: 1
                        },
                        3 : {
                            color    : '#999',
                            lineWidth: 1
                        },
                        4 : {
                            color    : '#999',
                            lineWidth: 1
                        },
                        5 : {
                            color    : '#999',
                            lineWidth: 3
                        },
                        6 : {
                            color    : '#999',
                            lineWidth: 1
                        },
                        7 : {
                            color    : '#999',
                            lineWidth: 1
                        },
                        8 : {
                            color    : '#999',
                            lineWidth: 1
                        },
                        9 : {
                            color    : '#999',
                            lineWidth: 1
                        },
                        10: {
                            color: '#dc3545'
                        },
                        11: {
                            color: '#ffc107'
                        },
                        12: {
                            color: '#007bff',
                            lineWidth: 3
                        }
                    },
                    axes: {
                        x: {
                            0: {side: 'top'}
                        }
                    },
                    vAxis      : {
                        minValue: data.minLinks,
                        ticks   : [
                            {
                                v: data.minLinks,
                                f: ''
                            },
                            {
                                v: data.average,
                                f: 'average'
                            },
                            {
                                v: data.topWinnersAverage,
                                f: '- min/max av'
                            },
                            {
                                v: data.minMaxAverage,
                                f: 'min/max av'
                            }
                        ]
                    },
                    legend     : 'none',
                    chartArea  : {
                        width : '80%',
                        height: '80%'
                    }
                };

                var chart = new google.visualization.LineChart(document.getElementById(elementId));
                chart.draw(chartData, chartOptions);

                google.visualization.events.addListener(chart, 'select', function() {
                    var row = chart.getSelection()[0].row;
                    var column = chart.getSelection()[0].column;
                    var link = chartData.getValue(row, column + 1);
                    $("#stakePos-" + row).val(link.substring(1));
                    $("#stakePos-" + (!row ? 14 : row - 1)).val(link.substring(0,1));

                    console.log();
                });

            }

            function nextLink(index, down) {
                var request = new XMLHttpRequest();
                var params = 'code=' + encodeURIComponent(currentCode) + '&index=' + index + '&down=' + down;
                request.open('GET', '${g.createLink(controller: "drawing", action: "nextLinkAjax", id: request.drawing?.number)}?' + params);
                request.addEventListener('readystatechange', function () {
                    if (request.readyState !== 4) {
                        return;
                    }

                    var responseData = JSON.parse(request.responseText);

                    if (responseData.data) {
                        drawChart(responseData, "chart_div");
                        currentCode = responseData.winnersCode;

                        for (var i = 0; i < currentCode.length; i++) {
                            $("#stakePos-" + i).val(currentCode.charAt(i))
                        }
                    }
                });
                request.send();
            }
        </script>
        <g:layoutHead/>
    </head>

    <body>
        <nav class="navbar navbar-expand-lg navbar-light position-fixed w-100" style="background-color: #e3f2fd;z-index: 1100">
            <div class="container">
                <a class="navbar-brand" href="${createLink(controller: 'drawing')}">fogen</a>
                <button class="navbar-toggler"
                        type="button"
                        data-toggle="collapse"
                        data-target="#navbarNavDropdown"
                        aria-controls="navbarNavDropdown"
                        aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav mr-auto order-1">
                        <g:if test="${session.user}">
                            <li class="nav-item ${controllerName == 'drawing' && (!actionName ||  actionName == 'index') ? 'active' : ''}">
                                <a class="nav-link" href="${createLink(controller: 'drawing')}">Список тиражей</a>
                            </li>
                            <g:if test="${session.user.admin}">
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle ${controllerName == 'statistic' ? 'active' : ''}"
                                       href="#"
                                       id="statisticDropdown"
                                       role="button"
                                       data-toggle="dropdown"
                                       aria-haspopup="true"
                                       aria-expanded="false">
                                        Отчёты
                                    </a>

                                    <div class="dropdown-menu" aria-labelledby="statisticDropdown">
                                        <g:each in="${StatisticController.availableActions}" var="action">
                                            <g:if test="${!StatisticController.actionsWithoutId.contains(action.key) && request.drawing}">
                                                <a class="dropdown-item"
                                                   href="${createLink(controller: 'statistic', action: action.key, id: request.drawing.number)}">${action.value}</a>
                                            </g:if>
                                            <g:if test="${StatisticController.actionsWithoutId.contains(action.key)}">
                                                <a class="dropdown-item"
                                                   href="${createLink(controller: 'statistic', action: action.key)}">${action.value}</a>
                                            </g:if>
                                        </g:each>
                                    </div>
                                </li>
                            </g:if>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle"
                                   href="#"
                                   id="chartDropdown"
                                   role="button"
                                   data-toggle="dropdown"
                                   aria-haspopup="true"
                                   aria-expanded="false">
                                    Графики
                                </a>

                                <div class="dropdown-menu" aria-labelledby="chartDropdown">
                                    <g:if test="${request.drawing}">
                                        <a class="dropdown-item"
                                           href="${createLink(controller: 'drawing', action: 'stakeDictChart',
                                                              id: request.drawing.number)}">Распределения ставок</a>
                                        <a class="dropdown-item"
                                           href="${createLink(controller: 'drawing', action: 'chart',
                                                              id: request.drawing.number)}">Ставок</a>
                                        <a class="dropdown-item"
                                           href="${createLink(controller: 'drawing', action: 'chart',
                                                              id: request.drawing.number,
                                                              params: [dataType: 'Probability'])}">Вероятностей</a>
                                    </g:if>
                                </div>
                            </li>
                            <g:if test="${request.drawing && request.drawing.state < 3}">
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle"
                                       href="#"
                                       id="averageDropdown"
                                       role="button"
                                       data-toggle="dropdown"
                                       aria-haspopup="true"
                                       aria-expanded="false">
                                        Варианты выигрыша
                                    </a>

                                    <div class="dropdown-menu" aria-labelledby="averageDropdown">
                                        <a class="dropdown-item"
                                           href="${createLink(controller: 'drawing', action: 'selection', id: request.drawing.number)}">Подобрать</a>
                                        <a class="dropdown-item"
                                           href="${createLink(controller: 'drawing', action: 'intellectual', id: request.drawing.number)}">Интеллектуальные</a>
                                        <a class="dropdown-item"
                                           href="${createLink(controller: 'drawing', action: 'average', id: request.drawing.number)}">Случайные</a>
                                        <a class="dropdown-item"
                                           href="${createLink(controller: 'drawing', action: 'average', id: request.drawing.number, params: [count: true])}">Средние</a>
                                    </div>
                                </li>
                            </g:if>
                        </g:if>
                    </ul>
                    <ul class="navbar-nav order-2">
                        <g:if test="${session.user}">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle"
                                   href="#"
                                   id="userDropdown"
                                   role="button"
                                   data-toggle="dropdown"
                                   aria-haspopup="true"
                                   aria-expanded="false">
                                    ${session.user.login}
                                </a>

                                <div class="dropdown-menu" aria-labelledby="userDropdown">
                                    <a class="nav-link"
                                       href="${createLink(controller: 'user', action: 'operations')}">Ставки</a>
                                    <a class="nav-link"
                                       href="${createLink(controller: 'user', action: 'settings')}">Настройки</a>
                                    <a class="nav-link"
                                       href="${createLink(controller: 'user', action: 'autoRegisterParams')}">Параметры автоставок</a>
                                    <a class="nav-link"
                                       href="${createLink(controller: 'user', action: 'changePassword')}">Сменить пароль</a>

                                    <div class="dropdown-divider"></div>
                                    <a class="nav-link"
                                       href="${createLink(controller: 'user', action: 'logout')}">Выход</a>
                                </div>
                            </li>
                        </g:if>
                        <g:else>
                            <li class="nav-item">
                                <a class="nav-link ${actionName == 'login' ? 'active' : ''}"
                                   href="${createLink(controller: 'user', action: 'login')}">Вход</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Регистрация</a>
                            </li>
                        </g:else>
                    </ul>
                    <g:if test="${session.user?.isAuth()}">
                        <ul class="navbar-nav order-3 user-balance" onclick="updateBalance()">
                            <li class="nav-item">Баланс: <span id="user-balance" style="font-weight: bold;"></span>
                            </li>
                        </ul>
                    </g:if>
                </div>
            </div>
        </nav>
        <div style="padding-top: 60px;min-height: calc(100vh - 60px);">
            <g:if test="${request.drawing}">
                <nav>
                    <div class="text-center">
                        <div class="text-primary">Тираж</div>
                    </div>
                    <ul class="pagination justify-content-center">
                        <g:set var="currentIndex"
                               value="${request.drawingNumbers?.indexOf(request.drawing.number)}"/>
                        <li class="page-item">
                            <g:link class="page-link"
                                    action="${actionName}"
                                    id="${request.drawingNumbers[currentIndex + 1]}"
                                    params="${params}">&laquo;</g:link>
                        </li>
                        <li class="page-item">
                            <g:link class="page-link"
                                    controller="drawing"
                                    action="view"
                                    id="${request.drawing.number}">${request.drawing.number}</g:link>
                        </li>
                        <li class="page-item ${request.drawing.state == 2 ? 'disabled' : ''}">
                            <g:link class="page-link"
                                    action="${actionName}"
                                    id="${request.drawingNumbers[currentIndex - 1]}"
                                    params="${params}">&raquo;</g:link>
                        </li>
                    </ul>
                </nav>
            </g:if>
            <div class="container-fluid">
                <g:layoutBody/>
            </div>
        </div>
        <div class="footer bg-dark text-light">
            <div class="row w-100">
                <div class="col"></div>
                <g:set var="currenctYear" value="${Calendar.getInstance().get(Calendar.YEAR)}"/>
                <div class="col-md-auto text-center">2018${currenctYear > 2018 ? ' - ' + currenctYear : ''}</div>

                <div class="col text-right">v${grailsApplication.metadata.getApplicationVersion()}</div>
            </div>
        </div>
    </body>
</html>
