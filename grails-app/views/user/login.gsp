<%--
  Created by IntelliJ IDEA.
  User: sa
  Date: 21.10.18
  Time: 14:11
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Авторизация</title>
    <meta name="layout" content="main">
    <style type="text/css">
        .form-signin {
            width: 100%;
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
        }
        .form-signin input[type="email"] {
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }
        .form-signin .form-control {
            position: relative;
            box-sizing: border-box;
            height: auto;
            padding: 10px;
            font-size: 16px;
        }
    </style>
</head>

<body class="text-center">
    <g:form class="form-signin" action="login" method="post">
        <g:field class="form-control" type="text" name="login" required="true" placeholder="Логин"/>
        <g:field class="form-control" type="password" name="password" required="true" placeholder="Пароль"/>
        <g:submitButton class="btn btn-lg btn-primary btn-block" name="submit" value="Войти"/>
    </g:form>
</body>
</html>