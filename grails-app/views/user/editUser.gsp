<%--
  Created by IntelliJ IDEA.
  User: sa
  Date: 12.02.19
  Time: 22:41
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>${user.id ? 'Редактирование' : 'Добавление'} параметра автоставок</title>
    <meta name="layout" content="main">
</head>

<body>
    <g:form action="saveUser" method="post" id="${user.id}">
        <div class="form-group row align-items-center">
            <div class="col-12">
                <div class="col-lg-3 col-md-6 text-center">Логин</div>
                <div class="col-lg-3 col-md-6 input-group">
                    <g:textField class="form-control" name="login" type="text" value="${user.login}"/>
                </div>
                <div class="col-lg-3 col-md-6 text-center">Пароль</div>
                <div class="col-lg-3 col-md-6 input-group">
                    <g:textField class="form-control" name="password" type="password" value=""/>
                </div>
                <div class="col-lg-3 col-md-6 text-center">id клиента</div>
                <div class="col-lg-3 col-md-6 input-group">
                    <g:textField class="form-control" name="clientId" type="text" value="${user.id ? user.data.clientId : ''}"/>
                </div>
                <div class="col-lg-3 col-md-6 text-center">id сессии</div>
                <div class="col-lg-3 col-md-6 input-group">
                    <g:textField class="form-control" name="sessionId" type="text" value="${user.id ? user.data.sessionId : ''}"/>
                </div>
            </div>
        </div>
        <g:submitButton class="btn btn-success " name="save" value="Сохранить"/>
    </g:form>
</body>
</html>