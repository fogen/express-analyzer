<%--
  Created by IntelliJ IDEA.
  User: semenuk
  Date: 18.01.19
  Time: 17:04
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>${autoRegisterParams.id ? 'Редактирование' : 'Добавление'} параметра автоставок</title>
        <meta name="layout" content="main">

        <script type="text/javascript">
            $(document).ready(function () {
                $("#intellectual").change(function () {
                    if ($(this).prop("checked")) {
                        $("#not-intellectual").hide();
                        $("#monthCount").show();
                        $("#minWinCount").show();

                    } else {
                        $("#not-intellectual").show();
                        $("#monthCount").hide();
                        $("#minWinCount").hide();
                    }
                }).change();

                $("#autoExclude").change(function () {
                    if ($(this).prop("checked")) {
                        $("#iterations").show();
                    } else {
                        $("#iterations").hide();
                    }
                }).change();

                $("#allDrawings").change(function () {
                    if ($(this).prop("checked")) {
                        $("#drawingNumber").val("");
                        $(".drawingNumber").hide();
                    } else {
                        $("#drawingNumber").val("${currentDrawingNumber}");
                        $(".drawingNumber").show();
                    }
                }).change();
            });
        </script>

    </head>

    <body>
        <g:form action="saveAutoRegisterParams" id="${autoRegisterParams.id}">
            <g:hiddenField name="id" value="${autoRegisterParams.id}" />
            <div class="form-group row align-items-center">
                <div class="col-lg-6 col-md-12">
                    <div class="text-center">Холд</div>
                    <table width="100%" style="table-layout: fixed;">
                        <thead>
                            <tr>
                                <g:each in="${(1..15)}" var="i">
                                    <th class="text-center">${i}</th>
                                </g:each>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <g:each in="${(0..14)}" var="i">
                                    <td class="text-center">
                                        <g:select class="custom-select" name="mask" from="${["1", "X", "2"]}" noSelection="${["_": ""]}" value="${autoRegisterParams.mask ? autoRegisterParams.mask[i] : '_'}"/>
                                    </td>
                                </g:each>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="text-center">Исключения</div>
                    <table width="100%" style="table-layout: fixed;">
                        <thead>
                            <tr>
                                <g:each in="${(1..15)}" var="i">
                                    <th class="text-center">${i}</th>
                                </g:each>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <g:each in="${(0..14)}" var="i">
                                    <td class="text-center">
                                        <g:select class="custom-select" name="excludes" from="${["1", "X", "2"]}" noSelection="${["_": ""]}" value="${autoRegisterParams.excludes ? autoRegisterParams.excludes[i] : '_'}"/>
                                    </td>
                                </g:each>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-12">
                    <div class="col-lg-3 col-md-6">
                        <g:checkBox id="intellectual" name="intellectual" value="${autoRegisterParams.intellectual}" />
                        <label>Интеллектуальный режим</label>
                    </div>
                    <div id="monthCount" class="col-lg-3 col-md-6">
                        <div class="text-center">Кол-во месяцев для сбора статистики</div>
                        <div class="input-group">
                            <g:textField class="form-control" name="monthCount" type="text" value="${autoRegisterParams.monthCount}"/>
                        </div>
                    </div>
                    <div id="minWinCount" class="col-lg-3 col-md-6">
                        <div class="text-center">Кол-во угаданных</div>
                        <div class="input-group">
                            <g:textField class="form-control" name="minWinCount" type="text" value="${autoRegisterParams.minWinCount}"/>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="col-lg-3 col-md-6">
                        <g:checkBox id="allDrawings" name="allDrawings" value="${!autoRegisterParams.drawingNumber}"/>
                        <label>Все тиражи</label>
                    </div>
                </div>
                <div class="col-12">
                    <div class="col-lg-3 col-md-6">
                        <g:checkBox id="onlyUnique" name="onlyUnique" value="${autoRegisterParams.onlyUnique}"/>
                        <label>Только уникальные</label>
                    </div>
                </div>
                <div class="col-12 drawingNumber">
                    <div class="col-lg-3 col-md-6 text-center">Тираж</div>
                    <div class="col-lg-3 col-md-6 input-group">
                        <g:textField class="form-control" name="drawingNumber" type="text" value="${autoRegisterParams.drawingNumber}"/>
                    </div>
                </div>
                <div class="col-12">
                    <div class="col-lg-3 col-md-6">
                        <g:checkBox id="autoExclude" name="autoExclude" value="${autoRegisterParams.autoExclude}" />
                        <label>Авто исключения</label>
                        <div id="iterations">
                            <div class="input-group">
                                <div class="text-center">Кол-во итераций</div>
                                <g:textField class="form-control" name="iterations" type="text" value="${autoRegisterParams.iterations ?: 1}"/>
                            </div>
                            <div class="input-group">
                                <div class="text-center">Кол-во купонов в каждой итерации</div>
                                <g:textField class="form-control" name="packSize" type="text" value="${autoRegisterParams.packSize ?: 10}"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="not-intellectual" class="col-12">
                    <div class="col-lg-3 col-md-6">
                        <div class="text-center">13 совпадений</div>
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">с</span></div>
                            <g:textField class="form-control" name="from13" type="text" value="${autoRegisterParams.from13}"/>
                            <div class="input-group-prepend"><span class="input-group-text">по</span></div>
                            <g:textField class="form-control" name="to13" type="text" value="${autoRegisterParams.to13}"/>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="text-center">14 совпадений</div>
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">с</span></div>
                            <g:textField class="form-control" name="from14" type="text" value="${autoRegisterParams.from14}"/>
                            <div class="input-group-prepend"><span class="input-group-text">по</span></div>
                            <g:textField class="form-control" name="to14" type="text" value="${autoRegisterParams.to14}"/>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="text-center">%</div>
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">с</span></div>
                            <g:textField class="form-control" name="winnersMin" type="text" value="${autoRegisterParams.winnersMin}"/>
                            <div class="input-group-prepend"><span class="input-group-text">по</span></div>
                            <g:textField class="form-control" name="winnersMax" type="text" value="${autoRegisterParams.winnersMax}"/>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="col-lg-3 col-md-6">
                        <div class="text-center">Количество купонов</div>
                        <div class="input-group">
                            <g:textField class="form-control" name="count" type="text" value="${autoRegisterParams.count}"/>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="text-center">Минут до окончания</div>
                        <div class="input-group">
                            <g:textField class="form-control" name="minutesBeforeEnd" type="text" value="${autoRegisterParams.minutesBeforeEnd}"/>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="text-center">Примечание</div>
                        <div class="input-group">
                            <g:textField class="form-control" name="comment" type="text" value="${autoRegisterParams.comment}"/>
                        </div>
                    </div>
                </div>
            </div>
            <g:submitButton class="btn btn-success " name="save" value="Сохранить"/>
        </g:form>
    </body>
</html>