<%--
  Created by IntelliJ IDEA.
  User: semenuk
  Date: 07.12.18
  Time: 14:32
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>Настройки</title>
        <meta name="layout" content="main">
    </head>

    <body>
        <g:if test="${flash.message}">
            <div class="alert alert-info">
                <g:message message="${flash.message}"/>
            </div>
        </g:if>
        <g:form action="save">
            <div class="form-group">
                <label for="sessionId">Сессия fonbet</label>
                <input class="form-control" id="sessionId" name="sessionId" value="${userData.sessionId}"/>
            </div>
            <div class="form-group">
                <label for="clientId">ID Клиента fonbet</label>
                <input class="form-control" id="clientId" name="clientId" value="${userData.clientId}"/>
            </div>
            <div class="form-group">
                <label for="clientId">Telegram chat id</label>
                <input class="form-control" id="telegramChatId" name="telegramChatId" value="${userData.telegramChatId}"/>
            </div>
            <g:submitButton class="btn btn-success " name="save" value="Сохранить"/>
        </g:form>
    </body>
</html>