<%--
  Created by IntelliJ IDEA.
  User: sa
  Date: 21.10.18
  Time: 14:35
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>Смена пароля</title>
        <meta name="layout" content="main">
    </head>

    <body>
        <div class="col-lg-4">
            <g:form action="changePassword" method="post">
                <table class="table">
                    <tr>
                        <td>Старый пароль:</td>
                        <td><g:passwordField name="oldPassword" required="true"/></td>
                    </tr>
                    <tr>
                        <td>Новый пароль:</td>
                        <td><g:passwordField name="newPassword1" required="true"/></td>
                    </tr>
                    <tr>
                        <td>Повтор:</td>
                        <td><g:passwordField name="newPassword2" required="true"/></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-center"><g:submitButton name="submit" value="Сменить" required="true"/></td>
                    </tr>
                </table>

            </g:form>
        </div>
    </body>
</html>