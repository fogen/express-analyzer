<%--
  Created by IntelliJ IDEA.
  User: semenuk
  Date: 07.12.18
  Time: 16:25
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>Последние операции</title>
        <meta name="layout" content="main">
    </head>

    <body>
        <g:if test="${!operations}">
            <div class="alert alert-info">Операций нет</div>
        </g:if>
        <g:else>
            <table class="table table-striped text-center">
                <thead>
                    <tr>
                        <td>Тираж</td>
                        <td>Купон</td>
                        <td>Сумма</td>
                        <td>Код</td>
                        <td>Совпадений</td>
                        <td>Примечание</td>
                    </tr>
                </thead>
                <tbody>
                    <g:each in="${operations}" var="operation">
                        <tr>
                            <td rowspan="${operation.value.size() + 1}">
                                <g:link controller="drawing"
                                        action="view"
                                        id="${operation.key}">${operation.key}</g:link> (${operation.value.count { it.sum < 0 }})
                            </td>
                        </tr>
                        <g:each in="${operation.value}" var="value">
                            <tr>
                                <td>${value.couponId}</td>
                                <td>${value.sum}</td>
                                <td class="text-monospace">
                                    <%=  value.code %>
                                    <g:link controller="drawing"
                                            action="view"
                                            params="${[stakePos: value.simpleCode.toList()]}"
                                            id="${operation.key}"
                                            target="_blank"
                                            title="Показать график">&#128480;</g:link>
                                    <a href="http://toto-info.co/#/${operation.key}/${value.couponId}"
                                       title="Посмотреть расчёт на toto-info"
                                       target="_blank">&#128270;</a>
                                </td>
                                <td>${value.matchCount}</td>
                                <td>${value.comment}</td>
                            </tr>
                        </g:each>
                    </g:each>
                </tbody>
            </table>
        </g:else>
    </body>
</html>