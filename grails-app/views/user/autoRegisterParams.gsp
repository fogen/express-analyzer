<%--
  Created by IntelliJ IDEA.
  User: semenuk
  Date: 16.01.19
  Time: 16:43
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>Параметры автоставок</title>
        <meta name="layout" content="main">
    </head>

    <body>
        <g:if test="${flash.message}">
            <div class="alert alert-info">
                <g:message message="${flash.message}"/>
            </div>
        </g:if>
        <g:if test="${autoRegisterParams}">
            <g:form action="changeAutoRegisterCouponsEnabled">
                <g:if test="${session.user.data.autoRegisterCouponsEnabled}">
                    <g:hiddenField name="flag" value=""/>
                    <g:submitButton name="autoRegisterCouponsEnabled" value="Выключить"/>
                </g:if>
                <g:else>
                    <g:hiddenField name="flag" value="1"/>
                    <g:submitButton name="autoRegisterCouponsEnabled" value="Включить"/>
                </g:else>
            </g:form>
        </g:if>
        <table class="table-bordered table-striped text-center" width="100%">
            <thead>
                <tr>
                    <th>Кол-во</th>
                    <th>Минут до конца</th>
                    <th>13 совпадений</th>
                    <th>14 совпадений</th>
                    <th>Процентный период</th>
                    <th>Уникальные</th>
                    <th>Маска</th>
                    <th>Тираж</th>
                    <th>Примечание</th>
                    <th>Действия</th>
                </tr>
            </thead>
            <tbody>
                <g:each in="${autoRegisterParams}" var="autoRegisterParam">
                    <tr>
                        <td>${autoRegisterParam.count}</td>
                        <td>${autoRegisterParam.minutesBeforeEnd}</td>
                        <g:if test="${autoRegisterParam.intellectual}">
                            <td colspan="3">Интелектуальный режим (анализ последних ${autoRegisterParam.monthCount} мес.)</td>
                        </g:if>
                        <g:else>
                            <td>${autoRegisterParam.from13} - ${autoRegisterParam.to13}</td>
                            <td>${autoRegisterParam.from14} - ${autoRegisterParam.to14}</td>
                            <td>${autoRegisterParam.winnersMin} - ${autoRegisterParam.winnersMax}</td>
                        </g:else>
                        <td>${autoRegisterParam.onlyUnique ? 'Да' : 'Нет'}</td>
                        <td>${autoRegisterParam.mask ?: ''}</td>
                        <td>${autoRegisterParam.drawingNumber ?: 'Все'}</td>
                        <td>${autoRegisterParam.comment}</td>
                        <td>
                            <g:link action="editAutoRegisterParams" id="${autoRegisterParam.id}">Изменить</g:link>
                            <g:link action="deleteAutoRegisterParams" id="${autoRegisterParam.id}">Удалить</g:link>
                        </td>
                    </tr>
                </g:each>
            </tbody>
        </table>
        <g:link action="editAutoRegisterParams">Добавить</g:link>
    </body>
</html>