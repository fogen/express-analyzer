<%--
  Created by IntelliJ IDEA.
  User: sa
  Date: 12.02.19
  Time: 22:36
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Список пользователей</title>
    <meta name="layout" content="main">
</head>

<body>
    <g:if test="${flash.message}">
        <div class="alert alert-info">
            <g:message message="${flash.message}"/>
        </div>
    </g:if>
    <table class="table-bordered table-striped text-center" width="100%">
        <thead>
            <tr>
                <th>Логин</th>
                <th>Пароль</th>
                <th>telegram chat id</th>
                <th>id клиента</th>
                <th>id сессии</th>
                <th>Действия</th>
            </tr>
        </thead>
        <tbody>
            <g:each in="${users}" var="user">
                <tr>
                    <td>${user.login}</td>
                    <td>${user.passwd}</td>
                    <td>${user.data.telegramChatId ?: ''}</td>
                    <td>${user.data.clientId ?: ''}</td>
                    <td>${user.data.sessionId ?: ''}</td>
                    <td>
                        <g:link action="editUser" id="${user.id}">Изменить</g:link>
                        <g:link action="deleteUser" id="${user.id}">Удалить</g:link>
                    </td>
                </tr>
            </g:each>
        </tbody>
    </table>
    <g:link action="editUser">Добавить</g:link>
</body>
</html>