package express

class StakeDict {

    Integer drawingId
    Date drawingDate
    String code
    Integer count = 1
    Integer winCount
    BigDecimal winnersAverage
    BigDecimal poolAverage
    BigDecimal lineAverage

    static constraints = {
        code maxSize: 15, unique: 'drawingDate'
        winCount nullable: true
        drawingDate nullable: true
        winnersAverage nullable: true
        poolAverage nullable: true
        lineAverage nullable: true
    }

}
