package express

class AutoRegisterCouponsParams {

    Integer drawingNumber
    Boolean intellectual = true
    Integer count = 10
    Integer winnersMin = 100
    Integer winnersMax = 110
    Integer from13 = 1
    Integer to13 = 50
    Integer from14 = 1
    Integer to14 = 5
    Integer minutesBeforeEnd = 15
    Integer monthCount = 2
    Boolean autoExclude = true
    Integer iterations = 1
    Integer packSize = 10
    Integer minWinCount = 13
    Boolean onlyUnique = true

    String comment = "Авто"

    String mask
    String excludes

    static belongsTo = [userData: UserData]

    static constraints = {
        drawingNumber nullable: true
        mask nullable: true
        excludes nullable: true
        minutesBeforeEnd nullable: true
        comment nullable: true
        onlyUnique nullable: true
        monthCount nullable: true
        autoExclude nullable: true
        iterations nullable: true
        packSize nullable: true
        minWinCount nullable: true
    }

}
