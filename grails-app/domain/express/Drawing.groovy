package express

import grails.converters.JSON

class Drawing {

    Integer number
    Long ver
    Integer state

    BigDecimal complexity
    BigDecimal pool
    BigDecimal jackpot
    Integer couponCount
    Integer stakeCount
    Integer optionCount
    Date expired
    String winnersCode

    String resultsData
    List results = []

    static hasMany = [events: Event]

    static transients = [
        'needFillWinnersCode',
        'results',
        'maxCode',
        'minCode'
    ]

    static constraints = {
        winnersCode nullable: true
        optionCount nullable: true
        resultsData nullable: true, maxSize: 4096
    }

    static mapping = {
        events sort: 'sortOrder'
    }

    Boolean isNeedFillWinnersCode() {
        state == 1 && !winnersCode
    }

    def beforeUpdate() {
        resultsData = results as JSON
    }

    def beforeInsert() {
        resultsData = results as JSON
    }

    def afterLoad() {
        results = resultsData ? JSON.parse(resultsData) as List : []
    }

    def getEventsResultCode() {

        def result = ""

        events.each {
            result += it.resultCode ?: "_"
        }

        result
    }

    String getMaxCode() {

        def code = ""

        events.each { event ->
            def isProbabilitiesDifferent = [event.drawProbability, event.win1Probability, event.win2Probability].unique().size() == 3
            def max = isProbabilitiesDifferent ? [event.drawProbability, event.win1Probability, event.win2Probability].max() : [event.drawPercentage, event.win1Percentage, event.win2Percentage].max()

            switch (max) {
                case { isProbabilitiesDifferent && it == event.win1Probability || it == event.win1Percentage }:
                    code += "1"
                    break
                case { isProbabilitiesDifferent && it == event.win2Probability || it == event.win2Percentage }:
                    code += "2"
                    break
                default:
                    code += "X"
            }
        }

        code
    }

    String getMinCode() {

        def code = ""

        events.each { event ->
            def isProbabilitiesDifferent = [event.drawProbability, event.win1Probability, event.win2Probability].unique().size() == 3
            def min = isProbabilitiesDifferent ? [event.drawProbability, event.win1Probability, event.win2Probability].min() : [event.drawPercentage, event.win1Percentage, event.win2Percentage].min()

            switch (min) {
                case { isProbabilitiesDifferent && it == event.win1Probability || it == event.win1Percentage }:
                    code += "1"
                    break
                case { isProbabilitiesDifferent && it == event.win2Probability || it == event.win2Percentage }:
                    code += "2"
                    break
                default:
                    code += "X"
            }
        }

        code
    }
}
