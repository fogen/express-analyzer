package express

class RegisteredCoupon {

    Integer drawingNumber
    String code
    String regId

    String comment

    User user

    static constraints = {
        comment nullable: true
    }
}
