package express

class AverageCodeCacheArchive {

    Integer drawingNumber
    String code
    Integer winnersCount
    Boolean shown
    BigDecimal average
    String interval

    static constraints = {
        interval nullable: true
    }

}
