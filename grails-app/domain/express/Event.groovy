package express

class Event {

    Integer extId
    String name
    Integer sortOrder
    Date date
    String resultCode
    String score

    BigDecimal drawPercentage
    BigDecimal win1Percentage
    BigDecimal win2Percentage

    BigDecimal drawProbability
    BigDecimal win1Probability
    BigDecimal win2Probability

    static belongsTo = [drawing: Drawing]

    static transients = ['winnerPercentage', 'winnerProbability']

    static constraints = {
        resultCode nullable: true
        score nullable: true
    }

    def getNameWithResult() {
        name + ", " + DateUtils.formatDate(date) + (resultCode ? ', Результат: ' + resultCode : '')
    }

    BigDecimal getWinnerPercentage(String winnerCode) {
        switch (winnerCode) {
            case "1": return win1Percentage
            case "2": return win2Percentage
            case "X": return drawPercentage
        }
    }

    BigDecimal getWinnerProbability(String winnerCode) {
        switch (winnerCode) {
            case "1": return win1Probability
            case "2": return win2Probability
            case "X": return drawProbability
        }
    }
}
