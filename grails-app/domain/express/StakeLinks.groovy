package express

import grails.converters.JSON


class StakeLinks {

    Integer drawingNumber
    Integer total
    BigDecimal average
    String winnersCode
    BigDecimal winnersAverage
    String linksData
    Date drawingDate

    List data = []

    static constraints = {
        linksData nullable: true, maxSize: 4096
        winnersCode nullable: true
        winnersAverage nullable: true
        drawingDate nullable: true
    }

    static transients = ['data']

    def beforeUpdate() {
        linksData = data as JSON
    }

    def beforeInsert() {
        linksData = data as JSON
    }

    def afterLoad() {
        data = JSON.parse(linksData) as List
    }

}
