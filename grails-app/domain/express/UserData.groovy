package express

import grails.util.Holders

import java.math.RoundingMode
import java.text.NumberFormat


class UserData {

    Long clientId
    String sessionId

    BigDecimal balance = BigDecimal.ZERO
    Boolean autoRegisterCouponsEnabled = false

    String telegramChatId

    static belongsTo = [user: User]

    static hasMany = [autoRegisterParams: AutoRegisterCouponsParams]

    static transients = ['fsid']

    static constraints = {
        clientId nullable: true
        sessionId nullable: true
        autoRegisterCouponsEnabled nullable: true
        telegramChatId nullable: true
    }

    def beforeUpdate() {

        def oldBalance = getPersistentValue('balance')

        if (oldBalance != balance) {
            def message = "Изменился баланс:\n<b>" +
                NumberFormat.numberInstance.format(oldBalance.setScale(0, RoundingMode.HALF_UP)) + " => " +
                NumberFormat.numberInstance.format(balance.setScale(0, RoundingMode.HALF_UP)) + "</b>"

            Holders.grailsApplication.mainContext.getBean("telegramService").sendMessage(user, message)
        }
    }

    String getFsid() {

        sessionId

/*        if (!sessionId) {
            return
        }

        sessionId.endsWith("$clientId") ? sessionId : sessionId + "@$clientId"*/
    }
}
