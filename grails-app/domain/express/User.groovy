package express

import grails.util.Holders

class User {

    String login
    String passwd
    Boolean needChangePassword = true
    Boolean admin = false

    static transients = ['password']

    static mapping = {
        table "user_info"
    }

    static constraints = {
        admin nullable: true
    }

    static SALT = Holders.config.express?.security?.salt

    void setPassword(String newPassword) {
        passwd = (newPassword + SALT).encodeAsSHA1()
    }

    boolean checkPassword(String password) {

        if (!passwd) {
            log.warn("User ${id} has empty password")
        }

        if (!passwd || !password) {
            return false
        }

        passwd == (password + SALT).encodeAsSHA1()
    }

    def isAuth() {
        data.sessionId && data.clientId
    }

    def canRegisterCoupon() {
        isAuth() && data.balance >= 50
    }

    def getData() {
        UserData.findByUser(this) ?: new UserData(user: this)
    }
}
