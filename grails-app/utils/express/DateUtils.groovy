package express

import java.text.SimpleDateFormat

/**
 *
 * @author semenuk
 */
class DateUtils {

    static Date parseDateFromJson(String value) {
        new Date(value.replaceAll('(/Date\\()(\\d{13})(\\)/)', '$2') as long)
    }

    private static get(Date date, int what) {

        Calendar cal = Calendar.getInstance()
        cal.setTime(date)
        cal.get(what)
    }

    static int getYear(Date date = new Date()) {
        get(date, Calendar.YEAR)
    }

    static int getMonth(Date date = new Date()) {
        get(date, Calendar.MONTH)
    }

    static int getDayOfMonth(Date date = new Date()) {
        get(date, Calendar.DAY_OF_MONTH)
    }

    static Date plus(int type, Integer count, Date oldDate = new Date()) {

        Calendar cal = Calendar.getInstance()
        cal.setTime(oldDate)
        cal.add(type, count)
        cal.getTime()
    }

    static Date minus(int type, Integer count, Date oldDate = new Date()) {
        plus(type, count * -1, oldDate)
    }

    static Date minusYear(Integer years, Date oldDate = new Date()) {
        plus(Calendar.YEAR, years * -1, oldDate)
    }

    static Date plusYear(Integer years, Date oldDate = new Date()) {
        plus(Calendar.YEAR, years, oldDate)
    }

    static Date minusHours(Integer hours, Date oldDate = new Date()) {
        plus(Calendar.HOUR_OF_DAY, hours * -1, oldDate)
    }

    static Date plusHours(Integer hours, Date oldDate = new Date()) {
        plus(Calendar.HOUR_OF_DAY, hours, oldDate)
    }

    static Date minusMinutes(Integer minutes, Date oldDate = new Date()) {
        plus(Calendar.MINUTE, minutes * -1, oldDate)
    }

    static Date plusMinutes(Integer minutes, Date oldDate = new Date()) {
        plus(Calendar.MINUTE, minutes, oldDate)
    }

    static Date minusMonths(Integer months, Date oldDate = new Date()) {
        plus(Calendar.MONTH, months * -1, oldDate)
    }

    static Date plusMonths(Integer months, Date oldDate = new Date()) {
        plus(Calendar.MONTH, months, oldDate)
    }

    static String formatDate(Date date, String format = "dd.MM.yy HH:mm") {
        new SimpleDateFormat(format).format(date)
    }
}
