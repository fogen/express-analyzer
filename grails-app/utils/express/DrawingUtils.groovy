package express

class DrawingUtils {

    static VARIANTS = ['1', 'X', '2']

    static String nextMask(String value, int position = 1) {

        if (value[-position] == VARIANTS.last()) {
            value = value.substring(0, value.length() - position) + VARIANTS.first() * position
            return value.length() == position ? value : nextMask(value, position + 1)
        }

        value.substring(0, value.length() - position) + VARIANTS.get(VARIANTS.indexOf(value[-position]) + 1) + VARIANTS.first() * (position - 1)
    }

    static String formatCode(String code) {
        code[0..4] + "-" + code[5..9] + "-" + code[10..14]
    }
}
