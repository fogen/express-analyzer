package express

import grails.util.Holders
import org.grails.plugins.web.taglib.ApplicationTagLib
import org.springframework.web.context.request.RequestContextHolder

class AppUtils {

    static getG() {
        Holders.applicationContext.getBean(ApplicationTagLib)
    }

    static getRequestAttributes() {
        RequestContextHolder.requestAttributes
    }

    static getSession() {
        request.session
    }

    static getRequest() {
        requestAttributes.currentRequest
    }

    static getParams() {
        requestAttributes.params
    }

    static getResponse() {
        requestAttributes.currentResponse
    }

}
