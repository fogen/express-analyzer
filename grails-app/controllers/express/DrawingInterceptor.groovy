package express

class DrawingInterceptor {

    DrawingService drawingService

    boolean before() {

        params.id = params.number ? params.number as Integer : params.id ? params.id as Integer : null

        if (actionName && !DrawingController.actionsWithoutId.contains(actionName) && !params.id) {
            request.drawing = null
            redirect action: 'index'
            return false
        }

        if (params.id) {
            request.drawing = drawingService.getDrawing(params.id, true)

            if (!request.drawing) {
                return false
            }

            request.drawingNumbers = drawingService.allDrawingNumbers
        }

        true
    }

    boolean after() { true }

    void afterView() {
        // ..
    }
}
