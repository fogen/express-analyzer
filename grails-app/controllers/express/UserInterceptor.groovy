package express

class UserInterceptor {

    private static ADMIN_ACTIONS = ['list', 'editUser', 'saveUser']

    boolean before() {

        if (actionName in ['login', 'logout']) {
            return true
        }

        if (actionName in ADMIN_ACTIONS && !session.user.admin) {
            redirect controller: 'drawing'
            return
        }

        true
    }

    boolean after() { true }

    void afterView() {
        //
    }
}
