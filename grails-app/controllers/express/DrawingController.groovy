package express

import grails.converters.JSON

import org.xhtmlrenderer.simple.ImageRenderer

import javax.imageio.ImageIO
import java.awt.image.BufferedImage
import java.math.RoundingMode


class DrawingController {

    DrawingService drawingService
    HttpClientService httpClientService
    UserService userService

    static getActionsWithoutId() {
        ['index', 'eventResultsChart', 'statistic', 'finished']
    }

    def index() {

        [drawings: drawingService.getLastDrawings()]
    }

    def view() {

        if (!request.drawing) {
            redirect action: 'index'
            return
        }

        if (params.reset) {
            params.winCount = 0
            params.stakePos = []
        }

        if (params.nextLink && params.stakePos && !params.stakePos.any { it == '_' }) {
            def parts = params.nextLink.split("_")
            params.stakePos = drawingService.getNextCode(request.drawing, params.stakePos.join(), parts[1] as Integer, parts[0] == "down").toList()
        }

        def stakes = []
        def codeLike = request.drawing.state == 1 ? request.drawing.winnersCode : ""
        def stakePos = []
        def nonexistentStake = []
        def fillParam = params.find { it.key.startsWith("fill") }

        if (fillParam || params.findStakes || params.stakePos.any { it != '_' }) {

            switch (fillParam?.key) {
                case "fillWinners":
                    codeLike = request.drawing.getEventsResultCode()
                    break
                case "fillMinimal":
                    codeLike = drawingService.getMinimalStakeLinkCode(request.drawing)
                    break
                case "fillRandom":
                    codeLike = drawingService.getRandomCode(params.stakePos.any { it == '_' } ? params.stakePos.join() : null)
                    break
                default:
                    codeLike = params.stakePos.join()
            }

            stakes = StakeDict.withCriteria {
                eq("drawingDate", request.drawing.expired)
                eq("count", 1)
                like("code", codeLike)

                if (params.from && params.to) {
                    between("winnersAverage", new BigDecimal(params.from), new BigDecimal(params.to))
                }

                projections {
                    property("code")
                }
            }

            if (stakes && codeLike.contains("_")) {
                nonexistentStake = codeLike.toList()
                def indexes = []
                def idx = -1

                while ((idx = codeLike.indexOf("_", idx + 1)) > -1) {
                    indexes << idx
                }

                indexes.each { i ->
                    def uniq = stakes*.getAt(i).unique()
                    nonexistentStake[i] = (['1', 'X', '2'] - uniq).join()
                }
            }
        }

        if (!params.reset) {
            stakePos = codeLike?.toList() ?: []
        }

        def uniqueStakes

        if (params.winCount) {
            uniqueStakes = drawingService.countUniqueStakes(request.drawing, params.winCount as Integer, params.stakePos.join())
        } else {
            uniqueStakes = drawingService.getUniqueStakes(request.drawing, codeLike?.toList())
        }

        params.showOperations = params.showOperations || request.drawing.state == 3 || request.drawing.number == drawingService.lastFinishedDrawing.number

        def operations = params.showOperations ? userService.getLastOperations(session.user, request.drawing, request.drawing.state == 1, params.minMatchCount ?: 9).operations : []

        [
            events          : request.drawing.events?.sort { it.sortOrder },
            stakes          : stakes.take(20),
            nonexistentStake: nonexistentStake,
            stakePos        : stakePos,
            uniqueStakes    : uniqueStakes,
            operations      : operations,
            from            : params.from,
            to              : params.to,
            showOperations  : params.showOperations,
            chartData       : drawingService.getWinnersStakeLinksChartData(request.drawing, params.winCount ? params.stakePos.join() : codeLike) as JSON
        ]
    }

    def selection() {

        [
            stakePos : params.stakePos,
            chartData: drawingService.getWinnersStakeLinksChartData(request.drawing, params.stakePos?.join()) as JSON
        ]
    }

    def nextLinkAjax() {

        def newCode = drawingService.getNextCode(request.drawing, params.code, params.index as Integer, params.down == "true")

        render(drawingService.getWinnersStakeLinksChartData(request.drawing, newCode) as JSON)
    }

    def stakeLinksChartDataAjax() {
        render(drawingService.getWinnersStakeLinksChartData(request.drawing, params.code) as JSON)
    }

    def chart() {

        def result = drawingService.getEventChartData(request.drawing, params.dataType ?: "Percentage")

        [
            data: result,
            chartType: params.chartType ?: "Area"
        ]
    }

    def eventResultsChart() {

        [data: drawingService.getEventResultsChartData()]
    }

    def statistic() {

        [eventResults: drawingService.collectEventResults()]
    }

    def average() {

        if (!params.search) {
            return [:]
        }

        drawingService.findWinnersByAverages(request.drawing, params)
    }

    def intellectual() {

        if (!params.search) {
            return [:]
        }

        if (!params.packetSize) {
            params.averageRanges = drawingService.getIntellectualParams(params.monthCount as Integer, (params.howMany ?: 20) as Integer, (params.minWinCount as Integer ) ?: 13)
        }

        drawingService.findWinnersByAverages(request.drawing, params)
    }

    def stakeDictChart() {

        [
            chartData : drawingService.getWinnersStakeLinksChartData(request.drawing) as JSON,
            chartData2: drawingService.getWinnersStakeDictChartData(request.drawing)
        ]
    }

    def reset_average_cache() {

        drawingService.resetAverageCache(request.drawing)

        render "ok"
    }

    def getPossibleWinAmount() {

        def winAmount = drawingService.calcPossibleWinAmount(request.drawing, params.code)

        render([winAmount: g.formatNumber([number: winAmount, type: 'number'])] as JSON)
    }

    def updateStakeDict() {
        render([result: drawingService.startUpdateStakeDict(request.drawing)] as JSON)
    }

    def checkUpdateStakeDic() {

        boolean finished = request.drawing.state < 2 ?: drawingService.checkUpdateStakeDic(request.drawing)

        render([finished: finished] as JSON)
    }

    def updateAllDrawings() {
        drawingService.updateAllDrawings(true)
        render "ok"
    }

    def formatter() {

        def response = httpClientService.post(drawingService.urlPrefix + "GetDrawing", [id: request.drawing.number])?.d
        def lastChampionship
        def drawingInfo = ""
        def averages = []

        for (event in response.Details.Events) {
            def championship = event.Championships.find { it.Key == "ru" }.Value

            if (lastChampionship != championship) {
                drawingInfo += "[b]$championship[/b]<br/>"
                lastChampionship = championship
            }

            def eventDate = DateUtils.plusHours(-2, DateUtils.parseDateFromJson(event.Date))
            drawingInfo += "${event.Order + 1}. ${DateUtils.formatDate(eventDate, "dd.MM HH:mm")} ${event.Names.find { it.Key == "ru" }.Value}"

            if (event.ResultCode) {
                drawingInfo += " - [b]${event.ResultCode == "0" ? "[i]отменён[/i]" : event.Score}[/b]"
            }

            drawingInfo += "<br/>"

            averages << [
                poolX: event.UserDraw.Percentage,
                pool1: event.UserWin1.Percentage,
                pool2: event.UserWin2.Percentage,
                lineX: event.UserDraw.Probability,
                line1: event.UserWin1.Probability,
                line2: event.UserWin2.Probability
            ]
        }

        def se = SuperExpress.findByDrawingNumber(request.drawing.number)
        def codes = []

        if (params.codes) {
            params.codes.eachLine {
                codes << it.replaceAll("-", "")
            }
        }

        if (!se && !params.doFormat) {
            if (codes && params.save) {
                se = new SuperExpress(urlId: params.urlId, drawingNumber: request.drawing.number, drawingVersion: response.Version, codes: codes)
                se.save(flush: true)
            } else {

                if (!params.fullInfo) {
                    render template: "formatted_code", model: [formattedCodes: []]
                    return
                }
            }
        }

        if (!codes && se) {
            codes = se.codes
        }

        def poolData = []
        def lineData = []

        if (params.fullInfo) {
            (0..13).each { n ->
                def pool = [:]
                def line = [:]

                ["11", "1X", "12", "X1", "XX", "X2", "21", "2X", "22"].each { link ->
                    pool[link] = averages[n]."pool${link[0]}" + averages[n + 1]."pool${link[1]}"
                    line[link] = averages[n]."line${link[0]}" + averages[n + 1]."line${link[1]}"
                }

                poolData << pool
                lineData << line
            }

            if (request.drawing.state == 1 && request.drawing.winnersCode) {
                def winnersCode = request.drawing.winnersCode
                BigDecimal poolAverage = BigDecimal.ZERO
                BigDecimal lineAverage = BigDecimal.ZERO
                (0..13).each { n ->
                    poolAverage += poolData[n][winnersCode[n..n + 1]] ?: 66.67
                    lineAverage += lineData[n][winnersCode[n..n + 1]] ?: 66.67
                }
                codes << request.drawing.winnersCode
                params.average = params.fullInfo
            }
        }

        if (!codes) {
            return [drawingInfo: drawingInfo]
        }

        def formatCodesData = []

        codes.each {
            def code = it.replaceAll("-", "")
            def fCode = ""
            def bbCode = ""
            def matchCount = 15
            def needCalcAmount = false

            for (event in response.Details.Events) {
                def bet = code[event.Order]

                if (event.Order in [5, 10]) {
                    fCode += "-"
                    bbCode += "-"
                }

                def match = !event.ResultCode || event.ResultCode in ['0', bet]

                if (event.ResultCode) {
                    fCode += "<span style='color: ${match ? 'green' : 'red'}'>$bet</span>"
                    bbCode += "[color=#${match ? '26A65B' : 'D91E18'}]$bet[/color]"
                } else {
                    fCode += bet
                    bbCode += bet
                }

                if (!match) {
                    matchCount--
                }
            }

            def sum = 0

            if (needCalcAmount && matchCount > 8) {
                sum = drawingService.calcPossibleWinAmount(request.drawing, code)
            }

            def averageData = [:]

            if (params.average) {
                averageData.average = drawingService.getWinnersAverage(request.drawing, code)
                def drawingResults = drawingService.calculateCoupons(request.drawing, code)
                averageData.calc = "(13: ${drawingResults.find { it.Result == 13 }?.Count} 14: ${drawingResults.find { it.Result == 14 }?.Count} 15: ${drawingResults.find { it.Result == 15 }?.Count})"

                BigDecimal poolAverage = BigDecimal.ZERO
                BigDecimal lineAverage = BigDecimal.ZERO

                (0..13).each { n ->
                    poolAverage += poolData[n][code[n..n + 1]] ?: 66.67
                    lineAverage += lineData[n][code[n..n + 1]] ?: 66.67
                }

                averageData.poolAverage = (poolAverage  / 14 / 0.67).setScale(2, RoundingMode.HALF_UP)
                averageData.lineAverage = (lineAverage  / 14 / 0.67).setScale(2, RoundingMode.HALF_UP)
            }

            formatCodesData << averageData + [
                code      : fCode,
                bbcode    : params.bbCode ? bbCode : null,
                matchCount: matchCount,
                sum       : sum
            ]
        }

        if (params.average) {
            formatCodesData = formatCodesData.sort { -it.average }
        }

        formatCodesData = formatCodesData.groupBy { it.matchCount }.sort { -it.key }

        if (!params.fullInfo) {
            render template: "formatted_code", model: [formattedCodes: formatCodesData]
            return
        }

        [
            drawingInfo   : drawingInfo,
            formattedCodes: formatCodesData,
            averages      : params.average
        ]
    }

    def image() {

        def se = SuperExpress.withCriteria(uniqueResult: true) {
            eq("drawingNumber", request.drawing.number)

            if (params.uid) {
                eq("urlId", params.uid)
            }
        }

        if (!se) {
            redirect(action: '')
            return
        }

        def file = new File("/tmp/${se.urlId}.png")

        if (file.exists()) {
            response.setContentType("image/png")
            response.outputStream << new FileInputStream(file)
            response.outputStream.flush()
            return
        }

        BufferedImage buffer = ImageRenderer.renderToImage("http://localhost:8080/drawing/formatter/" + params.id, "/tmp/${se.urlId}.png", 200)
        OutputStream os = response.getOutputStream()
        ImageIO.write(buffer, "png", os)
        os.close()
    }

    def update() {

        render "update stake dict<br/>"

        for (drawing in drawingService.getLastDrawings(DateUtils.minusMonths((params.month as Integer) ?: 6), 1)) {
            render "checking drawing $drawing.number ... "

            if (drawingService.needUpdateStakeDict(drawing)) {
                drawingService.startUpdateStakeDict(drawing)
                render "scheduled<br/>"
            } else {
                render "not needed<br/>"
            }

        }

        render "finished"
    }

}
