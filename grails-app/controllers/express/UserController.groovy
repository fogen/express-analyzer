package express

import grails.converters.JSON


class UserController {

    static defaultAction = 'login'

    UserService userService
    DrawingService drawingService

    def login() {

        if (session.user) {
            redirect controller: 'drawing'
            return
        }

        if (params.submit && params.login && params.password) {
            def user = userService.authenticate(params.login, params.password)

            if (user) {
                session.user = user
                redirect controller: 'drawing'
            }
        }
    }

    def logout() {
        session.invalidate()
        redirect action: 'login'
    }

    def changePassword() {

        if (params.oldPassword && params.newPassword1 && params.newPassword1 == params.newPassword2) {
            if (userService.changePassword(session.user.login, params.oldPassword, params.newPassword1)) {
                session.invalidate()
                redirect action: 'login'
            }
        }
    }

    def settings() {

        [userData: session.user.data]
    }

    def save() {

        def changed = false

        if (params.sessionId != session.user.data.sessionId) {
            session.user.data.sessionId = params.sessionId
            changed = true
        }

        if (params.clientId != session.user.data.clientId) {
            session.user.data.clientId = params.clientId as Long
            changed = true
        }

        if (params.telegramChatId != session.user.data.telegramChatId) {
            session.user.data.telegramChatId = params.telegramChatId
            changed = true
        }

        if (changed) {
            session.user.data.save(flush: true)
        }

        flash.message = changed ? "Сохранено" : "Изменений нет"

        redirect action: "settings"
    }

    def operations() {

        [operations: userService.getLastOperations(session.user, null, params.onlyProfit)]
    }

    def couponRegister() {

        render (userService.registerCoupon(session.user, params.code, params.id as Integer, params.comment) as JSON)
    }

    def update_balance_ajax() {

        boolean result = userService.getSessionInfo(session.user)
        render ([balance: result ? session.user.data.balance : null] as JSON)
    }

    def autoRegisterParams() {
        [autoRegisterParams: session.user.data.autoRegisterParams?.sort { it.minutesBeforeEnd }]
    }

    def saveAutoRegisterParams() {

        def autoRegisterParams = params.id ? AutoRegisterCouponsParams.get(params.id as Long) : new AutoRegisterCouponsParams()

        autoRegisterParams.with {
            intellectual = params.intellectual as boolean
            winnersMax = params.winnersMax as Integer
            winnersMin = params.winnersMin as Integer
            from13 = params.from13 as Integer
            to13 = params.to13 as Integer
            from14 = params.from14 as Integer
            to14 = params.to14 as Integer
            mask = params.mask.any { it != "_" } ? params.mask.join() : null
            excludes = params.excludes.any { it != "_" } ? params.excludes.join() : null
            count = params.count as Integer
            minutesBeforeEnd = params.minutesBeforeEnd as Integer
            drawingNumber = !params.allDrawings && params.drawingNumber ? params.drawingNumber as Integer : null
            comment = params.comment
            onlyUnique = params.onlyUnique as Boolean
            monthCount = params.monthCount as Integer
            autoExclude = params.autoExclude as Boolean
            iterations = params.iterations as Integer
            packSize = params.packSize as Integer
            minWinCount = params.minWinCount as Integer
        }

        def message = "Сохранено"

        if (!autoRegisterParams.id) {
            session.user.data.addToAutoRegisterParams(autoRegisterParams)
            session.user.data.save(flush: true)
        }

        if (!autoRegisterParams.save(flush: true)) {
            log.error(autoRegisterParams.errors)
            message = "Ошибка сохранения"
        } else {
            userService.checkAutoRegisterCouponsParams(drawingService.currentDrawing, autoRegisterParams.id)
        }

        flash.message = message
        redirect action: 'autoRegisterParams'
    }

    def editAutoRegisterParams() {

        def autoRegisterParams = params.id ? AutoRegisterCouponsParams.get(params.id as Long) : new AutoRegisterCouponsParams()

        [
            autoRegisterParams  : autoRegisterParams,
            currentDrawingNumber: drawingService.currentDrawing?.number
        ]
    }

    def deleteAutoRegisterParams() {

        if (userService.deleteAutoRegisterParams(session.user, params.id as Long)) {
            flash.message = "Удалено"
        }

        redirect action: 'autoRegisterParams'
    }

    def changeAutoRegisterCouponsEnabled() {

        session.user.data.autoRegisterCouponsEnabled = params.flag as boolean
        session.user.data.save(flush: true)

        if (session.user.data.autoRegisterParams) {
            userService.updateAutoRegisterCouponsParams(session.user.data)
        }

        redirect action: 'autoRegisterParams'
    }

    def list() {
        [users: User.findAllByAdmin(false)]
    }

    def editUser() {

        def user = params.id ? User.get(params.id as Long) : new User(admin: false)

        [user: user]
    }

    def saveUser() {

        def user = params.id ? User.get(params.id as Long) : new User(admin: false, login: params.login)
        def changed = false

        if (!params.id || params.password) {
            user.password = params.password
            user.save(flush: true)
        }

        if (params.clientId && (params.clientId as Long) != user.data.clientId) {
            user.data.clientId = params.clientId as Long
            changed = true
        }

        if (params.sessionId != user.data.sessionId) {
            user.data.sessionId = params.sessionId
            changed = true
        }

        if (params.telegramChatId != user.data.telegramChatId) {
            user.data.telegramChatId = params.telegramChatId
            changed = true
        }

        if (changed) {
            user.data.save(flush: true)
        }

        redirect action: 'list'
    }

}

