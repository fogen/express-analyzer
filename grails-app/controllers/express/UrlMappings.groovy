package express

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: "drawing")
        "/robots.txt"(view: '/robots')
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
