package express

class StatisticInterceptor {

    DrawingService drawingService

    boolean before() {

        if (actionName && !StatisticController.actionsWithoutId.contains(actionName)) {
            request.drawing = params.id ? drawingService.getDrawing(params.id as Integer, true) : drawingService.lastFinishedDrawing
            request.drawingNumbers = drawingService.allDrawingNumbers
        }

        true
    }

    boolean after() { true }

    void afterView() {
        // ..
    }
}
