package express

import grails.converters.JSON

import java.math.RoundingMode


class StatisticController {

    DrawingService drawingService
    SchedulerService schedulerService
    StatisticService statisticService

    static getActionsWithoutId() {
        ['winnersStat', 'periodStatistic', 'jackpot', 'jobKeys', 'finished', 'winnersCount', 'averages']
    }

    static getAvailableActions() {
        [
            interestDistribution : "Распределение ставок по диапозонам",
            stakesReport         : "Выигрышные ставки",
            maxStakes            : "Максимальные совпадения по диапозонам",
            intellectualMaxStakes: "Максимальные совпадения по интеллектуальны кодам",
            winnersStat          : "Количество выигрышных комбинаций",
            periodStatistic      : "Выигрышный процент за период",
            jackpot              : "Тиражи с выигранным суперпризом",
            winStakeLinks        : "Выигрышные связи",
            finished             : "Графики законченных тиражей",
            stakeWinMatches      : "Максимальные совпадения по диапозонам и совпадения",
            winnersCount         : "Количество фаворитов в выигрышных купонах",
            jobKeys              : "Активные задания",
            averages             : "Законченные тиражи: вероятности"
        ]
    }

    def index() {
        [availableActions: availableActions, actionsWithoutId: actionsWithoutId]
    }

    def interestDistribution() {

        if (!request.drawing) {
            flash.error = "Нужен номер тиража"
            redirect([action: 'index'])
            return
        }

        [
            stakes      : statisticService.getInterestDistributionReport(request.drawing, params),
            renderParams: [
                thead: [
                    interval: "Интервал",
                    rate    : "Ставок, %"
                ]
            ]
        ]
    }

    def stakesReport() {

        if (!request.drawing) {
            flash.error = "Нужен номер тиража"
            redirect([action: 'index'])
            return
        }

        def renderParams = [
            thead: [
                code          : "Код",
                winCount      : "Совпадений",
                winnersAverage: "Выигрышный %",
                m15           : "15 совпадений",
                m14           : "14 совпадений",
                m13           : "13 совпадений"
            ],
            style: [
                code: 'font-family: monospace;'
            ]
        ]

        [
            winnersAverage: StakeLinks.findByDrawingDate(request.drawing.expired)?.winnersAverage,
            data          : statisticService.getWinStakesReport(request.drawing, params),
            renderParams  : renderParams
        ]
    }

    def maxStakes() {

        if (!request.drawing) {
            flash.error = "Нужен номер тиража"
            redirect([action: 'index'])
            return
        }

        [
            stakesData  : params.search ? statisticService.getMaxStakesReport(request.drawing, params) : [],
            maxCount    : params.maxCount ?: drawingService.maxMinWinnersCount(params).sort { -it.ratio }.first().max,
            minGroupSize: params.minGroupSize,
            unique      : params.unique,
            max14: params.max14 ?: 1,
            max13: params.max13 ?: 5
        ]
    }

    def stakeWinMatches() {

        if (!request.drawing) {
            flash.error = "Нужен номер тиража"
            redirect([action: 'index'])
            return
        }

        def winnersAverage = StakeLinks.findByDrawingDate(request.drawing.expired)?.winnersAverage ?: 105
        params.from = new BigDecimal(params.from ?: (winnersAverage / 10).toInteger() * 10)
        params.to = new BigDecimal(params.to ?: params.from + 10)

        def stakeCodes = StakeDict.withCriteria {
            eq("drawingDate", request.drawing.expired)
            eq("count", 1)
            between("winnersAverage", params.from, params.to)

            if (params.max) {
                maxResults(params.max as Integer)
            }

            projections {
                property("code")
            }
        }

        def winMatches = [:]
        def calcWinMatchesProcessed = 0

        stakeCodes.each { code ->
            def matches = drawingService.calcStakeWinMatches14(code, request.drawing.expired)
            def period = "14-${(matches / 10).toInteger() * 10}-${((matches / 10) + 1).toInteger() * 10}"

            if (!winMatches[period]) {
                winMatches[period] = []
            }

            winMatches[period] << code

            matches = drawingService.calcStakeWinMatches13(code, request.drawing.expired)
            period = "13-${(matches / 10).toInteger() * 10}-${((matches / 10) + 1).toInteger() * 10}"

            if (!winMatches[period]) {
                winMatches[period] = []
            }

            winMatches[period] << code

            calcWinMatchesProcessed++
        }

        def winMatchesProcessed = 0

        winMatches.each {
            render "<b>$it.key</b> ${it.value.size() / stakeCodes.size() * 100}%<br/>"
            def maxStakes = statisticService.collectMaxStakes(request.drawing, it.value, [:], stakeCodes.size())

            maxStakes.maxStakes.each {
                render it.toString() + "<br/>"
            }

            render "$maxStakes.max $maxStakes.additional<br/>"
            winMatchesProcessed += it.value.size() / 2
        }

        render ""
    }

    def winStakeLinks() {

        if (!request.drawing) {
            flash.error = "Нужен номер тиража"
            redirect([action: 'index'])
            return
        }

        def winnersAverage = StakeLinks.findByDrawingDate(request.drawing.expired).winnersAverage ?: 105
        params.from = new BigDecimal(params.from ?: (winnersAverage / 10).toInteger() * 10)
        params.to = new BigDecimal(params.to ?: params.from + 10)

        def stakeCodes = StakeDict.withCriteria {
            eq("drawingDate", request.drawing.expired)
            eq("count", 1)
            between("winnersAverage", params.from, params.to)

            projections {
                property("code")
            }
        }

        def allData = drawingService.calcStakeLinkData(stakeCodes)
        allData.each {
            render it.toString() + '<br/>'
        }

        render '<br/>'

        def winStakeCodes = StakeDict.withCriteria {
            eq("drawingDate", request.drawing.expired)
            eq("count", 1)
            gt("winCount", 11)

            projections {
                property("code")
            }
        }

        def data = drawingService.calcStakeLinkData(winStakeCodes)
        def result = []

        data.eachWithIndex { it, i ->
            def max = it.max { it.value }
            result << [(max.key): max.value / allData[i][max.key] * 100]
        }

        result.each {
            render it.toString() + '<br/>'
        }

        render ""
    }

    def jobKeys() {
        [data: schedulerService.jobKeys?.sort()?.collect { [job: it.toString().replace("DEFAULT.", "")] } ?: []]
    }

    def winnersStat() {

        def data = drawingService.getLastDrawings((params.max as Integer) ?: 30).findAll { it.results }.collect { Drawing drawing ->
            [
                drawing       : g.link(controller: "drawing", action: "view", id: drawing.number, drawing.number.toString()),
                number        : drawing.number,
                date          : drawing.expired,
                optionCount   : drawing.optionCount,
                winnersAverage: StakeLinks.findByDrawingDate(drawing.expired)?.winnersAverage,
                complexity    : drawing.complexity,
                w15           : (drawing.results.find { it.result == 15 }.count / drawing.optionCount * 100).setScale(4, RoundingMode.HALF_UP),
                w14           : (drawing.results.find { it.result == 14 }.count / drawing.optionCount * 100).setScale(4, RoundingMode.HALF_UP),
                w13           : (drawing.results.find { it.result == 13 }.count / drawing.optionCount * 100).setScale(4, RoundingMode.HALF_UP)
            ]
        }.sort { o1, o2 ->

            if (params.sort && o1.containsKey(params.sort)) {
                return o2."$params.sort" <=> o1."$params.sort"
            }

            o2.w15 <=> o1.w15 ?: o2.w14 <=> o1.w14 ?: o2.w13 <=> o1.w13
        }

        def renderParams = [
            thead: [
                drawing       : "Тираж",
                number        : "Номер",
                date          : "Дата",
                optionCount   : "Кол-во купонов",
                winnersAverage: "Выигрышный %",
                complexity    : "Сложность"
            ],
            style: [
                drawing: 'text-align: center;'
            ]
        ]

        [data: data, renderParams: renderParams]
    }

    def jackpot() {

        def renderParams = [
            thead: [
                drawing       : "Тираж",
                date          : "Дата",
                winnersAverage: "Выигрышный %",
                jackpot       : "Сумма"
            ],
            style: [
                drawing: 'text-align: center;'
            ]
        ]

        [data: statisticService.getJackpotsReport(params), renderParams: renderParams]
    }

    def periodStatistic() {

        if (!params.calc) {
            return [:]
        }

        def data = drawingService.getStakesStatistic(params.monthCount as Integer, (params.minWinCount as Integer ) ?: 12, params.averageType).sort { -it.value }.collect {
            [
                interval: it.key,
                value   : it.value.setScale(2, RoundingMode.HALF_UP)
            ]
        }

        [
            monthCount  : params.monthCount,
            minWinCount : params.minWinCount,
            averageType : params.averageType,
            data        : data,
            renderParams: [
                thead: [
                    interval: "Интервал", value: "Кол-во, %"
                ]
            ]
        ]
    }

    def finished() {

        if (params.id) {
            def drawing = drawingService.getDrawing(params.id as Integer)

            return [
                data: [
                    [
                        drawing   : drawing,
                        jackpot   : drawing.results.find { it.result == 15 && it.count > 0 },
                        chartData : drawingService.getWinnersStakeLinksChartData(drawing) as JSON,
                        chartData2: drawingService.getWinnersStakeDictChartData(drawing)
                    ]
                ]
            ]
        }

        def data = []

        drawingService.getLastDrawings((params.count as Integer) ?: 10, 1).each { drawing ->
            data << [
                drawing  : drawing,
                jackpot  : drawing.results.find { it.result == 15 && it.count > 0 },
                chartData: drawingService.getWinnersStakeLinksChartData(drawing) as JSON,
                chartData2: drawingService.getWinnersStakeDictChartData(drawing)
            ]
        }

        [data: data]
    }

    def reset_cache() {

        statisticService.resetCache(request.drawing, params.cacheName)

        render([result: "ok"] as JSON)
    }

    def winnersCount() {

        if (!params.calc) {
            return [:]
        }

        [
            monthCount  : params.monthCount,
            dataMaxMin  : drawingService.maxMinWinnersCount(params).sort { -it.ratio },
            renderParams: [
                thead: [
                    max     : "Фавориты",
                    min     : "Аутсайдеры",
                    minRange: "Мин. диапозон",
                    ratio   : "доля, %"
                ]
            ]
        ]
    }

    def intellectualMaxStakes() {

        if (!request.drawing) {
            flash.error = "Нужен номер тиража"
            redirect([action: 'index'])
            return
        }

        [
            stakesData : params.search ? statisticService.getIntellectualMaxStakes(request.drawing, params) : [],
            iteration  : params.iteration,
            monthCount : params.monthCount,
            minWinCount: params.minWinCount,
            howMany    : params.howMany
        ]
    }

    def averages() {

        def result = []

        for (drawing in drawingService.getLastDrawings(50, 1)) {

            if (!drawing.winnersCode) {
                continue
            }

            def averages = drawingService.getCodeAverage(drawing)

            result << [
                number     : drawing.number,
                code       : DrawingUtils.formatCode(drawing.winnersCode),
                linkAverage: drawingService.getWinnersAverage(drawing),
                poolAverage: averages.pool,
                lineAverage: averages.line
            ]
        }

        [data: result]
    }
}
