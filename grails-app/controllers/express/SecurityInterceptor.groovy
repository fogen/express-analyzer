package express

import grails.util.Environment


class SecurityInterceptor {

    UserService userService
    SchedulerService schedulerService

    private long startTime

    SecurityInterceptor() {
        matchAll()
    }

    boolean before() {

        startTime = System.currentTimeMillis()

        def userAgent = request.getHeader("User-Agent")
        def reqParams = params.findAll { !['controller', 'action'].contains(it.key) }
        def ip = Environment.developmentMode ? request.remoteAddr : request.getHeader('X-Real-IP')

        log.info("\"$request.method $request.requestURI\" \"$userAgent\" \"${ip}\" ${reqParams ? 'params: ' + reqParams : ''}")

        def referer = request.getHeader('Referer')

        if (!Environment.developmentMode && referer) {
            log.warn("Referer: $referer")
        }

        if (!ip && actionName != 'formatter' || !userAgent || userAgent.matches(/(?i).*bot.*/)) {
            response.status = 403
            log.info("Access denied: ${!ip ? 'no real ip' : !userAgent ? 'no user agent' : 'bot'}")
            return false
        }

        if (controllerName == "user" && actionName == "login" || controllerName == "drawing" && actionName in ["image", "formatter"]) {
            return true
        }

        if (!session.user && actionName != "login") {
            if (Environment.developmentMode && ip == "127.0.0.1") {
                session.user = User.findByLogin("admin")
            } else {
                response.status = 401
                redirect(controller: "user", action: "login")
                return false
            }
        }

        if (session.user.needChangePassword) {
            redirect(controller: 'user', action: 'changePassword')
            return
        }

        if (session.user.isAuth() && !schedulerService.checkExists(UpdateSessionInfoJob.class, session.user.id)) {
            userService.getSessionInfo(session.user)
        }

        return true
    }

    boolean after() { true }

    void afterView() {
        log.info("Render $request.requestURI in ${(System.currentTimeMillis() - startTime) / 1000} sec")
    }
}
