package express

import org.apache.commons.logging.LogFactory

class UpdateDrawingJob extends AbstractDrawingJob {

    protected static final log = LogFactory.getLog(UpdateDrawingJob)

    @Override
    void executeJob() {

        def drawing = drawingService.updateDrawing(drawingNumber)

        if (!drawing) {
            schedulerService.unscheduleJob(this.class, drawingNumber)
            return
        }

        switch (drawing.state) {
            case { it < 2 }:
                drawingService.updateSuperExpressImage(drawing)
                log.info("[$drawingNumber] Drawing is closed and job no longer needed. Unscheduling it")
                schedulerService.unscheduleJob(this.class, drawingNumber)
                break
            case 2:
                userService.checkAutoRegisterCouponsParams(drawing)
                break
            case 3:
                drawingService.updateSuperExpressImage(drawing)
                break
        }

        drawingService.updateAllDrawings()
        drawingService.getLastDrawings(1)
    }
}
