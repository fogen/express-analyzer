package express

import grails.util.Holders
import org.apache.commons.logging.LogFactory
import org.quartz.DisallowConcurrentExecution
import org.quartz.Job
import org.quartz.JobExecutionContext
import org.quartz.JobExecutionException

@DisallowConcurrentExecution
class UpdateAllSessionInfoJob implements Job {

    protected static final log = LogFactory.getLog(UpdateAllSessionInfoJob)

    static triggers = {
        simple repeatCount: 0
    }

    @Override
    void execute(JobExecutionContext context) throws JobExecutionException {

        log.info("execute UpdateAllSessionInfoJob")

        userService.updateAllSessionInfo()
    }

    private static getUserService() {
        Holders.grailsApplication.mainContext.getBean("userService")
    }

}
