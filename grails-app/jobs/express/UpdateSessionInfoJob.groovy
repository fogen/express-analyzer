package express

import grails.util.Holders
import org.apache.commons.logging.LogFactory
import org.quartz.DisallowConcurrentExecution
import org.quartz.Job
import org.quartz.JobExecutionContext
import org.quartz.JobExecutionException


@DisallowConcurrentExecution
class UpdateSessionInfoJob implements Job {

    protected static final log = LogFactory.getLog(UpdateSessionInfoJob)

    @Override
    void execute(JobExecutionContext context) throws JobExecutionException {

        log.info("mergedJobDataMap: ${context.mergedJobDataMap}")

        userService.getSessionInfo(context.mergedJobDataMap?.user)
    }

    private static getUserService() {
        Holders.grailsApplication.mainContext.getBean("userService")
    }

    static getPriority() {
        return 99
    }
}
