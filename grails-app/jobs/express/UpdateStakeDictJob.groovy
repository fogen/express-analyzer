package express

import grails.util.Holders
import org.apache.commons.logging.LogFactory


class UpdateStakeDictJob extends AbstractDrawingJob {

    protected static final log = LogFactory.getLog(UpdateStakeDictJob)

    @Override
    void executeJob() {

        Drawing drawing = drawingService.getDrawing(drawingNumber, true)

        if (!drawing) {
            schedulerService.unscheduleJob(this.class, drawingNumber)
            return
        }

        def totalCount = (context.mergedJobDataMap?.total as Integer) ?: drawingService.getStakeDict(drawing)

        if (!totalCount) {
            return
        }

        def startTime

        if (drawingService.countStakeDictUnique(drawing) < totalCount ||
            [3, 1].contains(drawing.state) && drawingService.countStakeDictTotal(drawing) != drawing.stakeCount) {

            startTime = System.currentTimeMillis()

            def partSize = Holders.config.express.stakeDict.partSize ?: null
            def partCount = partSize ? ((totalCount / partSize).toInteger() + 1) : (Holders.config.express.stakeDict.partCount ?: httpClientService.urlCount)

            if (totalCount < 10000) {
                partCount = 1
            }

            log.info("[$drawingNumber] Start getting stake dict (total $totalCount" + (partCount > 1 ? " by $partCount parts" : "") + ")")

            if (partCount > 1) {

                if (!partSize) {
                    partSize = (totalCount / partCount).toInteger()
                }

                partCount.times { n ->
                    def suffix = n

                    while (suffix >= httpClientService.urlCount) {
                        suffix -= httpClientService.urlCount
                    }

                    def params = [
                        drawingNumber: drawingNumber,
                        suffix: suffix,
                        partCount: partCount,
                        part: n + 1,
                        start: n ? partSize * n - 200 : 0,
                        total: partCount != (n + 1) ? partSize * (n + 1) : totalCount
                    ]

                    log.info("[$drawingNumber] Schedule part ${n + 1}/$partCount")

                    schedulerService.scheduleJob(GetStakeDictPartJob.class, params)
                }

                while (true) {
                    sleep(33344)

                    def finishedParts = 0

                    for (n in (0..partCount - 1)) {
                        if (!schedulerService.checkExists(GetStakeDictPartJob.class, drawingNumber, n)) {
                            finishedParts++
                        }
                    }

                    log.info("check")
                    if (finishedParts == partCount) {
                        break
                    }
                }
            } else {
                def startFrom, nextStartFrom = 400

                while (totalCount > nextStartFrom) {
                    startFrom = nextStartFrom
                    def result = drawingService.getStakeDict(drawing, startFrom)

                    if (result > totalCount) {
                        log.info("[$drawingNumber] Update total from $totalCount to $result")
                        totalCount = result
                    }

                    nextStartFrom += result ? 200 : -200
                }
            }

            log.info("[$drawingNumber] Finish getting stake dict in ${(System.currentTimeMillis() - startTime) / 1000} sec")
        } else {
            log.info("[$drawingNumber] All stake dict already received")
        }

        def stakeLinks

        if (drawingService.needUpdateStakeLinks(drawing)) {
            log.info("[$drawingNumber] Start update stake links")
            startTime = System.currentTimeMillis()
            stakeLinks = drawingService.updateStakeLinks(drawing)
            log.info("[$drawingNumber] Finish update stake links in ${(System.currentTimeMillis() - startTime) / 1000} sec")
        }

        startTime = System.currentTimeMillis()
        log.info("[$drawingNumber] Start update stake dict winners average and count")
        drawingService.updateStakeDict(drawing, stakeLinks)
        log.info("[$drawingNumber] Finish update stake dict winners average and count in ${(System.currentTimeMillis() - startTime) / 1000} sec")

        if (drawing.state != 2 || DateUtils.plusHours(-1, drawing.expired).after(new Date())) {
            schedulerService.unscheduleJob(this.class, drawingNumber)
        }
    }
}
