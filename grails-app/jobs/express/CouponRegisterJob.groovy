package express

import org.apache.commons.logging.LogFactory
import org.quartz.DisallowConcurrentExecution


/**
 *
 * @author semenuk
 */
@DisallowConcurrentExecution
class CouponRegisterJob extends AbstractDrawingJob {

    protected static final log = LogFactory.getLog(CouponRegisterJob)

    @Override
    protected void executeJob() {

        def jobParams = context.mergedJobDataMap
        Drawing drawing = drawingService.getDrawing(jobParams.drawingNumber)
        AutoRegisterCouponsParams autoRegisterParams = jobParams.autoRegisterParams

        if (!drawing) {
            log.error("[$drawingNumber] Can't get drawing")
            schedulerService.unscheduleJob(this.class, autoRegisterParams.id)
            return
        }

        User user = jobParams.user
        def balance = userService.getSessionInfo(user).saldo

        if (!balance) {
            log.info("[$drawingNumber] Bad balance: $balance")
            schedulerService.unscheduleJob(this.class, autoRegisterParams.id)
            return
        }

        def howMany = autoRegisterParams.count * 50 > balance ? ((balance / 50) as Integer) : autoRegisterParams.count
        def stakeLinks = drawingService.getStakeLinks(drawing)
        def codesToRegister = []

        if (autoRegisterParams.intellectual) {
            def iParamsHowMany = autoRegisterParams.packSize && howMany > autoRegisterParams.packSize ? autoRegisterParams.packSize : howMany
            def params = [
                howMany      : howMany,
                averageRanges: drawingService.getIntellectualParams(autoRegisterParams.monthCount, iParamsHowMany, autoRegisterParams.minWinCount),
                mask         : autoRegisterParams.mask,
                monthCount   : autoRegisterParams.monthCount,
                iterations   : autoRegisterParams.iterations ?: 1,
                packSize     : autoRegisterParams.packSize ?: howMany
            ]

            params.excludes = autoRegisterParams.autoExclude ? drawingService.getAutoExcludes(drawing, params) : autoRegisterParams.excludes

            if (params.excludes) {
                log.info("Found excludes: $params.excludes")
            }

            while (codesToRegister.size() < howMany) {

                if (codesToRegister.size() > 0) {
                    params.howMany = howMany - codesToRegister.size()
                }

                codesToRegister.addAll(drawingService.getAverageWinnersCodesEx(drawing, params, stakeLinks)?.code ?: [])
            }
        } else {
            drawingService.resetAverageCache(drawing)

            def params = [
                howMany   : howMany,
                winnersMax: autoRegisterParams.winnersMax,
                winnersMin: autoRegisterParams.winnersMin,
                from13    : autoRegisterParams.from13,
                to13      : autoRegisterParams.to13,
                from14    : autoRegisterParams.from14,
                to14      : autoRegisterParams.to14,
                mask      : autoRegisterParams.mask
            ]

            params.excludes = autoRegisterParams.autoExclude ? drawingService.getAutoExcludes(drawing, params) : autoRegisterParams.excludes

            if (params.excludes) {
                log.info("Found excludes: $params.excludes")
            }

            while (codesToRegister.size() < howMany) {

                if (codesToRegister.size() > 0) {
                    params.howMany = howMany - codesToRegister.size()
                }

                codesToRegister.addAll(drawingService.getAverageWinnersCodes(drawing, params, stakeLinks).code ?: [])
            }
        }

        doAutoRegister(drawing, user, codesToRegister, autoRegisterParams.comment)

        schedulerService.unscheduleJob(this.class, autoRegisterParams.id)
    }

    private void doAutoRegister(Drawing drawing, User user, codes, comment) {

        log.info("[$drawing.number] Start auto register codes: $codes")

        def message = "Авторегистрация купонов:"
        codes.each {
            message += "\n<code>${DrawingUtils.formatCode(it)}</code>"
        }

        telegramService.sendMessage(user, message)

        def attempts = 5
        def errorCodes = userService.registerCoupons(user, codes, drawing.number, comment ?: "Авто")

        while (attempts > 0 && errorCodes) {
            def today = new Date()

            if (today.after(drawing.expired)) {
                break
            }

            log.warn("Some codes were not registered. Retry to register codes: $errorCodes")
            def errorMessage = "Повторная регистрация купонов:"

            errorCodes.each {
                errorMessage += "\n<code>${DrawingUtils.formatCode(it)}</code>"
            }

            telegramService.sendMessage(user, errorMessage)

            errorCodes = userService.registerCoupons(user, errorCodes, drawing.number, comment ?: "Авто")
            attempts--
        }
    }

    static getPriority() {
        return 99
    }
}
