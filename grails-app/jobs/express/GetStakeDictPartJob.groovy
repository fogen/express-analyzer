package express

import org.apache.commons.logging.LogFactory

class GetStakeDictPartJob extends AbstractDrawingJob {

    protected static final log = LogFactory.getLog(GetStakeDictPartJob)

    @Override
    protected void executeJob() {

        Drawing drawing = drawingService.getDrawing(drawingNumber)

        if (!drawing) {
            return
        }

        def totalCount = context.mergedJobDataMap?.total as Integer
        def startFrom = context.mergedJobDataMap?.start as Integer
        def nextStartFrom = startFrom + 200

        log.info("[$drawingNumber] Start getting stake dict part #${context.mergedJobDataMap?.part} (start from: $startFrom total $totalCount)")

        while (totalCount > nextStartFrom) {
            startFrom = nextStartFrom
            def result = drawingService.getStakeDict(drawing, startFrom, context.mergedJobDataMap?.suffix as Integer)

            if (context.mergedJobDataMap?.partCount == context.mergedJobDataMap?.part && result > totalCount) {
                log.info("[$drawingNumber] Update total from $totalCount to $result")
                totalCount = result
            }

            nextStartFrom += result ? 200 : -200
        }

        log.info("[$drawingNumber] Finish getting stake dict part #${context.mergedJobDataMap?.part}")
    }
}
