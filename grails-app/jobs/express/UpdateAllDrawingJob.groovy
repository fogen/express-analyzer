package express

import org.quartz.DisallowConcurrentExecution

@DisallowConcurrentExecution
class UpdateAllDrawingJob extends AbstractDrawingJob {

    static triggers = {
        simple repeatCount: 0
    }

    @Override
    void executeJob() {

        if (context.mergedJobDataMap?.manual) {
            log.info("Start update all drawings")

            drawingService.allDrawingNumbers.each { number ->
                def drawing = drawingService.getDrawing(number, true)

                if (!drawing || drawing.state == 1) {
                    log.info("Start update drawing #$number")
                    while (true) {
                        if (!schedulerService.checkExists(UpdateDrawingJob.class, number) && !schedulerService.checkExists(UpdateStakeDictJob.class, number)) {
                            break
                        }

                        sleep(1000)
                    }
                }
            }

            drawingService.clearOldDrawingData()

            log.info("Finish update all drawings")
        }

        drawingService.getLastDrawings()
    }

    boolean isDrawingNumberNeeded() {
        false
    }
}
