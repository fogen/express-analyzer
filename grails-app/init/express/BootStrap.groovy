package express

import grails.util.Holders

class BootStrap {

    def init = { servletContext ->

        def defaultAdminPassword = Holders.config.express.security.defaultAdminPassword

        if (defaultAdminPassword && !User.count()) {
            new User(login: 'admin', password: defaultAdminPassword, admin: true).save()
        }
    }

    def destroy = {
    }
}
