import grails.plugin.cache.GrailsConcurrentLinkedMapCache
import grails.plugin.cache.GrailsConcurrentLinkedMapCacheManager


// Place your Spring DSL code here
beans = {
    grailsCacheManager GrailsConcurrentLinkedMapCacheManager
}
