package express

import grails.gorm.transactions.Transactional
import java.math.RoundingMode

@Transactional
class StatisticService {

    def grailsCacheManager
    DrawingService drawingService
    SchedulerService schedulerService

    def getInterestDistributionReport(Drawing drawing, Map params) {

        def total = StakeDict.countByDrawingDate(drawing.expired)
        def cacheName = "${drawing.number}_${params.five}"
        def interestDistributionReportCache = grailsCacheManager.getCache("interestDistributionReportCache", 10)
        def result = interestDistributionReportCache.get(cacheName)?.get() ?: [:]

        if (result.total == total) {
            log.info("[$drawing.number] Return interest distribution report from cache")
            return result.data
        }

        if (result) {
            interestDistributionReportCache.evict(cacheName)
        }

        result.total = total

        def allStakes = StakeDict.withCriteria {
            eq("drawingDate", drawing.expired)
            isNotNull("winnersAverage")

            if (drawing.state == 1) {
                gt("winCount", 9)
            }

            order("winnersAverage")
        }.groupBy { it.winCount }.sort { -it.key }

        result.data = []

        for (stakes in allStakes) {

            if (!stakes.value) {
                continue
            }

            def stakeResult = []
            def min = stakes.value.first().winnersAverage
            def max = stakes.value.last().winnersAverage

            ((min / 10).toInteger()..(max / 10).toInteger()).each {
                def start = it * 10
                def end = start + (params.five ? 5 : 10)
                def matches = stakes.value.findAll { it.winnersAverage > start && it.winnersAverage < end }.sum { it.count }

                if (matches) {
                    stakeResult << [
                        interval: "$start-$end",
                        rate    : (matches / stakes.value.size() * 100).setScale(2, RoundingMode.HALF_UP)
                    ]
                }

                if (params.five) {
                    start = end
                    end = start + 5

                    matches = stakes.value.findAll { it.winnersAverage > start && it.winnersAverage < end }.sum { it.count }

                    if (matches) {
                        stakeResult << [
                            interval: "$start-$end",
                            rate    : (matches / stakes.value.size() * 100).setScale(2, RoundingMode.HALF_UP)
                        ]
                    }
                }
            }

            result.data << [winCount: stakes.key, rates: stakeResult.sort { -it.rate }]
        }

        interestDistributionReportCache.put(cacheName, result)

        result.data
    }

    def getWinStakesReport(Drawing drawing, Map params) {

        def total = StakeDict.countByDrawingDate(drawing.expired)
        def cacheName = "${drawing.number}_${params.from ?: 0}_${params.to ?: 0}_${params.max ?: 1000}"
        def winStakesReportCache = grailsCacheManager.getCache("winStakesReportCache", 10)
        def result = winStakesReportCache.get(cacheName)?.get() ?: [:]

        if (result.total == total) {
            log.info("[$drawing.number] Return win stakes report from cache")
            return result.data
        }

        if (result) {
            winStakesReportCache.evict(cacheName)
        }

        result.total = total
        result.data = []

        List<StakeDict> stakes = StakeDict.withCriteria {
            eq("drawingDate", drawing.expired)

            if (params.from && params.to) {
                between("winnersAverage", new BigDecimal(params.from), new BigDecimal(params.to))
            }

            if (drawing.state == 1) {
                ge("winCount", 12)
                order("winCount", "desc")
            } else {
                not {
                    'in'("code", ["1" * 15, "2" * 15, "X" * 15])
                }
                maxResults((params.max as Integer) ?: 1000)
            }

            order("winnersAverage", "desc")
        }

        stakes.each { StakeDict stake ->

            def winMatches = [:]

            if (params.external) {
                def drawingResults = drawingService.calculateCoupons(drawing, stake.code)
                winMatches.m15 = drawingResults.find { it.Result == 15 }?.Count
                winMatches.m14 = drawingResults.find { it.Result == 14 }?.Count
                winMatches.m13 = drawingResults.find { it.Result == 13 }?.Count
            } else {
                winMatches = drawingService.calcStakeWinMatches(stake.code, stake.drawingDate)
            }

            result.data << [
                code          : DrawingUtils.formatCode(stake.code),
                winCount      : stake.winCount ?: "",
                winnersAverage: stake.winnersAverage ?: "",
                m13           : winMatches.m13,
                m14           : winMatches.m14,
                m15           : winMatches.m15
            ]
        }

        result.data = result.data.sort { o1, o2 ->
            o2.winCount <=> o1.winCount ?: o2.m13 <=> o1.m13 ?: o2.m14 <=> o1.m14 ?: o2.m15 <=> o1.m15
        }

        winStakesReportCache.put(cacheName, result)

        result.data
    }

    def getMaxStakesReport(Drawing drawing, Map params) {

        def maxCode = drawing.maxCode
        def index = ((params.maxCount as Integer) ?: 7) - 1
        def minGroupSize = (params.minGroupSize as Integer) ?: 2
        def startCode = maxCode[0..index]
        def maxLikes = []

        while (index >= -1) {
            def start = index == -1 ? "" : startCode[0..index]
            def count = startCode.findAll { it != "_" }.size() - start.findAll { it != "_" }.size()
            startCode = maxLikes ? (start + "_" + maxCode[index + 2..index + 1 + count]) : startCode
            maxLikes.addAll(getMaxLikes(maxCode, startCode))
            def lastCode = maxLikes.last()
            index = lastCode.lastIndexOf("_")

            while (index > -1 && lastCode[index] == "_") {
                index--
            }

            index--
        }

        log.info("Total max likes: ${maxLikes.size()} from ${maxLikes.first()} to ${maxLikes.last()}")

        def stakesData = []

        for (maxLike in maxLikes) {
            def notLikes = getNotLikes(drawing, maxLike)

            def stakeCodes = StakeDict.withCriteria {
                eq("drawingDate", drawing.expired)
                eq("count", 1)
                like("code", maxLike)
                not {
                    notLikes.each {
                        like("code", it)
                    }
                }
                projections {
                    property("code")
                }
            }

            if (!stakeCodes || stakeCodes.size() < minGroupSize) {
                continue
            }

/*
            if (params.group) {
                def groupedStakes = []
                def remStakeCodes = new ArrayList(stakeCodes)

                while (remStakeCodes.size() > 0) {
                    def winners = remStakeCodes.get(0).toList()
                    def winMatches = remStakeCodes.findAll { drawingService.countWinMatches(it, winners) > 9 }

                    if (winMatches) {
                        groupedStakes << winMatches
                        remStakeCodes.remove(winMatches)
                    }
                }

                def stakesData = []

                groupedStakes.sort { -it.size() }.each { filtered ->
                    def maxStakes = collectMaxStakes(drawing, filtered, params, stakeCodes.size())

                    if (maxStakes) {
                        stakesData << maxStakes
                    }
                }

                return stakesData
            }
*/

            def data = collectMaxStakes(drawing, stakeCodes, params)

            if (data) {
                data.maxLike = maxLike
                stakesData << data
            }
        }

        stakesData.sort { drawing.state == 1 ? -it.winMatches : -it.total }
    }

    private getMaxLikes = { String maxCode, String rootCode ->

        def result = [rootCode.padRight(15, "_")]

        if (rootCode.length() < 15) {
            def code = rootCode[0..-2]

            (rootCode.length()..14).each { n ->
                def k = n - rootCode.length() + 2
                result << (code + maxCode[n].padLeft(k, "_")).padRight(15, "_")
            }
        }

        result
    }

    private getNotLikes(Drawing drawing, String likeCode) {

        def maxCode = drawing.maxCode
        def result = []

        (0..14).each { n ->
            if (likeCode[n] == "_") {
                result << maxCode[n].padLeft(n + 1, "_").padRight(15, "_")
            }
        }

        result
    }

    private getLikes(Drawing drawing, Integer maxCount = 7) {

        def minCode = drawing.minCode
        def result = []

        (0..13).each { n ->
            def code = "_" * n + minCode[n]
            (n + 1..14).each { n2 ->
                result << code + "_" * (n2 - n - 1) + minCode[n2] + "_" * (14 - n2)
            }
        }

        result
    }

    Map collectMaxStakes(drawing, filtered, params = [:], allStakesCount = null, checkWinnerAverage = false) {

        def maxStakes = drawingService.calcMaxStakes(filtered, allStakesCount)
        def win = [max: "", min: "", mid: ""]

        maxStakes.each {
            win.max += it.max.code
            win.min += it.min.code
            win.mid += it.mid.code
        }

        if (!win.max) {
            return [:]
        }

        def stakeLinks = drawingService.getStakeLinks(drawing)
        def winAverage = drawingService.getWinnersAverage(stakeLinks, win.max)

        if (checkWinnerAverage && params.from && params.to && (winAverage < params.from || winAverage > params.to)) {
            return [:]
        }

        def existCode = StakeDict.findByDrawingDateAndCode(drawing.expired, win.max)

        if (params.unique && existCode) {
            return [:]
        }

        def drawingResults = drawing.state != 2 ? drawingService.calcStakeWinMatches(win.max, drawing.expired) : drawingService.calculateCoupons(drawing, win.max)
        def m13 = drawing.state != 2 ? drawingResults.m13 : drawingResults.find { it.Result == 13 }?.Count
        def m14 = drawing.state != 2 ? drawingResults.m14 : drawingResults.find { it.Result == 14 }?.Count
        def m15 = drawing.state != 2 ? drawingResults.m15 : drawingResults.find { it.Result == 15 }?.Count

        if (params.unique && m15 > 0) {
            return [:]
        }

        if (params.max14 && m14 > params.max14) {
            return [:]
        }

        if (params.max13 && m13 > params.max13) {
            return [:]
        }

        def result = [
            total     : filtered.size(),
            maxStakes : maxStakes,
            max       : AppUtils.g.link(controller: 'drawing', action: 'view', id: drawing.number, params: [stakePos: win.max.toList()], DrawingUtils.formatCode(win.max)),
            additional: "${winAverage} (13: $m13, 14: $m14, 15: $m15)",
            min       : win.min
        ]

        if (drawing.state == 1) {
            result.winMatches = drawingService.countWinMatches(win.max, drawing.winnersCode)
            result.additional += " - ${result.winMatches}"
        }

        result
    }

    def getJackpotsReport(Map params) {

        def drawings = Drawing.withCriteria {
            eq("state", 1)
            isNotNull("resultsData")
            not {
                eq("resultsData", "[]")
                like("resultsData", '[{"count":0%')
            }
            order("expired", "desc")
        }

        def data = drawings.collect { drawing ->
            def winnersAverage = StakeLinks.findByDrawingDate(drawing.expired)?.winnersAverage

            [
                drawing       : AppUtils.g.link(controller: "drawing", action: "view", id: drawing.number, drawing.number.toString()),
                date          : DateUtils.formatDate(drawing.expired),
                winnersAverage: winnersAverage,
                jackpot       : drawing.jackpot,
                w15           : drawing.results.find { it.result == 15 }.count,
                p15           : (drawing.results.find { it.result == 15 }.count / drawing.optionCount * 100).setScale(4, RoundingMode.HALF_UP),
                w14           : drawing.results.find { it.result == 14 }.count,
                p14           : (drawing.results.find { it.result == 14 }.count / drawing.optionCount * 100).setScale(4, RoundingMode.HALF_UP),
                w13           : drawing.results.find { it.result == 13 }.count,
                p13           : (drawing.results.find { it.result == 13 }.count / drawing.optionCount * 100).setScale(4, RoundingMode.HALF_UP)
            ]
        }

        if (params.sort) {
            data = data.sort { it[params.sort] }
        }

        data
    }

    void resetCache(Drawing drawing, String cacheName) {

        def cache = grailsCacheManager.getCache(cacheName)

        cache?.allKeys?.findAll { it.startsWith(drawing.number.toString()) }?.each { cache.evict(it) }
    }

    def getIntellectualMaxStakes(Drawing drawing, Map params) {

        params.averageRanges = drawingService.getIntellectualParams((params.monthCount ?: 1) as Integer, (params.howMany ?: 10) as Integer, (params.minWinCount ?: 12) as Integer)

        def codes = []
        def stakeLinks = drawingService.getStakeLinks(drawing)

        ((params.iteration as Integer) ?: 10).times {
            codes.addAll(drawingService.getAverageWinnersCodesEx(drawing, params, stakeLinks)?.code)
        }

        [collectMaxStakes(drawing, codes, params)]
    }
}
