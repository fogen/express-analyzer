package express

import grails.gorm.transactions.Transactional
import grails.util.Holders

import java.text.NumberFormat


@Transactional
class UserService {

    DrawingService drawingService
    HttpClientService httpClientService
    SchedulerService schedulerService
    TelegramService telegramService

    private static String getBaseUrl() {
        Holders.config.express.user.baseUrl ?: "https://clientsapi11.bkfon-resource.ru"
    }

    User authenticate(String login, String password) {

        def user = User.findByLogin(login)

        if (!user || !user.checkPassword(password)) {
            return
        }

        user
    }

    Boolean changePassword(String login, String oldPassword, String newPassword) {

        def user = User.findByLogin(login)

        if (!user?.checkPassword(oldPassword)) {
            return false
        }

        user.password = newPassword
        user.needChangePassword = false
        user.save(flush: true)

        true
    }

    void updateAllSessionInfo() {

        User.list().findAll { it.isAuth() }.each {
            getSessionInfo(it)
        }
    }

    private updateBalance(User user, BigDecimal newBalance) {

        if (user.data.balance == newBalance) {
            return
        }

        user.data.balance = newBalance
        user.data.save()
    }

    Map getSessionInfo(User user) {

        if (!user?.isAuth()) {
            return [:]
        }

        def params = [clientId: user.data.clientId, sysId: 1, fsid: user.data.fsid]
        def result = httpClientService.post(baseUrl + "/session/info", params)

        if (!result || result.result == "error") {

            if (result.errorCode == 17) {
                user.data.sessionId = null
                user.data.save(flush: true)

                telegramService.sendMessage(user, "Потеряна сессия пользователя")
            }

            schedulerService.unscheduleJob(UpdateSessionInfoJob.class, user.id)
            return [:]
        }

        updateBalance(user, result.saldo)

        schedulerService.scheduleJob(UpdateSessionInfoJob, [user: user, number: user.id])

        result
    }

    def updateLastOperations(Drawing drawing) {

        for (user in User.list()) {

            if (!user?.isAuth() || !RegisteredCoupon.countByDrawingNumberAndUser(drawing.number, user)) {
                continue
            }

            def finished = drawing.state == 1
            def operations = getLastOperations(user, drawing, finished)?.operations
            def message = "Розыгрыш <b>$drawing.number</b> ${finished ? 'завершён' : 'обновился'}.\n"

            if (!finished) {
                message += "Осталось <b>${15 - drawing.events.count { it.resultCode }}</b> игр.\n"
            }

            if (!operations) {
                message += "Выигрышных купонов нет.😭"
            } else {
                message += finished ? "Выиграли купоны:" : "Могут выиграть <b>${operations.size()}</b> купонов."

                if (!finished && operations.size() > 10) {
                    operations = operations.take(10)
                    message += "\nДесятка лучших:"
                }

                def padSize = 0
                operations.each {
                    def winAmount = NumberFormat.numberInstance.format(finished ? it.sum : drawingService.calcPossibleWinAmount(drawing, it.simpleCode))
                    padSize = padSize ?: winAmount.length()
                    message += "\n<code>" + DrawingUtils.formatCode(it.simpleCode) + " - " + winAmount.padLeft(padSize) + " (${it.matchCount.toString().padLeft(2)})</code>"
                }
            }

            telegramService.sendMessage(user, message)
        }
    }

    Map getLastOperations(User user, Drawing drawing = null, Boolean onlyProfit = true, Integer minMatchCount = 9) {

        if (!user || !user.isAuth()) {
            return [:]
        }

        def lastDrawingNumbers = []

        if (drawing) {
            lastDrawingNumbers << drawing.number
        } else {
            drawingService.allDrawingNumbers?.take(10)?.each {
                if (drawingService.getDrawing(it)?.state != 0) {
                    lastDrawingNumbers << it
                }
            }
        }

        def params = [
            clientId: user.data.clientId,
            sysId   : 1,
            fsid    : user.data.fsid,
            maxCount: drawing ? 100 : 800
        ]

        def result = httpClientService.post(baseUrl + "/session/client/lastOperations", params)

        if (!result || result.result == "error") {

            if (result.errorCode == 17) {
                user.data.sessionId = null
                user.data.save(flush: true)
            }

            return [:]
        }

        updateBalance(user, result.balance)

        def operations = result.operations?.findAll {
            it.operationId == (onlyProfit ? 102 : 101) && it.marker.toString()[-3..-1].toInteger() in lastDrawingNumbers
        }

        operations = operations?.collect {
            [
                regId    : it.marker,
                drawingId: it.marker.toString()[-3..-1].toInteger(),
                couponId : it.marker.toString()[0..-4],
                sum      : it.sum
            ]
        }

        params.remove("maxCount")

        params.putAll([
            lang: "ru",
            betTypeName: "toto"
        ])

        operations.each { op ->
            params.regId = op.regId
            op.code = ""
            op.matchCount = 0
            def couponDetails = httpClientService.post(baseUrl + "/coupon/info", params)?.body
            def simpleCode = ""
            couponDetails?.games?.eachWithIndex { game, i ->
                simpleCode += game.bet

                if (i in [5, 10]) {
                    op.code += "-"
                }

                if (game.result == "none") {
                    op.code += "<span style='color: blue'>$game.bet</span>"
                    op.matchCount++
                } else {
                    if (game.result == "win2" && game.bet.contains("2") ||
                        game.result == "win1" && game.bet.contains("1") ||
                        game.result == "draw" && game.bet.contains("X") ||
                        game.result == "cancelled") {

                        op.code += "<span style='color: green'>$game.bet</span>"
                        op.matchCount++
                    } else {
                        op.code += "<span style='color: red'>$game.bet</span>"
                    }
                }
            }

            op.simpleCode = simpleCode

            def savedCoupon = RegisteredCoupon.findByCodeAndDrawingNumber(simpleCode, op.drawingId)

            if (!savedCoupon) {
                new RegisteredCoupon(
                    regId: op.regId,
                    code: simpleCode,
                    drawingNumber: op.drawingId,
                    user: user,
                    comment: "с сервера"
                ).save(flush: true)
            }

            op.comment = savedCoupon?.comment ?: "с сервера"
        }

        if (drawing) {
            return [operations: operations.findAll { it.matchCount >= minMatchCount }.sort { -it.matchCount }]
        }

        operations.sort { -it.matchCount }.groupBy { it.drawingId }.sort { -it.key }
    }

    def registerCoupons(User user, List<String> codes, Integer drawingNumber, String comment = null) {

        def errorCodes = []

        codes.each { code ->
            def result = registerCoupon(user, code, drawingNumber, comment)

            if (!result || result.resultCode != 0) {
                errorCodes << code
            }
        }

        errorCodes
    }

    def registerCoupon(User user, String code, Integer drawingNumber, String comment = null) {

        if (!code || !drawingNumber || !user?.isAuth()) {
            log.warn("[$drawingNumber] Can't register coupon $code")
            return [resultCode: -1]
        }

        if (RegisteredCoupon.countByDrawingNumberAndCode(drawingNumber, code)) {
            log.info("[$drawingNumber] Coupon $code already registered")
            return [resultCode: -1]
        }

        def couponParams = [w1: 0, wX: 0, w2: 0]

        code.eachWithIndex { c, i ->
            couponParams."w${c}" += 2**i
        }

        def params = [
            client  : [id: user.data.clientId],
            clientId: user.data.clientId,
            sysId   : 1,
            fsid    : user.data.fsid,
            lang    : "rus"
        ]

        def requestId = httpClientService.post(baseUrl + "/session/toto/requestId", params)?.requestId

        if (!requestId) {
            log.warn("Can't get requestId for coupon register")
            return [resultCode: -1]
        }

        params.putAll([
            requestId: requestId,
            coupon   : [
                amount  : 50,
                totoId  : drawingNumber,
                win1Mask: couponParams.w1,
                drawMask: couponParams.wX,
                win2Mask: couponParams.w2
            ]
        ])

        def result = httpClientService.post(baseUrl + "/session/toto/register", params)

        if (result.resultCode == 0) {
            def parts = result.regId.split("-")
            def regId = parts[1] + parts[0]
            new RegisteredCoupon(
                regId: regId,
                code: code,
                drawingNumber: drawingNumber,
                user: user,
                comment: comment ?: "Вручную"
            ).save()

            updateBalance(user, result.clientSaldo)
        }

        result
    }

    void checkAutoRegisterCouponsParams(Drawing drawing, Long id = null) {

        if (id) {
            schedulerService.unscheduleJob(CouponRegisterJob.class, id)
            def params = AutoRegisterCouponsParams.read(id)

            if (!params.userData.autoRegisterCouponsEnabled) {
                return
            }

            def scheduleDate = DateUtils.plusMinutes(params.minutesBeforeEnd * -1, drawing.expired)

            if (scheduleDate.after(new Date())) {
                schedulerService.scheduleJobOnce(
                    CouponRegisterJob.class,
                    [user: params.userData.user, autoRegisterParams: params, number: id, drawingNumber: drawing.number],
                    scheduleDate
                )

                def message = "На <b>${DateUtils.formatDate(scheduleDate)}</b> запланирована авторегистрация <b>$params.count</b> купонов.\n" +
                    "Текущий баланс: <b>$params.userData.balance ₽</b>"

                telegramService.sendMessage(params.userData.user, message)
            }

            return
        }

        def userDatas = UserData.findAllByAutoRegisterCouponsEnabled(true)

        for (data in userDatas) {
            if (!data.user.isAuth() || !data.autoRegisterParams) {
                continue
            }

            data.autoRegisterParams.findAll {
                (!it.drawingNumber || it.drawingNumber == drawing.number) &&
                    !schedulerService.checkExists(CouponRegisterJob.class, it.id)
            }.each {
                def scheduleDate = DateUtils.plusMinutes(it.minutesBeforeEnd * -1, drawing.expired)

                if (scheduleDate.after(new Date())) {
                    schedulerService.scheduleJobOnce(
                        CouponRegisterJob.class,
                        [user: it.userData.user, autoRegisterParams: it, number: it.id, drawingNumber: drawing.number],
                        scheduleDate
                    )

                    def message = "На <b>${DateUtils.formatDate(scheduleDate)}</b> запланирована авторегистрация <b>$it.count</b> купонов.\n" +
                        "Текущий баланс: <b>$it.userData.balance ₽</b>"

                    telegramService.sendMessage(it.userData.user, message)
                }
            }
        }
    }

    void updateAutoRegisterCouponsParams(UserData userData) {

        userData.autoRegisterParams?.each { params ->

            if (userData.autoRegisterCouponsEnabled) {
                checkAutoRegisterCouponsParams(drawingService.currentDrawing, params.id)
            } else {
                schedulerService.unscheduleJob(CouponRegisterJob.class, params.id)
            }
        }

    }

    boolean deleteAutoRegisterParams(User user, Long id) {

        def autoRegisterCouponsParams = AutoRegisterCouponsParams.read(id)

        if (!autoRegisterCouponsParams) {
            return false
        }

        schedulerService.unscheduleJob(CouponRegisterJob.class, id)
        user.data.removeFromAutoRegisterParams(autoRegisterCouponsParams)

        if (!user.data.autoRegisterParams) {
            user.data.autoRegisterCouponsEnabled = false
        }

        user.data.save(flush: true)
        autoRegisterCouponsParams.delete(flush: true)

        true
    }
}
