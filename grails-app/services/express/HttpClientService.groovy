package express

import grails.plugins.rest.client.RestBuilder
import grails.util.Holders
import org.springframework.http.HttpStatus


class HttpClientService {

    private Integer sleepSeconds = 10
    private List currentBaseUrls
    private RestBuilder restBuilder = new RestBuilder()

    private getBaseUrls() {

        if (!currentBaseUrls) {
            currentBaseUrls = Holders.config.express.baseUrls ?: ["http://old.toto-info.co"]
        }

        Holders.config.express.baseUrls ?: ["http://old.toto-info.co"]
    }

    private getBaseUrl(Integer urlIndex = 0) {

        if (!urlIndex) {
            urlIndex = 0
        }

        if (!currentBaseUrls) {
            return getNextBaseUrl(urlIndex)
        }

        if ((urlIndex + 1) >= urlCount) {
            urlIndex = 0
        }

        currentBaseUrls.get(urlIndex)
    }

    private getNextBaseUrl(Integer index = 0) {

        if (currentBaseUrls) {
            log.warn("Wait $sleepSeconds sec and switching base url")
            sleep(sleepSeconds * 1000)
            sleepSeconds *= 2
        }

        def nextIndex = currentBaseUrls ? baseUrls.indexOf(currentBaseUrls.get(index)) + 1 : 0
        currentBaseUrls[index] = nextIndex == baseUrls.size() ? baseUrls.get(0) : baseUrls.get(nextIndex)

        log.info("Current base url [$index]: ${currentBaseUrls[index]}")

        currentBaseUrls[index]
    }

    Integer getUrlCount() {
        baseUrls.size()
    }

    def get(String path) {

        def url = !path.startsWith("http") ? (baseUrl + path) : path

        def resp = restBuilder.get(url)

        if (!resp || resp.statusCode != HttpStatus.OK) {
            log.error("Response failure. StatusCode: ${resp?.statusCode} body: ${resp?.body} url: $url")
            getNextBaseUrl()

            if (resp.statusCode in [HttpStatus.SERVICE_UNAVAILABLE, HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.GATEWAY_TIMEOUT, HttpStatus.BAD_GATEWAY]) {
                return get(path)
            }

            return [:]
        }

        sleepSeconds = 10

        resp.json
    }

    def post(String path, Map params = [:], Integer urlIndex = 0) {

        def url = !path.startsWith("http") ? (getBaseUrl(urlIndex) + path) : path

        def resp = restBuilder.post(url) {
            header "User-Agent", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0"
            json params
        }

        if (!resp || resp.statusCode != HttpStatus.OK) {
            log.error("Response failure. StatusCode: ${resp?.statusCode} body: ${resp?.body} url: $url params: $params")
            getNextBaseUrl(urlIndex)

            if (resp.statusCode in [HttpStatus.SERVICE_UNAVAILABLE, HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.GATEWAY_TIMEOUT, HttpStatus.BAD_GATEWAY]) {
                return post(path, params, urlIndex)
            }

            return [:]
        }

        sleepSeconds = 10

        resp.json
    }

}
