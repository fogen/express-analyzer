package express

import grails.gorm.transactions.Transactional
import grails.util.Holders
import org.hibernate.SessionFactory
import org.hibernate.criterion.CriteriaSpecification
import org.xhtmlrenderer.simple.ImageRenderer

import java.math.RoundingMode
import java.util.concurrent.ThreadLocalRandom


@Transactional
class DrawingService {

    def grailsCacheManager
    HttpClientService httpClientService
    SchedulerService schedulerService
    SessionFactory sessionFactory
    UserService userService

    private synchronized Set<Integer> drawingNumbers = []
    private Drawing currentDrawing

    static String getUrlPrefix() {
        Holders.config.express.urlPrefix ?: "/DataService.svc/"
    }

    void updateAllDrawings(startJob = false) {

        drawingNumbers.clear()
        drawingNumbers.addAll(httpClientService.get(urlPrefix + "GetAllDrawings")?.d ?: [])

        schedulerService.scheduleJob(UpdateAllDrawingJob.class, [manual: startJob])
    }

    List<Integer> getAllDrawingNumbers() {

        if (!drawingNumbers) {
            updateAllDrawings()
        }

        drawingNumbers.toList()
    }

    List<Drawing> getLastDrawings(Integer count = 20, Integer state = null) {

        def drawings = []

        for (number in allDrawingNumbers) {
            def drawing = getDrawing(number, true)

            if (drawing && (!state || drawing.state == state)) {
                drawings << drawing
            }

            if (drawings.size() == count) {
                break
            }
        }

        drawings
    }

    List<Drawing> getLastDrawings(Date fromDate, Integer state = 1) {

        def drawings = []

        for (number in allDrawingNumbers) {
            def drawing = getDrawing(number, true)

            if (!drawing || drawing.state != state) {
                continue
            }

            if (drawing.expired.before(fromDate)) {
                break
            }

            drawings << drawing
        }

        drawings
    }

    Drawing getLastFinishedDrawing() {

        for (number in allDrawingNumbers) {
            def drawing = getDrawing(number)

            if (drawing?.state == 1) {
                return drawing
            }
        }
    }

    Drawing getDrawing(Integer drawingNumber, Boolean updateIfAbsent = false) {

        Drawing drawing = Drawing.findByNumber(drawingNumber)

        if (!drawing) {

            if (updateIfAbsent && allDrawingNumbers.contains(drawingNumber)) {
                schedulerService.scheduleJob(UpdateDrawingJob.class, [drawingNumber: drawingNumber])
            }

            log.error("[$drawingNumber] Drawing not found")
            return
        }

        if (needUpdateDrawing(drawing)) {
            schedulerService.scheduleJob(UpdateDrawingJob.class, [drawingNumber: drawingNumber])
        }

        drawing
    }

    private boolean needUpdateDrawing(Drawing drawing, Long currentVersion = null) {

        if (!currentVersion) {
            currentVersion = getDrawingVersion(drawing.number)
        }

        if (drawing.state == 1) {
            return DateUtils.plusYear(-1).after(drawing.expired) ||
                currentVersion == drawing.ver && (!drawing.results || !drawing.winnersCode || !drawing.optionCount || drawing.events.count { !it.score })
        }

        currentVersion != drawing.ver
    }

    Long getDrawingVersion(Integer drawingNumber) {
        httpClientService.post(urlPrefix + "GetDrawingVersion", [id: drawingNumber])?.d as Long
    }

    Drawing updateDrawing(Integer drawingNumber) {

        def currentVersion = getDrawingVersion(drawingNumber)

        if (!currentVersion) {
            return getDrawing(drawingNumber)
        }

        def drawing = Drawing.findByNumberAndVer(drawingNumber, currentVersion)

        if (drawing && !needUpdateDrawing(drawing, currentVersion)) {

            if (drawing.needFillWinnersCode) {
                drawing.winnersCode = drawing.events.collect { it.resultCode }.join()
                drawing.save(flush: true)
            }

            return drawing
        }

        def response = httpClientService.post(urlPrefix + "GetDrawing", [id: drawingNumber])?.d

        if (!response) {
            log.error("[$drawingNumber] Error receiving drawing from external server")
            return getDrawing(drawingNumber)
        }

        drawing = updateDrawingFromJson(response)

        if (!drawing) {
            return getDrawing(drawingNumber)
        }

        if (drawing.state == 2) {
            currentDrawing = drawing
        }

        log.info("Drawing #$drawingNumber updated")

        drawing
    }

    private updateDrawingFromJson(json) {

        Drawing drawing = Drawing.findOrCreateByNumber(json.Id)

        if (drawing.id) {
            if (drawing.expired != DateUtils.parseDateFromJson(json.Expired)) {
                clearDrawingData(drawing)
            }

            if (drawing.state == 2 && ((json.OptionCount / 100000).toInteger() - (drawing.optionCount / 100000).toInteger() > 0)) {
                startUpdateStakeDict(drawing)
            }
        }

        def needUpdateLastOperations = json.State == 1 && allDrawingNumbers.take(3).contains(drawing.number) ||
            drawing.events && json.Details.Events.count { !it.ResultCode } &&
            drawing.events.count { it.resultCode } != json.Details.Events.count { it.ResultCode }

        drawing.with {
            ver = json.Version
            state = json.State
            complexity = json.Complexity
            pool = json.Pool
            jackpot = json.Jackpot
            couponCount = json.CouponCount
            stakeCount = json.StakeCount
            optionCount = json.OptionCount
            expired = DateUtils.parseDateFromJson(json.Expired)
            winnersCode = null
            results = null
        }

        if (drawing.events) {
            drawing.events.each { event ->
                def eventNode = json.Details.Events.find { it.Order == event.sortOrder }
                event.with {
                    extId = eventNode.Id
                    name = eventNode.Names.find({ it.Key == "ru" }).Value
                    date = DateUtils.parseDateFromJson(eventNode.Date)
                    resultCode = eventNode.ResultCode
                    score = eventNode.ResultCode == "0" ? "-:-" : eventNode.Score
                    drawPercentage = eventNode.UserDraw.Percentage
                    win1Percentage = eventNode.UserWin1.Percentage
                    win2Percentage = eventNode.UserWin2.Percentage
                    drawProbability = eventNode.UserDraw.Probability
                    win1Probability = eventNode.UserWin1.Probability
                    win2Probability = eventNode.UserWin2.Probability
                }
            }
        } else {
            for (event in json.Details.Events) {
                drawing.addToEvents(
                    new Event(
                        drawing: drawing,
                        extId: event.Id,
                        sortOrder: event.Order,
                        name: event.Names.find({ it.Key == "ru" }).Value,
                        date: DateUtils.parseDateFromJson(event.Date),
                        resultCode: event.ResultCode,
                        score: event.ResultCode == "0" ? "-:-" : event.Score,
                        drawPercentage: event.UserDraw.Percentage,
                        win1Percentage: event.UserWin1.Percentage,
                        win2Percentage: event.UserWin2.Percentage,
                        drawProbability: event.UserDraw.Probability,
                        win1Probability: event.UserWin1.Probability,
                        win2Probability: event.UserWin2.Probability
                    )
                )
            }
        }

        if (drawing.state == 1) {
            def results = []
            json.Details.DefaultResult.DrawingResults.each { result ->
                results << [
                    count : result.Count,
                    result: result.Result
                ]
            }

            drawing.results = results
            drawing.winnersCode = drawing.events.collect { it.resultCode }.join()
            updateWinnersCodeCacheArchive(drawing)
        }

        if (!drawing.save(flush: true)) {
            log.error("[$drawing.number] Error saving drawing. Errors: $drawing.errors")
            return
        }

        if (needUpdateLastOperations) {
            userService.updateLastOperations(drawing)
        }

        drawing
    }

    void clearOldDrawingData(List<Drawing> drawings = []) {

        log.info("Start clearing old drawing's data.")

        if (!drawings) {
            log.info("Find old drawing's data in database")
            drawings = Drawing.findAllByNumberNotInList(allDrawingNumbers)
        }

        if (drawings) {
            def staleLinks = StakeLinks.findAllByDrawingDateInList(drawings.expired)

            if (staleLinks) {
                log.info("Delete stake links")
                StakeLinks.deleteAll(staleLinks)
            }

            def stakes = StakeDict.findAllByDrawingDateInList(drawings.expired)

            if (stakes) {
                log.info("Delete stake dict")
                StakeDict.deleteAll(stakes)
            }
        }

        log.info("Finish clearing old drawing's data.")
    }

    private void clearDrawingData(Drawing drawing) {
        clearOldDrawingData([drawing])
    }

    Drawing getCurrentDrawing() {

        if (currentDrawing) {
            return currentDrawing
        }

        for (number in allDrawingNumbers) {
            def drawing = getDrawing(number, true)

            if (drawing.state == 2) {
                currentDrawing = drawing
                break
            }
        }

        currentDrawing
    }

    private updateWinnersCodeCacheArchive = { Drawing drawing ->

        if (!drawing.winnersCode) {
            return
        }

        def archive = AverageCodeCacheArchive.findAllByDrawingNumber(drawing.number)
        archive.each { it.winnersCount = countWinMatches(it.code, drawing.winnersCode) }
        AverageCodeCacheArchive.saveAll(archive)

        resetAverageCache(drawing)
    }

    private getEventResultsGroupBySortOrder() {

        def total = Event.countByResultCodeIsNotNull()
        def eventResultsGroupBySortOrderCache = grailsCacheManager.getCache("eventResultsGroupBySortOrderCache", 1)
        def result = eventResultsGroupBySortOrderCache.get("eventResultsGroupBySortOrderCache")?.get() ?: [:]

        if (result.total == total) {
            log.info("Return event results group by sortOrder from cache")
            return result
        }

        eventResultsGroupBySortOrderCache.clear()
        result = [total: total]

        def events = []

        allDrawingNumbers.reverse().each { number ->
            def drawing = getDrawing(number)
            if (drawing?.state == 1) {
                events.addAll(drawing.events)
            }
        }

        result.events = events.groupBy { it.sortOrder }

        eventResultsGroupBySortOrderCache.put("eventResultsGroupBySortOrderCache", result)

        result
    }

    def collectEventResults() {

        def eventResults = eventResultsGroupBySortOrder
        def collectedEventResultCache = grailsCacheManager.getCache("collectedEventResultCache", 1)
        def result = collectedEventResultCache.get("collectedEventResultCache")?.get() ?: [:]

        if (result.total == eventResults.total) {
            log.info("Return collected event results from cache")
            return result.data
        }

        collectedEventResultCache.clear()
        result = [total: eventResults.total, data: []]

        eventResults.events.each { k, v ->
            def drawCount = v.count { it.resultCode == "X" }
            def win1Count = v.count { it.resultCode == "1" }
            def win2Count = v.count { it.resultCode == "2" }
            result.data << [
                draw          : drawCount,
                win1          : win1Count,
                win2          : win2Count,
                drawPercentage: drawCount / v.size() * 100,
                win1Percentage: win1Count / v.size() * 100,
                win2Percentage: win2Count / v.size() * 100
            ]
        }

        collectedEventResultCache.put("collectedEventResultCache", result)

        result.data
    }

    def getEventResultsChartData() {

        def eventResults = eventResultsGroupBySortOrder
        def eventResultsChartDataCache = grailsCacheManager.getCache("eventResultsChartDataCache", 1)
        def result = eventResultsChartDataCache.get("eventResultsChartDataCache")?.get() ?: [:]

        if (result.total == eventResults.total) {
            log.info("Return event results chart data from cache")
            return result.data
        }

        eventResultsChartDataCache.clear()
        result = [total: eventResults.total, data: [:]]

        eventResults.events.each { k, v ->
            if (!result.data["l${k}"]) {
                result.data["l${k}"] = []
            }

            def counter = [win1: 0, winX: 0, win2: 0]

            v.eachWithIndex { event, i ->

                if (event.resultCode != "0") {
                    counter."win${event.resultCode}" += 1
                }

                result.data["l${k}"] << [i, counter.win1, counter.winX, counter.win2]
            }
        }

        eventResultsChartDataCache.put("eventResultsChartDataCache", result)

        result.data
    }

    def getEventChartData(Drawing drawing, String type) {

        if (!type) {
            type = "Percentage"
        }

        def total = Drawing.countByNumberAndExpired(drawing.number, drawing.expired)
        def chartDataCache = grailsCacheManager.getCache("chartDataCache", 10)

        def result = chartDataCache.get(drawing.number)?.get() ?: [:]

        if (result.total == total && result.type == type) {
            log.info("[$drawing.number] Return chart data from cache")
            return result.data
        }

        chartDataCache.evict(drawing.number)
        result = [total: total, type: type, data: [:]]

        Drawing.findAllByNumberAndExpired(drawing.number, drawing.expired, [sort: 'id']).events.eachWithIndex { events, i ->

            events.each { event ->

                if (!result.data."l${event.sortOrder}") {
                    result.data."l${event.sortOrder}" = [
                        name: event.nameWithResult,
                        data: []
                    ]
                }

                if (event.resultCode) {
                    result.data."l${event.sortOrder}".result = event.resultCode
                    result.data."l${event.sortOrder}".name = event.nameWithResult
                }

                result.data["l${event.sortOrder}"].data << [i, event."win1$type", event."draw$type", event."win2$type"]
            }
        }

        chartDataCache.put(drawing.number, result)

        result.data
    }

    private Boolean needUpdateStakeDict(Drawing drawing) {

        if (schedulerService.checkExists(UpdateStakeDictJob.class, drawing.number)) {
            return false
        }

        if (drawing.state == 2) {
            return DateUtils.plusHours(-1, drawing.expired).before(new Date())
        }

        drawing.state == 1 &&
            (drawing.winnersCode && StakeDict.countByDrawingDateAndWinCountIsNull(drawing.expired, [max: 1]) ||
                countStakeDictTotal(drawing) < drawing.stakeCount ||
                countStakeDictUnique(drawing) < drawing.optionCount) ||
            needUpdateStakeLinks(drawing) ||
            StakeDict.withCriteria {
                eq("drawingDate", drawing.expired)
                or {
                    isNull("winnersAverage")
                    isNull("poolAverage")
                    isNull("lineAverage")
                }
                maxResults(1)
                projections {
                    count()
                }
            }
    }

    Boolean needUpdateStakeLinks(Drawing drawing) {

        if (drawing.state == 0) {
            return false
        }

        def stakeLinks = StakeLinks.findByDrawingDate(drawing.expired)

        !stakeLinks || stakeLinks.total != drawing.optionCount || drawing.state == 1 && (!stakeLinks.winnersCode || !stakeLinks.winnersAverage)
    }

    Boolean needUpdateWinnersAverage(Drawing drawing) {
        drawing.state != 2 || DateUtils.plusMinutes(-30, drawing.expired).after(new Date())
    }

    Integer countStakeDictUnique(Drawing drawing) {
        StakeDict.countByDrawingDate(drawing.expired)
    }

    Integer countStakeDictTotal(Drawing drawing) {

        StakeDict.withCriteria(uniqueResult: true) {
            eq("drawingDate", drawing.expired)
            projections {
                sum("count")
            }
        }
    }

    Boolean startUpdateStakeDict(Drawing drawing) {
        schedulerService.scheduleJob(UpdateStakeDictJob.class, [drawingNumber: drawing.number])
    }

    Boolean checkUpdateStakeDic(Drawing drawing) {
        schedulerService.checkExists(UpdateStakeDictJob.class, drawing.number)
    }

    Integer getStakeDict(Drawing drawing, Integer startFrom = 0, Integer urlIndex = null) {

        if (!drawing) {
            return
        }

        def options = [
            "DrawingId": drawing.number,
            "StartFrom": startFrom,
            "Count"    : 200
        ]

        def result = httpClientService.post(urlPrefix + "GetStakeDict", [options: options], urlIndex)?.d

        if (!result || result.Summary.TotalCount == 0) {
            return
        }

        def existingStakeCount = StakeDict.withCriteria(uniqueResult: true) {
            eq("drawingDate", drawing.expired)

            if (drawing.state == 1) {
                or {
                    result.Items.each { item ->
                        and {
                            isNotNull("winCount")
                            eq("code", item.Code)
                            eq("count", item.Count)
                        }
                    }
                }
            } else {
                'in'("code", result.Items*.Code)
            }

            projections {
                count()
            }
        }

        if (existingStakeCount == result.Items*.Code.size()) {
            return result.Summary.TotalCount
        }

        def stakes = []

        for (item in result.Items) {
            def stakeDict = StakeDict.findOrCreateByDrawingDateAndDrawingIdAndCode(drawing.expired, drawing.number, item.Code)

            if (!stakeDict.id && drawing.state != 1) {
                stakeDict.count = item.Count
                def averageData = getCodeAverage(drawing, item.Code)
                stakeDict.poolAverage = averageData.pool
                stakeDict.lineAverage = averageData.line
                stakes << stakeDict
                continue
            }

            def changed = false

            if (stakeDict.count != item.Count) {
                stakeDict.count = item.Count
                changed = true
            }

            if (stakeDict.winCount == null && drawing.winnersCode) {
                stakeDict.winCount = countWinMatches(item.Code, drawing.winnersCode)
                changed = true
            }

            if (changed) {
                def averageData = getCodeAverage(drawing, item.Code)
                stakeDict.poolAverage = averageData.pool
                stakeDict.lineAverage = averageData.line
                stakes << stakeDict
            }
        }

        StakeDict.saveAll(stakes)

        result.Summary.TotalCount
    }

    void updateStakeDict(Drawing drawing, StakeLinks stakeLinks = null) {

        if (!stakeLinks) {
            stakeLinks = getStakeLinks(drawing)
        }

        if (!stakeLinks && !drawing.winnersCode && !needUpdateWinnersAverage(drawing)) {
            return
        }

        def total = StakeDict.withCriteria(uniqueResult: true) {
            eq("drawingDate", drawing.expired)

            if ((!stakeLinks || !needUpdateWinnersAverage(drawing)) && drawing.winnersCode) {
                isNull("winCount")
            }

            projections {
                count()
            }
        }

        if (!total) {
            return
        }

        def offset = 0

        while (offset < total) {
            def stakes = StakeDict.withCriteria {
                eq("drawingDate", drawing.expired)

                if (!stakeLinks && drawing.winnersCode) {
                    isNull("winCount")
                }

                firstResult(offset)
                maxResults(1000)
            }

            def stakeDictToSave = []

            stakes.each { StakeDict stake ->
                def changed = false

                if (stakeLinks && needUpdateWinnersAverage(drawing)) {
                    def winnersAverage = getWinnersAverage(stakeLinks, stake.code)

                    if (stake.winnersAverage != winnersAverage) {
                        stake.winnersAverage = winnersAverage
                        changed = true
                    }
                }

                if (!stake.winCount && drawing.winnersCode) {
                    stake.winCount = countWinMatches(stake.code, drawing.winnersCode)
                    changed = true
                }

                if (!stake.poolAverage || !stake.lineAverage) {
                    def averageData = getCodeAverage(drawing, stake.code)
                    stake.poolAverage = averageData.pool
                    stake.lineAverage = averageData.line
                    changed = true
                }

                if (changed) {
                    stakeDictToSave << stake
                }
            }

            StakeDict.saveAll(stakeDictToSave)
            offset += stakes.size()

            def session = sessionFactory.getCurrentSession()
            session.flush()
            session.clear()
        }
    }

    Integer calcStakeWinMatches14(String code, Date drawingDate) {

        StakeDict.withCriteria(uniqueResult: true) {
            eq("drawingDate", drawingDate)
            or {
                (0..14).each { n ->
                    like("code", (n == 0 ? "_" + code[n + 1..-1] : code[0..n - 1] + "_" + (n == 14 ? "" : code[n + 1..-1])))
                }
            }
            projections {
                sum("count")
            }
        } ?: 0
    }

    Integer calcStakeWinMatches13(String code, Date drawingDate) {

        StakeDict.withCriteria(uniqueResult: true) {
            eq("drawingDate", drawingDate)
            or {
                (0..13).each { n->
                    def code2 = (n == 0 ? "_" + code[n + 1..-1] : code[0..n - 1] + "_" + code[n + 1..-1])
                    (n + 1..14).each { n2 ->
                        like("code", (code2[0..n2 - 1] + "_" + (n2 == 14 ? "" : code2[n2 + 1..-1])))
                    }
                }
            }
            projections {
                sum("count")
            }
        } ?: 0
    }

    Integer calcStakeWinMatches12(String code, Date drawingDate) {

        StakeDict.withCriteria(uniqueResult: true) {
            eq("drawingDate", drawingDate)
            or {
                (0..12).each { n ->
                    def code2 = (n == 0 ? "_" + code[n + 1..-1] : code[0..n - 1] + "_" + code[n + 1..-1])
                    (n + 1..13).each { n2 ->
                        def code3 = code2[0..n2 - 1] + "_" + code2[n2 + 1..-1]
                        (n2 + 1..14).each { n3 ->
                            like("code", (code3[0..n3 - 1] + "_" + (n3 == 14 ? "" : code3[n3 + 1..-1])))
                        }
                    }
                }
            }
            projections {
                sum("count")
            }
        } ?: 0
    }

    Map calcStakeWinMatches(String code, Date drawingDate, Boolean with12 = false) {

        def winMatches = [
            m15: StakeDict.findByDrawingDateAndCode(drawingDate, code)?.count ?: 0,
            m14: calcStakeWinMatches14(code, drawingDate),
            m13: calcStakeWinMatches13(code, drawingDate)
        ]

        if (with12) {
            winMatches.m12 = calcStakeWinMatches12(code, drawingDate)
        }

        winMatches
    }

    BigDecimal getWinnersAverage(Drawing drawing, String code = null) {

        def stakeLinks = getStakeLinks(drawing)

        if (!stakeLinks) {
            return BigDecimal.ZERO
        }

        getWinnersAverage(stakeLinks, code ?: drawing.winnersCode)
    }

    BigDecimal getWinnersAverage(StakeLinks stakeLinks, String code) {

        if (!stakeLinks || !code) {
            return BigDecimal.ZERO
        }

        def sum = stakeLinks.data[0][code[-1] + code[0]] ?: stakeLinks.average

        (1..14).each { n ->
            sum += stakeLinks.data[n][code[n - 1..n]] ?: stakeLinks.average
        }

        (sum / 15 / stakeLinks.average * 100).setScale(2, RoundingMode.HALF_UP)
    }

    Map countUniqueStakes(Drawing drawing, Integer winCount, String customWinners = null) {

        def winners = drawing.winnersCode ?: customWinners

        if (winCount == null || !winners) {
            return [:]
        }

        def total = drawing.state != 2 ? drawing.optionCount : countStakeDictUnique(drawing)

        if (needUpdateStakeDict(drawing)) {
            startUpdateStakeDict(drawing)
        }

        if (!total) {
            return [:]
        }

        def cacheName = "${drawing.number}_${winCount}_${winners}"
        def countUniqueStakesCache = grailsCacheManager.getCache("countUniqueStakes")
        def result = countUniqueStakesCache.get(cacheName)?.get() ?: [:]

        if (result.total == total) {
            log.info("[$drawing.number] Return count unique stake's data from cache")
            return result
        }

        countUniqueStakesCache.evict(cacheName)

        result = [total: total, data: []]

        15.times { result.data << [0, 0, 0] }

        def stakes = StakeDict.withCriteria {
            eq("drawingDate", drawing.expired)

            if (drawing.state == 1) {
                or {
                    ge("winCount", winCount)
                    isNull("winCount")
                }
            }
        }

        if (!stakes) {
            return [:]
        }

        stakes = stakes.findAll {
            (winners ? countWinMatches(it.code, winners) : it.winCount != null ? it.winCount : 0) >= winCount
        }

        stakes.each { stake ->
            stake.code.eachWithIndex { s, i ->
                switch (s) {
                    case "1":
                        result.data[i][0] += stake.count
                        break
                    case "X":
                        result.data[i][1] += stake.count
                        break
                    case "2":
                        result.data[i][2] += stake.count
                }
            }
        }

        result.count = stakes.sum { it.count }

        countUniqueStakesCache.put(cacheName, result)

        result
    }

    Map getUniqueStakes(Drawing drawing, List winners) {

        if (!winners?.any { it && it != "_" }) {
            return [:]
        }

        def total = drawing.state != 2 ? drawing.optionCount : countStakeDictUnique(drawing)

        if (needUpdateStakeDict(drawing)) {
            startUpdateStakeDict(drawing)
        }

        if (!total) {
            return [:]
        }

        def cacheName = "${drawing.number}_" + winners.join()
        def uniqueStakesCache = grailsCacheManager.getCache("uniqueStakesCache")
        def result = uniqueStakesCache.get(cacheName)?.get() ?: [:]

        if (result.total == total) {
            log.info("[$drawing.number] Return unique stake's data from cache")
            return result
        }

        uniqueStakesCache.evict(cacheName)

        result = [
            total    : total,
            data     : []
        ]

        def stakes = StakeDict.withCriteria {
            eq("drawingDate", drawing.expired)
            like("code", winners.join())
        }

        if (!stakes) {
            return result
        }

        15.times { result.data << [0, 0, 0] }

        stakes.each { stake ->

            stake.code.eachWithIndex { s, i ->
                switch (s) {
                    case "1":
                        result.data[i][0] += stake.count
                        break
                    case "X":
                        result.data[i][1] += stake.count
                        break
                    case "2":
                        result.data[i][2] += stake.count
                }
            }
        }

        result.winners = winners
        result.count = stakes.sum { it.count }

        uniqueStakesCache.put(cacheName, result)

        result
    }

    static Integer countWinMatches(String coupon, String winners = null) {

        if (!winners) {
            return 15
        }

        Integer count = 0

        (0..14).each { n ->
            if (winners.charAt(n) in ["_", "0", coupon.charAt(n)]) {
                count++
            }
        }

        count
    }

    StakeLinks updateStakeLinks(Drawing drawing) {

        if (drawing.state == 0) {
            return
        }

        def stakeLinks = StakeLinks.findOrCreateByDrawingNumberAndDrawingDate(drawing.number, drawing.expired)

        if (stakeLinks.total == drawing.optionCount && stakeLinks.winnersCode && stakeLinks.winnersAverage) {
            return
        }

        if (stakeLinks.total != drawing.optionCount) {

            def uniqueCodes = StakeDict.withCriteria {
                eq("drawingDate", drawing.expired)
                projections {
                    property("code")
                }
            }

            stakeLinks.total = uniqueCodes.size()
            stakeLinks.data = calcStakeLinkData(uniqueCodes)
            stakeLinks.average = stakeLinks.data?.get(0)?.values()?.sum() / 9
            stakeLinks.drawingDate = drawing.expired
        }

        if (drawing.winnersCode && (!stakeLinks.winnersCode || !stakeLinks.winnersAverage)) {
            stakeLinks.winnersCode = drawing.winnersCode
            stakeLinks.winnersAverage = getWinnersAverage(stakeLinks, drawing.winnersCode)
        }

        if (!stakeLinks.save()) {
            log.error("[$drawing.number] Can't to save stake links. Errors: $stakeLinks.errors")
            return
        }

        stakeLinks
    }

    def calcStakeLinkData(codes) {

        def data = []

        (0..14).each { n ->
            def links = [:]

            ["11", "1X", "12", "X1", "XX", "X2", "21", "2X", "22"].each { link ->
                links[link] = codes.count { link == (n == 0 ? it[-1] + it[0] : it[n - 1..n]) }
            }

            data << links.sort { -it.value }
        }

        data
    }

    def calcAverageData(Drawing drawing) {

        def cacheName = "${drawing.number}"
        def averageDataCache = grailsCacheManager.getCache("averageData")
        def result = averageDataCache.get(cacheName)?.get() ?: [:]

        if (result.ver == drawing.ver) {
            return result.data
        }

        averageDataCache.evict(cacheName)

        result.ver = drawing.ver

        def averages = []
        def events = Event.findAllByDrawing(drawing).sort { it.sortOrder }

        for (event in events) {
            averages << [
                poolX: event.drawPercentage,
                pool1: event.win1Percentage,
                pool2: event.win2Percentage,
                lineX: event.drawProbability,
                line1: event.win1Probability,
                line2: event.win2Probability
            ]
        }

        def poolData = []
        def lineData = []

        (0..13).each { n ->
            def pool = [:]
            def line = [:]

            ["11", "1X", "12", "X1", "XX", "X2", "21", "2X", "22"].each { link ->
                pool[link] = averages[n]."pool${link[0]}" + averages[n + 1]."pool${link[1]}"
                line[link] = averages[n]."line${link[0]}" + averages[n + 1]."line${link[1]}"
            }

            poolData << pool
            lineData << line
        }

        result.data = [
            pool: poolData,
            line: lineData
        ]

        averageDataCache.put(cacheName, result)

        result.data
    }

    def getCodeAverage(Drawing drawing, String code = null) {

        code = code ?: drawing.winnersCode

        if (!code) {
            return [:]
        }

        def averages = calcAverageData(drawing)

        BigDecimal poolAverage = BigDecimal.ZERO
        BigDecimal lineAverage = BigDecimal.ZERO

        (0..13).each { n ->
            poolAverage += averages.pool[n][code[n..n + 1]] ?: 66.67
            lineAverage += averages.line[n][code[n..n + 1]] ?: 66.67
        }

        [
            pool: (poolAverage / 14 / 0.67).setScale(2, RoundingMode.HALF_UP),
            line: (lineAverage / 14 / 0.67).setScale(2, RoundingMode.HALF_UP)
        ]
    }

    StakeLinks getStakeLinks(Drawing drawing) {

        def stakeLinks = StakeLinks.findByDrawingDate(drawing.expired)

        if (!stakeLinks && drawing.state == 1 && countStakeDictUnique(drawing) == drawing.optionCount) {
            return updateStakeLinks(drawing)
        }

        stakeLinks
    }

    private static void fillDefaultAverageParams(Drawing drawing, Map params = [:]) {

        params.averageType = params.averageType ?: "winners"

        params.winnersMin = (![null, ""].contains(params.winnersMin) ? params.winnersMin : 100) as Integer
        params.winnersMax = (![null, ""].contains(params.winnersMax) ? params.winnersMax : 110) as Integer
        params.averageMin = (![null, ""].contains(params.averageMin) ? params.averageMin : 100) as Integer
        params.averageMax = (![null, ""].contains(params.averageMax) ? params.averageMax : 110) as Integer
        params.from13 = (![null, ""].contains(params.from13) ? params.from13 : (drawing.stakeCount / 100000)) as Integer
        params.to13 = (![null, ""].contains(params.to13) ? params.to13 : (drawing.stakeCount / 10000)) as Integer
        params.from14 = (![null, ""].contains(params.from14) ? params.from14 : 1) as Integer
        params.to14 = (![null, ""].contains(params.to14) ? params.to14 : (drawing.stakeCount / 100000)) as Integer
        params.howMany = (params.howMany as Integer) ?: 20
        params.onlyUnique = params.onlyUnique != null ? params.onlyUnique : true
    }

    Map findWinnersByAverages(Drawing drawing, Map params = [:]) {

        def stakeLinks = getStakeLinks(drawing)

        if (!stakeLinks) {
            return [:]
        }

        fillDefaultAverageParams(drawing,  params)

        def winCodes = [] as Set

        if (params.count) {
            def minMaxAverage = (stakeLinks.data[0].min { it.value }.value + stakeLinks.data[0].max { it.value }.value) / 2
            def fromMin = minMaxAverage / stakeLinks.average > 1.2

            def minAverage = stakeLinks.data[0].findAll {
                fromMin ? it.value / stakeLinks.average < 1 : it.value / stakeLinks.average > 1
            }

            minAverage.each { startPoint ->
                def code = startPoint.key[1]

                (1..14).each { n ->
                    def links = stakeLinks.data[n].findAll { it.key.startsWith(code[-1]) }
                    minMaxAverage = (stakeLinks.data[0].min { it.value }.value + stakeLinks.data[0].max { it.value }.value) / 2
                    fromMin = minMaxAverage / stakeLinks.average > 1.2

                    def minLinks = links.findAll {
                        fromMin ? it.value / stakeLinks.average < 1 : it.value / stakeLinks.average > 1
                    }

                    if (minLinks) {
                        code += fromMin ? minLinks.max { it.value }.key[1] : minLinks.min { it.value }.key[1]
                    } else {

                        minLinks = links.findAll {
                            fromMin ? it.value / stakeLinks.average > 1 : it.value / stakeLinks.average < 1
                        }

                        code += fromMin ? minLinks.min { it.value }.key[1] : minLinks.max { it.value }.key[1]
                    }
                }

                winCodes << code
            }
        }

        def possibleData = []

        if (!params.count) {

            if (stakeLinks.winnersCode) {
                AverageCodeCacheArchive.withCriteria {
                    eq("drawingNumber", drawing.number)
                    ge("winnersCount", 9)
                    order("winnersCount", "desc")
                }.each { cacheArchive ->

                    def drawingResults = calculateCoupons(drawing, cacheArchive.code)
                    def m13 = drawingResults.find { it.Result == 13 }?.Count
                    def m14 = drawingResults.find { it.Result == 14 }?.Count
                    def m15 = drawingResults.find { it.Result == 15 }?.Count

                    def data = [
                        code   : cacheArchive.code,
                        average: cacheArchive.average,
                        m13    : m13,
                        m14    : m14,
                        m15    : m15,
                        matches: cacheArchive.winnersCount
                    ]

                    possibleData << data
                }
            } else {
                if (params.packetSize) {
                    def howMany = (params.howMany ?: 20) as Integer
                    def packetSize = params.packetSize as Integer

                    while (true) {
                        if (howMany < packetSize) {
                            packetSize = howMany
                        }

                        params.averageRanges = params.intellectual ? getIntellectualParams(params.monthCount as Integer, packetSize, (params.minWinCount as Integer ) ?: 13, params.averageType) : null
                        params.howMany = packetSize

                        possibleData += params.averageRanges ? getAverageWinnersCodesEx(drawing, params, stakeLinks, possibleData) : getAverageWinnersCodes(drawing, params, stakeLinks)

                        if (howMany == packetSize) {
                            break
                        }

                        howMany -= possibleData.size()
                    }
                } else {
                    possibleData = params.averageRanges ? getAverageWinnersCodesEx(drawing, params, stakeLinks) : getAverageWinnersCodes(drawing, params, stakeLinks)
                }
            }
        } else {
            winCodes.each { code ->
                def average = getWinnersAverage(stakeLinks, code)

                if (average >= params.averageMin && average <= params.averageMax) {
                    def data = [
                        code   : code,
                        average: average
                    ]

                    if (stakeLinks.winnersCode) {
                        data.matches = countWinMatches(code, stakeLinks.winnersCode)
                    }

                    possibleData << data
                }
            }
        }

        def result = [
            possible     : possibleData.sort { -it.average },
            from13       : params.from13,
            to13         : params.to13,
            from14       : params.from14,
            to14         : params.to14,
            averageMax   : params.averageMax,
            averageMin   : params.averageMin,
            averageType  : params.averageType,
            mask         : params.mask,
            excludes     : params.excludes,
            onlyUnique   : params.onlyUnique,
            minWinCount  : params.minWinCount,
            monthCount   : params.monthCount,
            withoutCharts: params.withoutCharts,
            chartsData   : []
        ]

        if (params.withoutCharts) {
            return result
        }

        if (stakeLinks.winnersCode) {
            def chartData = getWinnersStakeLinksChartData(drawing, stakeLinks.winnersCode, stakeLinks)

            if (drawing.results) {
                chartData += [
                    m13: drawing.results.find { it.result == 13 }?.count,
                    m14: drawing.results.find { it.result == 14 }?.count,
                    m15: drawing.results.find { it.result == 15 }?.count
                ]
            }

            chartData += [matches: 15]
            result.chartsData << chartData
        }

        result.possible.each {
            result.chartsData << getWinnersStakeLinksChartData(drawing, it.code, stakeLinks) +
                [m13: it.m13, m14: it.m14, m15: it.m15, matches: it.matches, maxWin: it.maxWin, minWin: it.minWin, packetCode: it.packetCode]
        }

        result
    }

    def getAverageWinnersCodesEx(Drawing drawing, Map params, stakeLinks = null, packetData = []) {

        if (!stakeLinks) {
            stakeLinks = getStakeLinks(drawing)

            if (!stakeLinks) {
                return []
            }
        }

        drawing = getDrawing(drawing.number)

        fillDefaultAverageParams(drawing,  params)

        def maxCode = drawing.maxCode
        def minCode = drawing.minCode
        def maxExcludes = 15
        def minExcludes = 15

        if (params.excludes && params.excludes.any { it != "_"}) {
            def excludesCode = params.excludes instanceof String ? params.excludes : params.excludes.join()
            maxExcludes -= countWinMatches(excludesCode, maxCode)
            minExcludes -= countWinMatches(excludesCode, minCode)

            if (params.mask && params.mask.any { it != "_"}) {
                def maskCode = params.mask instanceof String ? params.mask : params.mask.join()

                (0..14).each { n ->
                    if (maskCode.charAt(n) != "_") {
                        if (maskCode.charAt(n) != maxCode.charAt(n)) {
                            maxExcludes--
                        }
                        if (maskCode.charAt(n) != minCode.charAt(n)) {
                            minExcludes--
                        }
                    }
                }
            }

            log.info("Excludes max: $maxExcludes min: $minExcludes")
        }

        def maxMinWinnersCount = maxMinWinnersCount([monthCount: params.monthCount]).sort { it.ratio }
        def maxMinCount = []
        def howMany = params.packSize && params.howMany > params.packSize ? params.packSize : params.howMany

        for (max in maxMinWinnersCount) {
            def total = (max.ratio * howMany / 100).setScale(0, RoundingMode.DOWN)

            if (max == maxMinWinnersCount.last()) {
                total = howMany - (maxMinCount ? maxMinCount.sum { it.total } : 0)
            }

            if (!total) {
                continue
            }

            def maxCount = max.max > maxExcludes ? maxExcludes : max.max

            maxMinCount << [max: maxCount, min: max.min, total: total]
        }

        def result = []
        def codes = []

        params.averageRanges.each { range ->
            log.info("Searching $range.count codes with range $range.min-$range.max")

            def rangeResult = []
            def startTime = System.currentTimeMillis()
            def skippingMax = []
            def win9attempts = 0

            while (rangeResult.size() < range.count && !skippingMax.contains(range.max)) {

                if (System.currentTimeMillis() - startTime > 60000) {
                    skippingMax << range.max
                    log.warn("Searching code is too long, break searching with range $range.min-$range.max [$skippingMax]")

                    break
                }

                def code = getRandomCode(params.mask, params.excludes)
                def win9 = !rangeResult

                for (c in result.code) {
                    if (countWinMatches(code, c) > 9) {
                        win9attempts = 0
                        win9 = true
                        break
                    }
                }

                if (!win9) {
                    win9attempts++

                    if (win9attempts > (range.count * 10)) {
                        win9attempts = 0
                        rangeResult = []
                    }

                    continue
                }

                def maxWin = countWinMatches(code, maxCode)
                def minWin = countWinMatches(code, minCode)

                if (!maxMinCount.max.contains(maxWin) ||
                    packetData && packetData.code.contains(code) ||
                    codes.contains(code) ||
                    isExceedsMaxMatch(code) ||
                    RegisteredCoupon.countByCodeAndDrawingNumber(code, drawing.number)) {

                    continue
                }

                def codeAverage = getWinnersAverage(stakeLinks, code)
                def averageRangeStart = ((codeAverage / 10).toInteger() * 10)

                if (range.min != averageRangeStart) {
                    continue
                }

                def shown = false
                def interval = "$range.min-$range.max"

                def drawingResults
                def m13 = 0
                def m14 = 0
                def m15 = 0

                if (params.onlyUnique) {
                    drawingResults = drawing.state != 2 ? calcStakeWinMatches(code, drawing.expired) : calculateCoupons(drawing, code)
                    m13 = drawing.state != 2 ? drawingResults.m13 : drawingResults.find { it.Result == 13 }?.Count
                    m14 = drawing.state != 2 ? drawingResults.m14 : drawingResults.find { it.Result == 14 }?.Count
                    m15 = drawing.state != 2 ? drawingResults.m15 : drawingResults.find { it.Result == 15 }?.Count
                }

                if (m15 == 0) {
                    def packetCode = "50"
                    code.eachWithIndex { s, i -> packetCode += "; ${i + 1}-($s)" }
                    packetCode += "."

                    def data = [
                        code      : code,
                        average   : codeAverage,
                        m13       : m13,
                        m14       : m14,
                        m15       : m15,
                        maxWin    : maxWin,
                        minWin    : minWin,
                        packetCode: packetCode
                    ]

                    shown = true
                    rangeResult << data
                }

                if (!AverageCodeCacheArchive.countByDrawingNumberAndCodeAndInterval(drawing.number, code, interval)) {
                    new AverageCodeCacheArchive(
                        code: code,
                        drawingNumber: drawing.number,
                        winnersCount: 0,
                        shown: shown,
                        average: codeAverage,
                        interval: interval
                    ).save(flush: true)
                }

                codes << code
            }

            result.addAll(rangeResult)
            codes = result.code
        }

        result
    }

    def getAverageWinnersCodes(Drawing drawing, Map params = [:], stakeLinks = null) {

        if (!stakeLinks) {
            stakeLinks = getStakeLinks(drawing)

            if (!stakeLinks) {
                return []
            }
        }

        drawing = getDrawing(drawing.number)

        fillDefaultAverageParams(drawing, params)

        def averageWinnersCodesCache = grailsCacheManager.getCache("averageWinnersCodesCache")
        def interval = "$params.averageMin-$params.averageMax"
        def cacheName = "${drawing.number}_${params.averageType ?: 'winners'}_${interval}"
        def codes = averageWinnersCodesCache.get(cacheName)?.get() ?: []
        averageWinnersCodesCache.evict(cacheName)

        def maxWinnersCount = maxWinnersCount([monthCount: params.monthCount])

        def maxCount = []

        for (max in maxWinnersCount) {
            def count = (max.ratio * params.howMany / 100).setScale(0, RoundingMode.DOWN)

            if (!count) {
                continue
            }

            maxCount << max.count
        }

        def minWinnersCount = minWinnersCount([monthCount: params.monthCount])

        def minCount = []

        for (min in minWinnersCount) {
            def count = (min.ratio * params.howMany / 100).setScale(0, RoundingMode.DOWN)

            if (!count) {
                continue
            }

            minCount << min.count
        }

        def maxCode = drawing.maxCode
        def minCode = drawing.minCode

        def iterations = 0
        def result = []
        def win9attempts = 0

        while (result.size() < params.howMany) {
            iterations++

            def code = getRandomCode(params.mask, params.excludes)
            def win9 = !result

            for (c in result.code) {
                if (countWinMatches(code, c) > 9) {
                    win9attempts = 0
                    win9 = true
                    break
                }
            }

            if (!win9) {
                win9attempts++

                if (win9attempts > (params.howMany * 10)) {
                    win9attempts = 0
                    result = []
                }

                continue
            }

            def maxWin = countWinMatches(code, maxCode)
            def minWin = countWinMatches(code, minCode)
            def maxWinCount = maxCount.find { it == maxWin }
            def minWinCount = minCount.find { it == minWin }

            if (!maxWinCount || !minWinCount || codes.contains(code) || isExceedsMaxMatch(code) || RegisteredCoupon.countByCodeAndDrawingNumber(code, drawing.number)) {
                continue
            }

            def codeAverage = !params.averageType || params.averageType == 'winners' ? getWinnersAverage(stakeLinks, code) : getCodeAverage(drawing, code)[params.averageType]
            def shown = false

            if (codeAverage >= params.averageMin && codeAverage <= params.averageMax) {
                def drawingResults = calculateCoupons(drawing, code)
                def m13 = drawingResults.find { it.Result == 13 }?.Count
                def m14 = drawingResults.find { it.Result == 14 }?.Count
                def m15 = drawingResults.find { it.Result == 15 }?.Count

                if (m13 in (params.from13..params.to13) && m14 in (params.from14..params.to14) && (!params.onlyUnique || m15 == 0)) {
                    def packetCode = "50"
                    code.eachWithIndex { s, i -> packetCode += "; ${i + 1}-($s)" }
                    packetCode += "."
                    def data = [
                        code      : code,
                        average   : codeAverage,
                        m13       : m13,
                        m14       : m14,
                        m15       : m15,
                        maxWin    : maxWin,
                        minWin    : minWin,
                        packetCode: packetCode
                    ]

                    shown = true
                    result << data
                }
            }

            if (!AverageCodeCacheArchive.countByDrawingNumberAndCodeAndInterval(drawing.number, code, interval)) {
                new AverageCodeCacheArchive(
                    code: code,
                    drawingNumber: drawing.number,
                    winnersCount: 0,
                    shown: shown,
                    average: codeAverage,
                    interval: interval
                ).save(flush: true)
            }

            codes << code
        }

        averageWinnersCodesCache.put(cacheName, codes)

        log.info("[$drawing.number] Find winners by average links iterations: $iterations, codes in cache: ${codes.size()}")

        result
    }

    def getRandomCode(mask = null, excludes = null) {

        def code = ""
        Random random = ThreadLocalRandom.current()
        15.times { n ->

            if (mask && mask[n] != "_") {
                code += mask[n]
            } else {
                def i = random.nextInt(3)
                def c = i == 2 ? "X" : (i + 1).toString()

                if (excludes && excludes[n] == c) {
                    while (excludes[n] == c) {
                        i = random.nextInt(3)
                        c = i == 2 ? "X" : (i + 1).toString()
                    }
                }

                code += c
            }
        }

        code
    }

    private static boolean isExceedsMaxMatch(String code, Integer maxMatch = 9) {
        code.count("1") > maxMatch ?: code.count("X") > maxMatch ?: code.count("2") > maxMatch
    }

    def getWinnersStakeLinksChartData(Drawing drawing, String customWinners = null, StakeLinks stakeLinks = null) {

        def winnersCode = (customWinners && !customWinners.any { it == "_" } ? customWinners : drawing.winnersCode)?.replaceAll("0", "X")

        if (!stakeLinks) {
            stakeLinks = getStakeLinks(drawing)

            if (!stakeLinks) {
                return [:]
            }
        }

        def winnersStakeLinksChartDataCache = grailsCacheManager.getCache("winnersStakeLinksChartDataCache")
        def cacheName = "${drawing.number}_" + (winnersCode ?: '')
        def result = winnersStakeLinksChartDataCache.get(cacheName)?.get() ?: [:]
        def average = stakeLinks.average.setScale(2, RoundingMode.HALF_UP)

        if (result.average == average && result.drawingState == drawing.state) {
            log.info("[$drawing.number] Return winners stake links chart data from cache")
            return result
        }

        if (result) {
            winnersStakeLinksChartDataCache.evict(cacheName)
        }

        result.average = average
        result.drawingState = drawing.state
        result.maxLinks = 0
        result.minLinks = 0

        stakeLinks.data.each { line ->
            result.maxLinks = result.maxLinks ? [result.maxLinks, line.values().max()].max() : line.values().max()
            result.minLinks = result.minLinks ? [result.minLinks, line.values().min()].min() : line.values().min()
        }

        def link = winnersCode ? winnersCode[-1] + winnersCode[0] : null
        def event = drawing.events.find { it.sortOrder == 0 }
        def data = [
            "1",
            (stakeLinks.data[0].min { it.value }.value + stakeLinks.data[0].max { it.value }.value) / 2
        ]

        stakeLinks.data[0].sort { it.value }.each {
            data << it.value
            data << it.key
        }

        if (link) {
            data.addAll([
                event ? stakeLinks.total * drawing.events.find { it.sortOrder == 0 }?.getWinnerPercentage(winnersCode[0]) / 300 : stakeLinks.average,
                event ? stakeLinks.total * drawing.events.find { it.sortOrder == 0 }?.getWinnerProbability(winnersCode[0]) / 300 : stakeLinks.average,
                stakeLinks.data[0][link]
            ])
        }

        result.data = [data]

        (1..14).each { n ->
            link = winnersCode ? winnersCode[n - 1..n] : null
            event = drawing.events.find { it.sortOrder == n }
            data = [
                "${n + 1}",
                (stakeLinks.data[n].min { it.value }.value + stakeLinks.data[n].max { it.value }.value) / 2
            ]

            stakeLinks.data[n].sort { it.value }.each {
                data << it.value
                data << it.key
            }

            if (link) {
                data.addAll([
                    event ? stakeLinks.total * event.getWinnerPercentage(winnersCode[n]) / 300 : stakeLinks.average,
                    event ? stakeLinks.total * event.getWinnerProbability(winnersCode[n]) / 300 : stakeLinks.average,
                    stakeLinks.data[n][link]
                ])
            }

            result.data << data
        }

        result.winnersAverage = getWinnersAverage(stakeLinks, winnersCode)
        def averageData = getCodeAverage(drawing, winnersCode)
        result.poolAverage = averageData.pool
        result.lineAverage = averageData.line
        result.minMaxAverage = (result.data*.get(1).sum() / 15).setScale(2, RoundingMode.HALF_UP)
        result.topWinnersAverage = (stakeLinks.average * 2 - result.minMaxAverage).setScale(2, RoundingMode.HALF_UP)
        result.winnersCode = winnersCode

        if (winnersCode) {
            def drawingResults = drawing.state == 1 ? calcStakeWinMatches(winnersCode, drawing.expired) : calculateCoupons(drawing, customWinners ?: drawing.winnersCode)
            result.m13 = drawing.state == 1 ? drawingResults.m13 : drawingResults.find { it.Result == 13 }?.Count
            result.m14 = drawing.state == 1 ? drawingResults.m14 : drawingResults.find { it.Result == 14 }?.Count
            result.m15 = drawing.state == 1 ? drawingResults.m15 : drawingResults.find { it.Result == 15 }?.Count
            result.maxWin = countWinMatches(winnersCode, drawing.maxCode)
            result.minWin = countWinMatches(winnersCode, drawing.minCode)
        }

        winnersStakeLinksChartDataCache.put(cacheName, result)

        result
    }

    def getWinnersStakeDictChartData(Drawing drawing) {

        def winnersStakeDictChartData = grailsCacheManager.getCache("winnersStakeDictChartData")
        def cacheName = "${drawing.number}"
        def result = winnersStakeDictChartData.get(cacheName)?.get() ?: [:]
        def total = StakeDict.countByDrawingDateAndWinnersAverageIsNotNull(drawing.expired)

        if (!total) {
            log.info("[$drawing.number] Stake dict not found")
            return [:]
        }

        if (result.total == total && result.drawingState == drawing.state) {
            log.info("[$drawing.number] Return winners stake dict chart data from cache")
            return result
        }

        if (result) {
            winnersStakeDictChartData.evict(cacheName)
        }

        result.total = total
        result.drawingState = drawing.state

        def stakeDict = StakeDict.withCriteria {
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            eq("drawingDate", drawing.expired)
            isNotNull("winnersAverage")
            order("winnersAverage")
            projections {
                property("winnersAverage", "winners")
                property("poolAverage", "pool")
                property("lineAverage", "line")
                property("count", "count")
            }
        }

        def totalCount = stakeDict.sum { it.count }
        def allAverages = stakeDict*.winners + stakeDict*.pool + stakeDict*.line
        def minAverage = allAverages.min()
        def maxAverage = allAverages.max()

        def data = []

        ((minAverage / 10).toInteger()..(maxAverage / 10).toInteger()).each {
            def start = it * 10 ?: 0
            def end = start + 10
            def winnersMatches = stakeDict.findAll { it.winners > start && it.winners < end }
            def poolMatches = stakeDict.findAll { it.pool > start && it.pool < end }
            def lineMatches = stakeDict.findAll { it.line > start && it.line < end }

            data << [
                "'$start-$end'",
                winnersMatches.size() / total,
                winnersMatches ? winnersMatches.sum { it.count } / totalCount : 0,
                poolMatches.size() / total,
                lineMatches.size() / total
            ]
        }

        result.data = data

        winnersStakeDictChartData.put(cacheName, result)

        result
    }

    String getMinimalStakeLinkCode(Drawing drawing) {

        def stakeLinks = getStakeLinks(drawing)

        if (!stakeLinks) {
            return
        }

        def minAverage = stakeLinks.data*.min { it.value }
        def code = minAverage[0].key[1]

        (1..14).each { n ->
            code += minAverage[n].key[1]
        }

        code
    }

    List findCoupons(Integer drawingNumber, List coupons) {

        if (!drawingNumber || !coupons) {
            return []
        }

        def params = [
            options: [
                DrawingId   : drawingNumber,
                StartFrom   : 0,
                Count       : coupons.size(),
                Kind        : "coupon",
                ExtraCoupons: coupons
            ]
        ]

        httpClientService.post(urlPrefix + "FindCoupons", params)?.d?.Items ?: []
    }

    def calculateCoupons(Drawing drawing, String code) {

        def params = [
            options: [
                DrawingId   : drawing.number,
                ExtraCoupons: [],
                Kind        : "coupon",
                StartFrom   : 0,
                Count       : 50,
                Mask        : code
            ]
        ]

        httpClientService.post(urlPrefix + "CalculateCoupons", params)?.d?.Summary?.CalcResult?.DrawingResults
    }

    Integer calcPossibleWinAmount(Drawing drawing, String code) {

        if (drawing.state != 3) {
            return
        }

        def winCode = ""
        def matchCount = 0

        drawing.events.each {
            winCode += it.resultCode ?: code[it.sortOrder]

            if (!it.resultCode || it.resultCode == code[it.sortOrder]) {
                matchCount++
            }
        }

        def sum = BigDecimal.ZERO
        calculateCoupons(drawing, winCode).findAll { it.Count && it.Result <= matchCount }.each { sum += it.K * 50 }

        sum as Integer
    }

    void resetAverageCache(Drawing drawing) {

        def cache = grailsCacheManager.getCache("averageWinnersCodesCache")

        cache?.allKeys?.findAll { it.startsWith(drawing.number.toString()) }?.each { cache.evict(it) }
    }

    def getStakesStatistic(Integer monthCount, Integer minWinCount = 12, String averageType = "winners") {

        log.info("Getting stakes statistic [monthCount: $monthCount, minWinCount: $minWinCount, averageType: $averageType]")

        def stakesStatisticCache = grailsCacheManager.getCache("stakesStatisticCache")
        def cacheName = "${monthCount}_${minWinCount}_${averageType}_${DateUtils.formatDate(new Date(), 'dd.MM')}"
        def result = stakesStatisticCache.get(cacheName)?.get() ?: [:]

        if (result) {
            log.info("Return stakes statistic from cache")
            return result
        }

        def averages = StakeDict.withCriteria {
            isNotNull(averageType + "Average")
            between("drawingDate", DateUtils.plusMonths(-1 * (monthCount ?: 1)), new Date())
            ge("winCount", minWinCount)
            projections {
                property(averageType + "Average")
            }
        }

        if (!averages) {
            return [:]
        }

        ((averages.min() / 10).toInteger()..(averages.max() / 10).toInteger()).each {
            def start = it * 10
            def end = start + 10
            def matches = averages.findAll { it > start && it < end }

            if (matches) {
                result.put("$start-$end", matches.size() / averages.size() * 100)
            }
        }

        stakesStatisticCache.put(cacheName, result)

        result
    }

    def getIntellectualParams(Integer monthCount, Integer howMany, Integer minWinCount = 13, String averageType = "winners") {

        def statParams = getStakesStatistic(monthCount, minWinCount, averageType)

        if (!statParams) {
            return [[min: 120, max: 130, count: howMany]]
        }

        def bestParam = statParams.max { it.value }
        statParams.remove(bestParam.key)

        def coupons = []

        for (params in statParams) {
            def count = (params.value * howMany / 100).setScale(0, RoundingMode.DOWN)

            if (!count) {
                continue
            }

            def parts = params.key.split("-")
            coupons << [min: parts[0].toInteger(), max: parts[1].toInteger(), count: count]
        }

        def bestParts = bestParam.key.split("-")

        coupons << [min: bestParts[0].toInteger(), max: bestParts[1].toInteger(), count: howMany - (coupons ? coupons.sum { it.count } : 0)]

        coupons
    }

    def calcMaxStakes(List<String> stakeCodes, Integer allStakesCount = null) {

        if (!stakeCodes) {
            return [:]
        }

        def result = []

        (0..14).each { n ->
            def codes = stakeCodes*.getAt(n)
            def count = [
                "1": codes.count { it == "1" },
                "X": codes.count { it == "X" },
                "2": codes.count { it == "2" }
            ]

            def max = count.max { it.value }
            def min = count.findAll { it != max }.min { it.value }
            def mid = count.find { it != min && it != max }

            result << [
                max: [
                        code: max.key,
                        percent: (max.value / stakeCodes.size() * 100).setScale(2, RoundingMode.HALF_UP),
                        fromAll: allStakesCount ? (max.value / allStakesCount * 100).setScale(2, RoundingMode.HALF_UP) : 0
                ],
                mid: [
                        code: mid.key,
                        percent: (mid.value / stakeCodes.size() * 100).setScale(2, RoundingMode.HALF_UP),
                        fromAll: allStakesCount ? (mid.value / allStakesCount * 100).setScale(2, RoundingMode.HALF_UP) : 0
                ],
                min: [
                        code: min.key,
                        percent: (min.value / stakeCodes.size() * 100).setScale(2, RoundingMode.HALF_UP),
                        fromAll: allStakesCount ? (min.value / allStakesCount * 100).setScale(2, RoundingMode.HALF_UP) : 0
                ]
            ]
        }

        result
    }

    String getNextCode(Drawing drawing, String code, Integer index, Boolean down = false) {

        def linkData = StakeLinks.findByDrawingDate(drawing.expired).data

        def codeLinks = []

        (0..14).each { n ->
            codeLinks << code[n - 1] + code[n]
        }

        if (codeLinks[index] == (down ? linkData[index].min { it.value }.key : linkData[index].max { it.value }.key)) {
            log.info("[$drawing.number] Code link can't moved ${down ? 'down' : 'up'} because it last")
            return code
        }

        def oldValue = linkData[index].find { it.key == codeLinks[index] }.value
        codeLinks[index] = down ? linkData[index].findAll { it.value < oldValue }?.max { it.value }?.key : linkData[index].findAll { it.value > oldValue }?.min { it.value }?.key

        if (!codeLinks[index]) {
            log.error("[$drawing.number] Next link not found")
            return code
        }

        if (index != 14) {
            codeLinks[index + 1] = linkData[index + 1].find { it.key[0] == codeLinks[index][1] && it.key[1] == codeLinks[index + 2 == 15 ? 0 : index + 2][0] }.key
        } else {
            codeLinks[0] = linkData[0].find { it.key[0] == codeLinks[index][1] && it.key[1] == codeLinks[1][0] }.key
        }


        if (index != 0) {
            codeLinks[index - 1] = linkData[index - 1].find { it.key[1] == codeLinks[index][0] && it.key[0] == codeLinks[index - 2 < 0 ? 14 : index - 2][1] }.key
        } else {
            codeLinks[14] = linkData[14].find { it.key[1] == codeLinks[index][0] && it.key[0] == codeLinks[13][1] }.key
        }

        codeLinks*.getAt(1).join()
    }

    def maxMinWinnersCount(Map params) {

        def drawings = getLastDrawings(DateUtils.plusMonths(((params.monthCount as Integer) ?: 1) * -1))
        def maxMinCount = []

        drawings.each { drawing ->
            if (drawing.winnersCode) {
                maxMinCount << [
                    max: countWinMatches(drawing.winnersCode, drawing.maxCode),
                    min: countWinMatches(drawing.winnersCode, drawing.minCode)
                ]
            }
        }

        def total = maxMinCount.size()
        def maxMinWinnersCount = []

        maxMinCount.groupBy { it.max }.each {
            def min = it.value.min.sort()
            maxMinWinnersCount << [
                max     : it.key,
                min     : min.unique(),
                minRange: (min.min()..min.max()),
                ratio   : (it.value.size() / total * 100).setScale(2, RoundingMode.HALF_UP)
            ]
        }

        maxMinWinnersCount
    }

    def maxWinnersCount(Map params) {

        def drawings = getLastDrawings(DateUtils.plusMonths(((params.monthCount as Integer) ?: 1) * -1))
        def maxWinCount = []

        drawings.each { drawing ->
            if (drawing.winnersCode) {
                maxWinCount << countWinMatches(drawing.winnersCode, drawing.maxCode)
            }
        }

        def total = maxWinCount.size()
        def maxWinnersCount = []

        maxWinCount.groupBy { it }.each {
            maxWinnersCount << [
                count: it.key,
                ratio: (it.value.size() / total * 100).setScale(2, RoundingMode.HALF_UP)
            ]
        }

        maxWinnersCount
    }

    def minWinnersCount(Map params) {

        def drawings = getLastDrawings(DateUtils.plusMonths(((params.monthCount as Integer) ?: 1) * -1))
        def minWinCount = []

        drawings.each { drawing ->
            if (drawing.winnersCode) {
                minWinCount << countWinMatches(drawing.winnersCode, drawing.minCode)
            }
        }

        def total = minWinCount.size()
        def minWinnersCount = []

        minWinCount.groupBy { it }.each {
            minWinnersCount << [
                count: it.key,
                ratio: (it.value.size() / total * 100).setScale(2, RoundingMode.HALF_UP)
            ]
        }

        minWinnersCount
    }

    def getAutoExcludes(Drawing drawing, Map params = [:]) {

        if (!params.averageRanges) {
            params.averageRanges = getIntellectualParams(params.monthCount ?: 1, params.howMany ?: 10, params.minWinCount ?: 13)
        }

        def stakeLinks = getStakeLinks(drawing)
        def codes = []

        ((params.iterations as Integer) ?: 10).times {
            codes.addAll(getAverageWinnersCodesEx(drawing, params, stakeLinks)?.code)
        }

        def result = ""

        calcMaxStakes(codes).each {
            result += it.min.percent < 30 ? it.min.code : "_"
        }

        result
    }

    void updateSuperExpressImage(Drawing drawing) {

        def se = SuperExpress.findByDrawingNumber(drawing.number)

        if (!se || se.drawingVersion == drawing.ver) {
            return
        }

        ImageRenderer.renderToImage("http://localhost:8080/drawing/formatter/$drawing.number", "/tmp/${se.urlId}.png", 200)

        se.drawingVersion = drawing.ver
        se.save(flush: true)
    }
}
