package express

import grails.gorm.transactions.Transactional
import grails.util.Holders
import org.quartz.CronExpression
import org.quartz.CronScheduleBuilder
import org.quartz.Job
import org.quartz.JobBuilder
import org.quartz.JobDataMap
import org.quartz.JobDetail
import org.quartz.JobKey
import org.quartz.SimpleScheduleBuilder
import org.quartz.TriggerBuilder
import org.quartz.TriggerKey
import org.quartz.impl.matchers.GroupMatcher


@Transactional
class SchedulerService {

    def jobManagerService

    static DEFAULT_REPEAT_INTERVALS = [
        UpdateDrawingJob    : 10,
        UpdateAllDrawingJob : 10,
        UpdateSessionInfoJob: 5
    ]

    private getScheduler() {
        jobManagerService.quartzScheduler
    }

    static getRepeatInterval(String className) {
        Holders.config.express.jobs."$className".repeatInterval ?: DEFAULT_REPEAT_INTERVALS[className]
    }

    Boolean scheduleJobOnce(Class<? extends Job> jobClass, Map data, Date date) {

        def number = data.number ?: data.drawingNumber

        if (checkExists(jobClass, number, data.suffix)) {
            return false
        }

        def cronExpression = "0 $date.minutes $date.hours ${DateUtils.getDayOfMonth(date)} ${DateUtils.getMonth(date) + 1} ?"

        CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(new CronExpression(cronExpression))
        TriggerKey triggerKey = new TriggerKey("${jobClass.simpleName}Trigger_" + number + (data.suffix != null ? "_" + data.suffix : ''))

        def trigger = TriggerBuilder.newTrigger()
                                    .withIdentity(triggerKey)
                                    .withPriority(jobClass.priority ?: 5)
                                    .withSchedule(scheduleBuilder)
                                    .usingJobData(new JobDataMap(data))
                                    .build()

        JobDetail job = JobBuilder.newJob(jobClass)
                                  .withIdentity("$jobClass.simpleName" + number + (data.suffix != null ? "_" + data.suffix : ''))
                                  .build()

        if (!scheduler.started) {
            scheduler.start()
        }

        scheduler.scheduleJob(job, trigger)

        log.info("$jobClass.simpleName scheduled [data: $data cronExpression: $cronExpression]")

        true
    }

    Boolean scheduleJob(Class<? extends Job> jobClass, Map data = [:]) {

        def number = data.number ?: data.drawingNumber ?: 0

        if (checkExists(jobClass, number, data.suffix)) {
            return false
        }

        SimpleScheduleBuilder scheduleBuilder = SimpleScheduleBuilder.simpleSchedule()
        TriggerKey triggerKey = new TriggerKey("${jobClass.simpleName}Trigger_" + number + (data.suffix != null ? "_" + data.suffix : ''))

        def interval = getRepeatInterval(jobClass.simpleName) ?: 0

        def trigger = TriggerBuilder.newTrigger()
                                    .withIdentity(triggerKey)
                                    .withPriority(jobClass.priority ?: 5)
                                    .withSchedule(interval ? scheduleBuilder.repeatForever().withIntervalInMinutes(interval) : scheduleBuilder)
                                    .usingJobData(new JobDataMap(data))
                                    .build()

        JobDetail job = JobBuilder.newJob(jobClass)
                                  .withIdentity("$jobClass.simpleName" + number + (data.suffix != null ? "_" + data.suffix : ''))
                                  .build()

        if (!scheduler.started) {
            scheduler.start()
        }

        scheduler.scheduleJob(job, trigger)

        log.info("$jobClass.simpleName scheduled [data: $data interval: $interval]")

        true
    }

    void unscheduleJob(Class<? extends Job> jobClass, Long number = 0, Integer suffix = null) {

        if (!checkExists(jobClass, number, suffix)) {
            return
        }

        def triggers = scheduler.getTriggersOfJob(new JobKey("$jobClass.simpleName" + number + (suffix != null ? "_" + suffix : '')))

        triggers.each {

            if (!scheduler.unscheduleJob(it.key)) {
                log.warn("Can't unschedule job ${it.key}")
            }
        }

        log.info("$jobClass.simpleName unscheduled [number: $number, triggers: ${triggers.name}]")
    }

    Boolean checkExists(Class<? extends Job> jobClass, Long number = 0, Integer suffix = null) {

        scheduler.getTriggersOfJob(new JobKey("$jobClass.simpleName" + number + (suffix != null ? "_" + suffix : ''))).find {
            it.key.name.startsWith("${jobClass.simpleName}Trigger_" + number + (suffix != null ? "_" + suffix : ''))
        }
    }

    def getJobKeys() {

        scheduler.getJobKeys(GroupMatcher.anyJobGroup())
    }

    def getTriggers() {

        def result = []

        getJobKeys().each {
            result << scheduler.getTriggersOfJob(it)
        }

        result
    }

    def getJobDetails() {
        def result = []

        getJobKeys().each {
            result << scheduler.getJobDetail(it)
        }

        result
    }
}
