package express

import grails.gorm.transactions.Transactional
import grails.plugins.rest.client.RestBuilder
import grails.util.Holders


@Transactional
class TelegramService {

    private RestBuilder restBuilder = new RestBuilder(proxy: proxy)

    private static getProxy() {

        def proxyString = Holders.config.express?.telegram?.proxy

        if (!proxyString) {
            return
        }

        def proxy = proxyString.split(":")

        new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxy[0], proxy[1].toIneger()))
    }

    private static getTelegramUrl() {

        def token = Holders.config.express?.telegram?.token

        if (!token) {
            return
        }

        "https://api.telegram.org/bot" + token
    }

    void sendMessage(User user, String message) {

        if (!message || !telegramUrl) {
            return
        }

        def telegramChatId = UserData.findByUser(user)?.telegramChatId

        if (!telegramChatId) {
            return
        }

        restBuilder.post(telegramUrl + "/sendMessage") {
            json chat_id: telegramChatId,  parse_mode: "HTML", text: message
        }
    }

}
