package express

import grails.util.Holders
import org.apache.commons.logging.LogFactory
import org.quartz.DisallowConcurrentExecution
import org.quartz.Job
import org.quartz.JobExecutionContext
import org.quartz.JobExecutionException
import org.quartz.Trigger


/**
 *
 * @author semenuk
 */
@DisallowConcurrentExecution
abstract class AbstractDrawingJob implements Job {

    protected static final log = LogFactory.getLog(AbstractDrawingJob)

    private JobExecutionContext context

    @Override
    void execute(JobExecutionContext context) throws JobExecutionException {

        this.context = context

        if (drawingNumberNeeded && !drawingNumber) {
            log.info("current drawing id don't set, remove job")
            return
        }

        def startTime = System.currentTimeMillis()

        log.info("Execute ${this.class.simpleName} with mergedJobDataMap: ${context.mergedJobDataMap}")

        executeJob()

        log.info("Finish ${this.class.simpleName} with mergedJobDataMap: ${context.mergedJobDataMap} in ${(System.currentTimeMillis() - startTime) / 1000} sec")
    }

    Integer getDrawingNumber() {
        (context.mergedJobDataMap?.drawingNumber as Integer) ?: (context.mergedJobDataMap?.number as Integer)
    }

    static getDrawingService() {
        Holders.grailsApplication.mainContext.getBean("drawingService")
    }

    static getUserService() {
        Holders.grailsApplication.mainContext.getBean("userService")
    }

    static getSchedulerService() {
        Holders.grailsApplication.mainContext.getBean("schedulerService")
    }

    static getTelegramService() {
        Holders.grailsApplication.mainContext.getBean("telegramService")
    }

    static getHttpClientService() {
        Holders.grailsApplication.mainContext.getBean("httpClientService")
    }

    boolean isDrawingNumberNeeded() {
        true
    }

    def getContext() {
        return context
    }

    static getPriority() {
        return Trigger.DEFAULT_PRIORITY
    }

    protected abstract void executeJob()
}
