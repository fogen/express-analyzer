package express

/**
 *
 * @author semenuk
 */
enum AverageType {

    LINK("winners", "Связи"),
    POOL("pool", "Пул"),
    LINE("line", "Линия")

    final String id
    final String decription

    AverageType(id, description) {
        this.id = id
        this.decription = description
    }

}